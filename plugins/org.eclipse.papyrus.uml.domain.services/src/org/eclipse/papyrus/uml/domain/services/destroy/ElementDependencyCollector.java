/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.CollaborationHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.OccurrenceSpecificationHelper;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.UMLService;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.UMLTemporalHelper;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageEnd;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PartDecomposition;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Switch class used to delegate "before destroy dependence" actions according
 * to the type of the object to delete.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class ElementDependencyCollector implements IDestroyerDependencyCollector {

    /**
     * Adapter used to get inverse references.
     */
    private final ECrossReferenceAdapter crossReferenceAdapter;

    /**
     * Constructor.
     *
     * @param theCrossReferenceAdapter
     *                                 an adapter used to get inverse references
     */
    public ElementDependencyCollector(ECrossReferenceAdapter theCrossReferenceAdapter) {
        super();
        this.crossReferenceAdapter = theCrossReferenceAdapter;
    }

    @Override
    public Set<EObject> collectDependencies(EObject source) {
        DestroyDependencyCollectorSwitch collector = new DestroyDependencyCollectorSwitch(this.crossReferenceAdapter);
        collector.doSwitch(source);
        return collector.getDependentsToRemove();
    }

    static class DestroyDependencyCollectorSwitch extends UMLSwitch<Void> {

        /** Adapter used to get inverse references. */
        private final ECrossReferenceAdapter crossReferenceAdapter;

        /** Set of dependences to remove. */
        private final Set<EObject> dependentsToRemove = new HashSet<>();

        /**
         * Default constructor.
         *
         * @param crossReferenceAdapter
         *                              cross reference adapter
         */
        DestroyDependencyCollectorSwitch(ECrossReferenceAdapter crossReferenceAdapter) {
            this.crossReferenceAdapter = crossReferenceAdapter;
        }

        private void safeAdd(Element value) {
            if (value != null) {
                dependentsToRemove.add(value);
            }
        }

        /**
         * Action to launch before deleting a {@link NamedElement}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.NamedElementHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * <pre>
         * This method deletes {@link DirectedRelationship} related to the named element (source or target).
         * </pre>
         *
         * @param namedElementToDelete
         *                             the {@link NamedElement} to remove
         *
         */
        @Override
        public Void caseNamedElement(NamedElement namedElementToDelete) {
            Iterator<DirectedRelationship> srcRelationhipsIt = namedElementToDelete
                    .getSourceDirectedRelationships(UMLPackage.eINSTANCE.getDependency()).iterator();
            while (srcRelationhipsIt.hasNext()) {
                DirectedRelationship directedRelationship = srcRelationhipsIt.next();

                // If all sources from the directed relationship are to be destroyed, add the
                // relationship destruction
                if (directedRelationship.getSources().contains(namedElementToDelete)) {
                    this.dependentsToRemove.add(directedRelationship);
                }
            }
            Iterator<DirectedRelationship> tgtRelationhipsIt = namedElementToDelete
                    .getTargetDirectedRelationships(UMLPackage.eINSTANCE.getDependency()).iterator();
            while (tgtRelationhipsIt.hasNext()) {
                DirectedRelationship directedRelationship = tgtRelationhipsIt.next();

                // If all sources from the directed relationship are to be destroyed, add the
                // relationship destruction
                if (directedRelationship.getTargets().contains(namedElementToDelete)) {
                    this.dependentsToRemove.add(directedRelationship);
                }
            }
            return super.caseNamedElement(namedElementToDelete);
        }

        /**
         * Action to launch before deleting a {@link ActivityNode}. See
         * org.eclipse.papyrus.uml.service.types.helper.advice.ActivityNodeHelperAdvice.getDestroyActivityEdgeCommand(DestroyDependentsRequest)
         *
         * @param activityNodeToDelete
         *                             the {@link ActivityNode} to remove.
         */
        @Override
        public Void caseActivityNode(ActivityNode activityNodeToDelete) {
            this.dependentsToRemove.addAll(activityNodeToDelete.getOutgoings());
            this.dependentsToRemove.addAll(activityNodeToDelete.getIncomings());
            return super.caseActivityNode(activityNodeToDelete);
        }

        /**
         * Action to launch before deleting a {@link ConnectorEnd}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ConnectorEndHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest))}
         *
         * <pre>
         * It deletes the related Connector in case this connector only has less than 2 ends left.
         * </pre>
         *
         * @param connectorEndToDelete
         *                             the {@link NamedElement} to remove
         */
        @Override
        public Void caseConnectorEnd(ConnectorEnd connectorEndToDelete) {
            Connector connector = (Connector) connectorEndToDelete.getOwner();
            if (connector.getEnds().size() <= 2) {
                this.dependentsToRemove.add(connector);
            }
            return super.caseConnectorEnd(connectorEndToDelete);
        }

        /**
         * Action to launch before deleting a {@link Property}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * @param propertyToDelete
         *                         the property to delete
         *
         */
        @Override
        public Void caseProperty(Property propertyToDelete) {
            EReference[] refs = null;

            // Get related ConnectorEnd to be destroyed with the property
            // Possible references from ConnectorEnd to Property (or Port)
            refs = new EReference[] { UMLPackage.eINSTANCE.getConnectorEnd_Role(),
                    UMLPackage.eINSTANCE.getConnectorEnd_PartWithPort() };
            Collection<EObject> connectorEndRefs = UMLService.getReferencers(propertyToDelete, refs,
                    this.crossReferenceAdapter);
            this.dependentsToRemove.addAll(connectorEndRefs);

            // Get possible associations using this Property as end
            refs = new EReference[] { UMLPackage.eINSTANCE.getAssociation_MemberEnd() };
            Collection<EObject> associationRefs = UMLService.getReferencers(propertyToDelete, refs,
                    this.crossReferenceAdapter);
            for (EObject association : associationRefs) {

                // Test the number of remaining ends considering the dependents elements
                // deletion in progress
                List<Property> remainingMembers = new ArrayList<>();
                remainingMembers.addAll(((Association) association).getMemberEnds());
                if (remainingMembers.size() <= 2) {
                    this.dependentsToRemove.add(association);
                }
            }

            return super.caseProperty(propertyToDelete);
        }



        @Override
        public Void caseElement(Element object) {
            // When deleting an Element also delete all stereotype applications linked to it
            object.getStereotypeApplications().stream()//
                    .filter(EObject.class::isInstance)//
                    .map(EObject.class::cast)//
                    .forEach(dependentsToRemove::add);
            return super.caseElement(object);
        }

        /**
         * Action to launch before deleting a {@link Classifier}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ClassifierHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * <pre>
         * This method deletes :
         * - Generalization related to the Classifier (source or target).
         * - Association related to the Classifier (source or target type).
         * </pre>
         *
         * @param classifierToDelete
         *                           the {@link Classifier} to remove
         *
         */
        @Override
        public Void caseClassifier(Classifier classifierToDelete) {
            // Get related generalizations
            this.dependentsToRemove.addAll(
                    classifierToDelete.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getGeneralization()));
            this.dependentsToRemove.addAll(
                    classifierToDelete.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getGeneralization()));

            // Get related association for this classifier, then delete member ends for
            // which this classifier is the type.
            for (Association association : classifierToDelete.getAssociations()) {
                for (Property end : association.getMemberEnds()) {
                    if (end.getType() == classifierToDelete) {
                        this.dependentsToRemove.add(association);
                    }
                }
            }
            return super.caseClassifier(classifierToDelete);
        }

        @Override
        public Void casePackage(Package object) {
            this.crossReferenceAdapter.getInverseReferences(object, true).stream()
                    .filter(s -> s.getEStructuralFeature() == UMLPackage.eINSTANCE.getPackageImport_ImportedPackage()
                            || s.getEStructuralFeature() == UMLPackage.eINSTANCE.getPackageMerge_MergedPackage())
                    .map(s -> s.getEObject()).forEach(this.dependentsToRemove::add);
            return super.casePackage(object);
        }

        /**
         * Action to launch before deleting a {@link Collaboration}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.CollaborationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * @param classifierToDelete
         *                           the {@link Collaboration} to remove
         *
         */
        @Override
        public Void caseCollaboration(Collaboration collaborationToDelete) {
            this.dependentsToRemove.addAll(CollaborationHelper.getRelatedRoleBindings(collaborationToDelete, null,
                    this.crossReferenceAdapter));
            return super.caseCollaboration(collaborationToDelete);
        }

        @Override
        public Void caseIntervalConstraint(IntervalConstraint interval) {
            if (interval.getSpecification() instanceof Interval value) { // Only case allowed
                for (ValueSpecification terminal : List.of(value.getMin(), value.getMax())) {
                    if (terminal != null && UMLService.isOnlyUsage(terminal, value, crossReferenceAdapter)) {
                        dependentsToRemove.add(terminal);
                    }
                }
            }
            return super.caseIntervalConstraint(interval);
        }

        /**
         * Action to launch before deleting a {@link ExecutionSpecification}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ExecutionSpecificationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * @param esToDelete
         *                   the {@link ExecutionSpecification} to remove
         */
        @Override
        public Void caseExecutionSpecification(ExecutionSpecification esToDelete) {
            // Check whether start - finish referenced OccurrenceSpecification should be
            // added to the dependents list
            OccurrenceSpecification osStart = esToDelete.getStart();
            if (OccurrenceSpecificationHelper.shouldDestroyWithExecution(esToDelete, osStart,
                    this.crossReferenceAdapter)) {
                this.dependentsToRemove.add(osStart);
            }

            OccurrenceSpecification osFinish = esToDelete.getFinish();
            if (OccurrenceSpecificationHelper.shouldDestroyWithExecution(esToDelete, osFinish,
                    this.crossReferenceAdapter)) {
                this.dependentsToRemove.add(osFinish);
            }
            // add TimeElement and Duration
            dependentsToRemove.addAll(UMLTemporalHelper.getTemporalElements(esToDelete, crossReferenceAdapter));

            return super.caseExecutionSpecification(esToDelete);
        }

        /**
         * Action to launch before deleting a {@link Message}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.MessageHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest))}.
         * <p>
         * Deletes the related {@link MessageEnd} when they are set and not shared with other elements.
         *
         * @param messageToDelete
         *                        the {@link Message} to remove
         */
        @Override
        public Void caseMessage(Message messageToDelete) {
            safeAdd(messageToDelete.getReceiveEvent()); // Either MOS or Gate
            safeAdd(messageToDelete.getSendEvent());

            // add TimeElement and Duration
            dependentsToRemove.addAll(UMLTemporalHelper.getTemporalElements(messageToDelete, crossReferenceAdapter));

            return super.caseMessage(messageToDelete);
        }


        /**
         * Action to launch before deleting a {@link Lifeline}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.LifelineHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * @param lifelineToDelete
         *                         the {@link Lifeline} to remove
         */
        @Override
        public Void caseLifeline(Lifeline lifelineToDelete) {
            for (InteractionFragment ift : lifelineToDelete.getCoveredBys()) {
                // Destroy covered ExecutionSpecification
                if (ift instanceof ExecutionSpecification) {
                    this.dependentsToRemove.add(ift);
                }

                // Destroy covered OccurrenceSpecification
                if (ift instanceof OccurrenceSpecification) {
                    this.dependentsToRemove.add(ift);
                    // Destroy related Message
                    if (ift instanceof MessageOccurrenceSpecification mos) {
                        this.dependentsToRemove.add(mos.getMessage());
                    }
                }
                // Destroy all the interactionFragments that cover only the lifeline being deleted
                if (ift.getCovereds().size() == 1) {
                    this.dependentsToRemove.add(ift);
                }
            }

            // Destroy decomposed lifelines
            PartDecomposition decomposition = lifelineToDelete.getDecomposedAs();
            if (decomposition != null
                    && UMLService.isOnlyUsage(decomposition, lifelineToDelete, this.crossReferenceAdapter)) {
                this.dependentsToRemove.add(decomposition);
            }
            return super.caseLifeline(lifelineToDelete);
        }

        /**
         * Action to launch before deleting a {@link OccurrenceSpecification}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.OccurrenceSpecificationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * <pre>
         * Add dependents to destroy :
         * - related time elements
         * - linked general ordering
         * </pre>
         *
         * @param osToDelete
         *                   the {@link OccurrenceSpecification} to remove
         */
        @Override
        public Void caseOccurrenceSpecification(OccurrenceSpecification osToDelete) {
            for (Optional<ExecutionSpecification> execution : List.of(
                    OccurrenceSpecificationHelper.getExecutionFromStartOccurrence(osToDelete),
                    OccurrenceSpecificationHelper.getExecutionFromFinishOccurrence(osToDelete))) {
                if (execution.isPresent()) {
                    dependentsToRemove.add(execution.get());
                }
            }

            // delete linked time elements
            dependentsToRemove.addAll(UMLTemporalHelper.getTemporalElements(osToDelete, crossReferenceAdapter));

            // delete linked general ordering
            /**
             * Note: GeneralOrdering should be necessarily removed because the opposite
             * references 'GeneralOrdering::before[1]' and 'GeneralOrdering::after[1]' which
             * designate this OccurrenceSpecification are mandatory
             */
            this.dependentsToRemove.addAll(osToDelete.getToBefores());
            this.dependentsToRemove.addAll(osToDelete.getToAfters());

            return super.caseOccurrenceSpecification(osToDelete);
        }

        @Override
        public Void caseMessageOccurrenceSpecification(MessageOccurrenceSpecification osToDelete) {
            safeAdd(osToDelete.getMessage());

            return super.caseMessageOccurrenceSpecification(osToDelete);
        }

        /**
         * In the StateMachine, incoming and outgoing transitions are removed with the
         * Vertex (ex: State, PseudoState and InitialState)
         */
        @Override
        public Void caseVertex(Vertex vertex) {
            this.dependentsToRemove.addAll(vertex.getIncomings());
            this.dependentsToRemove.addAll(vertex.getOutgoings());
            return super.caseVertex(vertex);
        }

        /**
         * Action to launch before deleting a {@link Association}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.AssociationEditHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         *
         * @param association
         *                    the association to delete
         *
         */
        @Override
        public Void caseAssociation(Association association) {

            EList<Property> ownedEnds = association.getOwnedEnds();
            for (Property end : association.getMemberEnds()) {
                if (ownedEnds.contains(end)) {
                    this.dependentsToRemove.add(end);
                } else if (end.getType() != null) {
                    // when user set the source or target property (not owned by association) type
                    // to null, the association is removed (because it cannot be defined without a
                    // source or target type) but we do not remove the source or target property
                    // with the type null.
                    this.dependentsToRemove.add(end);
                }
            }
            return super.caseAssociation(association);
        }

        @Override
        public Void caseUseCase(UseCase useCase) {
            // Delete related includes
            this.dependentsToRemove.addAll(useCase.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getInclude()));
            this.dependentsToRemove.addAll(useCase.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getInclude()));
            // Delete related extends
            this.dependentsToRemove.addAll(useCase.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getExtend()));
            this.dependentsToRemove.addAll(useCase.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getExtend()));
            return super.caseUseCase(useCase);
        }

        public Set<EObject> getDependentsToRemove() {
            return this.dependentsToRemove;
        }

        @Override
        public Void caseInteractionOperand(InteractionOperand interactionOperand) {
            if (interactionOperand.getOwner() instanceof CombinedFragment) {
                CombinedFragment combinedFragment = (CombinedFragment) interactionOperand.getOwner();
                if (combinedFragment.getOperands().size() == 1
                        && combinedFragment.getOperands().contains(interactionOperand)) {
                    this.dependentsToRemove.add(combinedFragment);
                }
            }
            return super.caseInteractionOperand(interactionOperand);
        }
    }

}
