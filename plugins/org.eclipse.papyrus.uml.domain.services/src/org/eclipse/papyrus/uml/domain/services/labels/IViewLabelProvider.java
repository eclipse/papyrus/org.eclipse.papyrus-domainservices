/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import org.eclipse.emf.ecore.EObject;

/**
 * Provides a label of element UML Element in a view.
 *
 * @author Artuhr Daussy
 */
public interface IViewLabelProvider {

    /**
     * Gets the label for a semantic element.
     *
     * @param element
     *                a element
     * @return a label (never <code>null</code>)
     */
    String getLabel(EObject element);

}
