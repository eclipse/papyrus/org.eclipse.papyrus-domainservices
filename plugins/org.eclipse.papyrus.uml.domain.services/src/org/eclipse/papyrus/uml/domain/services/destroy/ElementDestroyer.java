/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA List, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.State;

/**
 * Most generic {@link IDestroyer}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class ElementDestroyer implements IDestroyer {

    private IDestroyerDependencyCollector dependencyCollector;

    /**
     * Adapter used to get inverse references.
     */
    private final ECrossReferenceAdapter crossReferenceAdapter;

    private final IDestroyerChecker destroyerChecker;

    /**
     * Constructor.
     *
     * @param theCrossReferenceAdapter
     *                                 an adapter used to get inverse references
     */
    public ElementDestroyer(ECrossReferenceAdapter theCrossReferenceAdapter, IDestroyerChecker destroyerChecker,
            IDestroyerDependencyCollector dependencyCollector) {
        super();
        this.dependencyCollector = dependencyCollector;
        this.crossReferenceAdapter = theCrossReferenceAdapter;
        this.destroyerChecker = destroyerChecker;
    }

    /**
     * Default Constructor.
     *
     * @param theCrossReferenceAdapter
     *                                 an adapter used to get inverse references
     * @return default Constructor.
     */
    public static ElementDestroyer buildDefault(ECrossReferenceAdapter theCrossReferenceAdapter,
            IEditableChecker editableChecker) {
        return new ElementDestroyer(theCrossReferenceAdapter,
                new ElementDestroyerChecker(theCrossReferenceAdapter, editableChecker),
                new ElementDependencyCollector(theCrossReferenceAdapter));
    }

    @Override
    public DestroyerStatus destroy(EObject semanticElement) {
        if (semanticElement == null) {
            return DestroyerStatus.createFailingStatus("The semantic object to destroy is null", Set.of());
        }
        return this.genericDelete(semanticElement);
    }

    /**
     * Removes a semantic element value from its container. Inspired by
     *
     * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelper#getEditCommand(IEditCommandRequest,
     *      IEditHelperAdvice[])
     *
     * @param semanticElement
     *                        the semantic element to delete
     * @return a destroy status
     */
    private DestroyerStatus genericDelete(EObject semanticElement) {

        Set<EObject> toDeletes = computeElementToDelete(semanticElement);

        DestroyerStatus canDestroyStatus = destroyerChecker.canDestroy(toDeletes);

        if (canDestroyStatus.getState().equals(State.DONE)) {
            for (EObject toDelete : toDeletes) {

                // Maybe add before behavior

                // tear down incoming references
                tearDownIncomingReferences(toDelete);

                // also tear down outgoing references, because we don't want
                // reverse-reference lookups to find destroyed objects
                tearDownOutgoingReferences(toDelete);

                // remove the object from its container
                EcoreUtil.remove(toDelete);

                // Maybe add after behavior
            }
        }

        return canDestroyStatus;
    }

    private Set<EObject> computeElementToDelete(EObject semanticElement) {
        Set<EObject> toDeletes = new LinkedHashSet<>();

        Set<EObject> notComputedDependencies = new LinkedHashSet<>();

        notComputedDependencies.add(semanticElement);
        while (!notComputedDependencies.isEmpty()) {
            // Impossible to simply loop on 'notComputedDependencies'
            // Collection is amended as element are analyzed.

            // Remove First
            Iterator<EObject> iterator = notComputedDependencies.iterator();
            EObject toAnalyse = iterator.next();
            iterator.remove();

            EMFUtils.eAllContentStreamWithSelf(toAnalyse) // with all sub-elements
                    .filter(e -> !toDeletes.contains(e)) // skip previous work
                    .forEach(e -> {
                        toDeletes.add(e);
                        Set<EObject> itemDependencies = dependencyCollector.collectDependencies(e);
                        // Amend Analysis
                        for (EObject o : itemDependencies) {
                            if (!toDeletes.contains(o)) {
                                notComputedDependencies.add(o);
                            }
                        }
                    });
        }
        return toDeletes;
    }


    /**
     * Tears down references to the object that we are destroying, from all other
     * objects in the resource set.
     *
     * @param destructee
     *                   the object being destroyed
     */
    private void tearDownIncomingReferences(EObject destructee) {
        if (crossReferenceAdapter != null) {
            Collection<?> inverseReferences = crossReferenceAdapter.getInverseReferences(destructee);
            if (inverseReferences != null) {
                int size = inverseReferences.size();
                if (size > 0) {
                    Setting setting;
                    EReference eRef;
                    Setting[] settings = inverseReferences.toArray(new Setting[size]);
                    for (int i = 0; i < size; ++i) {
                        setting = settings[i];
                        eRef = (EReference) setting.getEStructuralFeature();
                        if (eRef.isChangeable() && !eRef.isDerived() && !eRef.isContainment() && !eRef.isContainer()) {
                            EcoreUtil.remove(setting.getEObject(), eRef, destructee);
                        }
                    }
                }
            }
        }
    }

    /**
     * Tears down outgoing unidirectional references from the object being destroyed
     * to all other elements in the resource set. This is required so that
     * reverse-reference queries will not find the destroyed object.
     *
     * @param destructee
     *                   the object being destroyed
     */
    private void tearDownOutgoingReferences(EObject destructee) {
        // CHECKSTYLE:OFF Papyrus legacy Code
        for (Iterator<?> iter = destructee.eClass().getEAllReferences().iterator(); iter.hasNext();) {
            // CHECKSTYLE:ON
            EReference reference = (EReference) iter.next();

            // container/containment features are handled separately, and
            // bidirectional references were handled via incomings
            if (shouldUnset(reference)) {

                if (destructee.eIsSet(reference)) {
                    destructee.eUnset(reference);
                }
            }
        }
    }

    // CHECKSTYLE:OFF Papyrus legacy Code
    private boolean shouldUnset(EReference reference) {
        return reference.isChangeable() //
                && !reference.isDerived() //
                && !reference.isContainer() //
                && !reference.isContainment() //
                && (reference.getEOpposite() == null);
    }
    // CHECKSTYLE:ON

}
