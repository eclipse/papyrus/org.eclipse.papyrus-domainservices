/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Checks if a graphical D&D is possible in the Deployment diagram.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class DeploymentInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new DeploymentDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }


}
