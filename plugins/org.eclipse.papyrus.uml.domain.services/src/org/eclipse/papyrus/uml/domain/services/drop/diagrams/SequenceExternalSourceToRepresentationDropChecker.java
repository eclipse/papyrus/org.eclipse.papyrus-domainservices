/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Check semantic drop (from Explorer view) to a Sequence Diagram Element.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class SequenceExternalSourceToRepresentationDropChecker implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new SequenceDropInsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class SequenceDropInsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject target;

        SequenceDropInsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.target = target;
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            final CheckStatus result;
            if (this.target instanceof Lifeline || this.target instanceof Interaction) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Property can only be drag and drop on an Interaction or a Lifeline.");
            }
            return result;
        }

        @Override
        public CheckStatus caseType(Type type) {
            final CheckStatus result;
            if (this.target instanceof Lifeline || this.target instanceof Interaction) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Type can only be drag and drop on an Interaction or a Lifeline.");
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }
    }

}
