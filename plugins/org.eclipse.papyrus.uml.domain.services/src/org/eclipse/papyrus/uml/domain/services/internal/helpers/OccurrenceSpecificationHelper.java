/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.common.util.CacheAdapter;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides some utils method on {@link OccurrenceSpecification}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class OccurrenceSpecificationHelper {

    /**
     * Get interaction from a given {@link Element}.
     *
     * @param element
     *                the element attached to an interaction
     * @return its matching interaction.
     */
    public static Interaction getInteraction(Element element) {
        Interaction result = null;

        for (Element next = element; next != null && result == null; next = next.getOwner()) {
            if (next instanceof Interaction) {
                result = (Interaction) next;
            }
        }

        return result;
    }

    /**
     * Get time constraints in the contextual {@code interaction} that constrain only the {@code constrained} element
     * and no others.
     *
     * @param interaction
     *                    the contextual interaction
     * @param constrained
     *                    the constrained element
     *
     * @return its unique time constraints
     */
    public static Stream<TimeConstraint> getTimeConstraints(Interaction interaction, Element constrained) {
        if (interaction == null) {
            return Stream.empty();
        }

        return CacheAdapter.getInstance().getNonNavigableInverseReferences(constrained).stream()
                .filter(setting -> setting.getEStructuralFeature() == UMLPackage.Literals.CONSTRAINT__CONSTRAINED_ELEMENT)
                .map(setting -> (Constraint) setting.getEObject())
                .filter(TimeConstraint.class::isInstance)
                .filter(c -> c.getConstrainedElements().size() == 1)
                .filter(c -> getInteraction(c) == interaction)
                .map(TimeConstraint.class::cast);
    }

    /**
     * Get time observations in the contextual {@code interaction} that reference the given {@code observed} element.
     *
     * @param interaction
     *                    the contextual interaction
     * @param observed
     *                    the observed element
     *
     * @return its unique time constraints
     */
    public static Stream<TimeObservation> getTimeObservations(Interaction interaction, Element observed) {
        if (interaction == null) {
            return Stream.empty();
        }

        // These observations are contained by packages, so the interaction context
        // isn't
        // actually useful. It is specified for API consistency with other cases
        return CacheAdapter.getInstance().getNonNavigableInverseReferences(observed).stream()
                .filter(setting -> setting.getEStructuralFeature() == UMLPackage.Literals.TIME_OBSERVATION__EVENT)
                .map(setting -> (TimeObservation) setting.getEObject());
    }

    /**
     * <pre>
     * Check that given {@link OccurrenceSpecification} should be destroyed along with {@link ExecutionSpecification} which references it.
     * It should be destroyed in case:
     * It is of type {@link ExecutionOccurrenceSpecification} (since the opposite reference
     *   'ExecutionOccurrenceSpecification::execution[1]' which designates given {@link ExecutionSpecification} is mandatory).
     *   or
     * It is not used by another element.
     * </pre>
     *
     * @param es
     *           {@link ExecutionSpecification} which references {@link OccurrenceSpecification} (by means of
     *           #start/#finish references)
     * @param os
     *           start or finish {@link OccurrenceSpecification} which defines the duration of
     *           {@link ExecutionSpecification}
     * @return true in case {@link OccurrenceSpecification} should be destroyed
     */
    public static boolean shouldDestroyWithExecution(ExecutionSpecification es, OccurrenceSpecification os,
            ECrossReferenceAdapter crossReferenceAdapter) {
        return os != null //
                && !(os instanceof MessageOccurrenceSpecification)
                && (os instanceof ExecutionOccurrenceSpecification
                        // Strange case of unrelated event occurrence
                        || UMLService.isOnlyUsage(os, es, crossReferenceAdapter));
    }

    /**
     * Returns the element enclosing the {@code occurrenceSpecification}.
     * <p>
     * This element can be:
     * <ul>
     * <li>An {@link ExecutionSpecification} if the {@code occurrenceSpecification} is positioned between the
     * {@link ExecutionSpecification#getStart()} and {@link ExecutionSpecification#getFinish()} occurrences in the
     * enclosing {@link Interaction#getFragments()}, or if {@code occurrenceSpecification} is the start or finish of the
     * execution.</li>
     * <li>It's first covered {@link Lifeline} otherwise (see {@link OccurrenceSpecification#getCovereds()})</li>
     * </ul>
     * </p>
     *
     * @param occurrenceSpecification
     *                                the {@link OccurrenceSpecification} to retrieve the enclosing element from
     * @return the element enclosing the {@code occurrenceSpecification}
     *
     * @throws NullPointerException
     *                              if {@code occurrenceSpecification} is {@code null}
     */
    public static NamedElement findEnclosingElement(OccurrenceSpecification occurrenceSpecification) {
        Objects.requireNonNull(occurrenceSpecification);
        Lifeline coveredLifeline = occurrenceSpecification.getCovered();
        NamedElement enclosingElement = null;
        if (coveredLifeline != null) {
            enclosingElement = coveredLifeline;

            /*
             * Get all the enclosing fragments that cover the current Lifeline.
             */
            final List<InteractionFragment> fragments;
            if (occurrenceSpecification.getEnclosingInteraction() != null) {
                // The OccurrenceSpecification is directly contained in an Interaction, we look
                // for fragments inside the interaction.
                fragments = occurrenceSpecification.getEnclosingInteraction().getFragments().stream()
                        .filter(i -> i.getCovereds().contains(coveredLifeline)).toList();
            } else if (occurrenceSpecification.getEnclosingOperand() != null) {
                // The OccurrenceSpecification is directly contained in an InteractionOperand,
                // we look for fragments inside the operand.
                fragments = occurrenceSpecification.getEnclosingOperand().getFragments().stream()
                        .filter(i -> i.getCovereds().contains(coveredLifeline)).toList();
            } else {
                fragments = List.of();
            }

            ExecutionSpecification enclosingExecutionSpecification = getStartingOrFinishingExecution(
                    occurrenceSpecification);
            if (enclosingExecutionSpecification == null) {
                // The occurrence isn't the start/finish of an execution, iterate the fragments
                // to find the execution wrapping it.
                for (InteractionFragment fragment : fragments) {
                    if (fragment == occurrenceSpecification) {
                        break;
                    } else if (fragment instanceof ExecutionSpecification) {
                        enclosingExecutionSpecification = (ExecutionSpecification) fragment;
                    } else if (fragment instanceof OccurrenceSpecification occurrenceSpecificationFragment) {
                        if (enclosingExecutionSpecification != null && Objects
                                .equals(enclosingExecutionSpecification.getFinish(), occurrenceSpecificationFragment)) {
                            enclosingExecutionSpecification = null;
                        }
                    }
                }
            }

            if (enclosingExecutionSpecification != null) {
                // Return the enclosing ExecutionSpecification if we found one, otherwise return
                // the enclosing Lifeline
                enclosingElement = enclosingExecutionSpecification;
            }
        }
        return enclosingElement;
    }

    /**
     * Returns the {@link ExecutionSpecification} that has {@code occurrence} as its {@code start} or {@code finish}.
     *
     * @param occurrence
     *                   the {@link OccurrenceSpecification} to get the execution from
     * @return the {@link ExecutionSpecification} if it exists, {@code null} otherwise
     */
    private static ExecutionSpecification getStartingOrFinishingExecution(OccurrenceSpecification occurrence) {
        return getExecutionFromStartOccurrence(occurrence)
                .orElseGet(() -> getExecutionFromFinishOccurrence(occurrence).orElse(null));
    }

    /**
     * Returns {@code true} if {@code occurrence} is the start of an {@link ExecutionSpecification}.
     *
     * @param occurrence
     *                   the {@link OccurrenceSpecification} to check
     * @return {@code true} if {@code occurrence} is the start of an {@link ExecutionSpecification}
     * @see #getExecutionFromStartOccurrence(OccurrenceSpecification)
     */
    public static boolean isExecutionStartOccurrence(OccurrenceSpecification occurrence) {
        return getExecutionFromStartOccurrence(occurrence).isPresent();
    }

    /**
     * Returns {@code true} if {@code occurrence} is the finish of an {@link ExecutionSpecification}.
     *
     * @param occurrence
     *                   the {@link OccurrenceSpecification} to check
     * @return {@code true} if {@code occurrence} is the finish of an {@link ExecutionSpecification}
     * @see #getExecutionFromFinishOccurrence(OccurrenceSpecification)
     */
    public static boolean isExecutionFinishOccurrence(OccurrenceSpecification occurrence) {
        return getExecutionFromFinishOccurrence(occurrence).isPresent();
    }

    /**
     * Returns the {@link ExecutionSpecification} that contains {@code occurrence} as its start.
     *
     * @param occurrence
     *                   the {@link OccurrenceSpecification} to check
     * @return an {@link Optional} containing the {@link ExecutionSpecification}, or {@link Optional#empty()}
     * @see #isExecutionStartOccurrence(OccurrenceSpecification)
     */
    public static Optional<ExecutionSpecification> getExecutionFromStartOccurrence(OccurrenceSpecification occurrence) {
        return getExecutionFromOccurrence(occurrence, UMLPackage.eINSTANCE.getExecutionSpecification_Start());
    }

    /**
     * Returns the {@link ExecutionSpecification} that contains {@code occurrence} as its finish.
     *
     * @param occurrence
     *                   the {@link OccurrenceSpecification} to check
     * @return an {@link Optional} containing the {@link ExecutionSpecification}, or {@link Optional#empty()}
     * @see #isExecutionFinishOccurrence(OccurrenceSpecification)
     */
    public static Optional<ExecutionSpecification> getExecutionFromFinishOccurrence(
            OccurrenceSpecification occurrence) {
        return getExecutionFromOccurrence(occurrence, UMLPackage.eINSTANCE.getExecutionSpecification_Finish());
    }

    /**
     * Returns the {@link ExecutionSpecification} that contains {@code occurrence} in its {@code executionReference}.
     *
     * @param occurrence
     *                           the {@link OccurrenceSpecification} to check
     * @param executionReference
     *                           the {@link EReference} to check
     * @return an {@link Optional} containing the {@link ExecutionSpecification}, or {@link Optional#empty()}
     */
    private static Optional<ExecutionSpecification> getExecutionFromOccurrence(OccurrenceSpecification occurrence,
            EReference executionReference) {
        // Cross referencer is required for transformation:
        // Element may not be in the same container.
        ECrossReferenceAdapter crossReferenceAdapter = ECrossReferenceAdapter.getCrossReferenceAdapter(occurrence);
        Collection<Setting> settings = crossReferenceAdapter.getInverseReferences(occurrence, executionReference, true);
        return settings.stream() // Reference
                .map(Setting::getEObject) // source
                .filter(ExecutionSpecification.class::isInstance) // adapt
                .map(ExecutionSpecification.class::cast) // collect
                .findFirst();
    }
}
