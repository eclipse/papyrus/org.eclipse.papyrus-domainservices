/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA List, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.List;

import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.NamedElement;

/**
 * This helper provides interesting features for DurationObservation objects.
 * Copy from
 * {@link org.eclipse.papyrus.uml.diagram.common.helper.DurationObservationHelper}
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class DurationObservationHelper {

    /**
     * Get the list of all DurationObservation observing duration from or to an
     * element.
     *
     * @deprecated Use UMLTemporalHelper instead.
     * @param element
     *                              the observed element
     * @param crossReferenceAdapter
     *                              an adapter used to get inverse references
     *
     * @return list of DurationObservation
     */
    @Deprecated
    public static List<DurationObservation> getDurationObservationsOn(NamedElement element,
            ECrossReferenceAdapter crossReferenceAdapter) {
        return UMLTemporalHelper.getDurationObservations(element, crossReferenceAdapter);
    }
}
