/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;

/**
 * This service class includes all services used for ValueSpecification elements
 * like {@link LiteralInteger}, {@link LiteralReal}, {@link LiteralUnlimitedNatural}.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PropertiesValueSpecificationServices {

	/**
	 * The name of the "Value" feature for a {@link LiteralInteger}, {@link LiteralReal}
	 * and {@link LiteralUnlimitedNatural}.
	 */
	private static final String VALUE_FEATURE_NAME = "value"; //$NON-NLS-1$

	/**
	 * The service class used to get or compute a value.
	 */
    private final PropertiesUMLServices propertiesUMLServices;

	/**
	 * The service class used to set or update a value.
	 */
    private final PropertiesCrudServices propertiesCrudServices;

    public PropertiesValueSpecificationServices(ILogger logger, IEditableChecker checker) {
        super();
        this.propertiesUMLServices = new PropertiesUMLServices(logger);
        this.propertiesCrudServices = new PropertiesCrudServices(logger, checker);
    }

    /**
     * Set {@link LiteralInteger} value feature with a given value.
     * 
     * @param target
     *                 the {@link LiteralInteger} to modify
     * @param newValue
     *                 the new value to set
     * @return <code>true</code> if the value feature of {@link LiteralInteger} is
     *         set, <code>false</code> otherwise.
     */
    public boolean setLiteralIntegerValue(LiteralInteger target, Object newValue) {
        if (checkIntegerValue(newValue)) {
            Integer integerToSet = null;
            if (newValue instanceof Integer) {
                integerToSet = (Integer) newValue;
            } else if (newValue instanceof String) {
                integerToSet = propertiesUMLServices.convertStringToInteger(newValue);
            }
            return propertiesCrudServices.set(target, VALUE_FEATURE_NAME, integerToSet);
        } else
            return false;
	}

	/**
	 * Set {@link LiteralReal} value feature with a given value.
	 * 
	 * @param target
	 *            the {@link LiteralReal} to modify
	 * @param newValue
	 *            the new value to set
	 * @return <code>true</code> if the value feature of {@link LiteralReal} is set, <code>false</code> otherwise.
	 */
    public boolean setLiteralRealValue(LiteralReal target, Object newValue) {
        if (checkRealValue(newValue)) {
            Double realToSet = null;
            if (newValue instanceof Double) {
                realToSet = (Double) newValue;
            } else if (newValue instanceof String) {
                realToSet = propertiesUMLServices.convertStringToReal(newValue);
            }
            return propertiesCrudServices.set(target, VALUE_FEATURE_NAME, realToSet);
        } else
            return false;
	}

	/**
	 * Set {@link LiteralUnlimitedNatural} value feature with a given value.
	 * 
	 * @param target
	 *            the {@link LiteralUnlimitedNatural} to modify
	 * @param newValue
	 *            the new value to set
	 * @return <code>true</code> if the value feature of {@link LiteralUnlimitedNatural} is set, <code>false</code> otherwise.
	 */
    public boolean setLiteralUnlimitedNaturalValue(LiteralUnlimitedNatural target, Object newValue) {
        if (checkUnlimitedNaturalValue(newValue)) {
            Integer unlimitedNaturalToSet = null;
            if (newValue instanceof Integer) {
                unlimitedNaturalToSet = (Integer) newValue;
            } else if (newValue instanceof String) {
                unlimitedNaturalToSet = propertiesUMLServices.convertStringToIUnlimitedNatural(newValue);
            }
            return propertiesCrudServices.set(target, VALUE_FEATURE_NAME, unlimitedNaturalToSet);
        } else
            return false;
	}

	/**
     * Gets the value of the specified {@link LiteralInteger}.
     * 
     * @param target
     *               the {@link LiteralInteger} which contains the integer value
     * @return the integer value as String
     */
    public String getLiteralIntegerValue(LiteralInteger target) {
		return Integer.toString(target.getValue());
	}

	/**
     * Gets the value of the specified {@link LiteralReal}.
     * 
     * @param target
     *               the {@link LiteralReal} which contains the real value
     * @return the real value as String
     */
    public String getLiteralRealValue(LiteralReal target) {
		return Double.toString(target.getValue());
	}

	/**
     * Gets the value of the specified {@link LiteralUnlimitedNatural}.
     * 
     * @param target
     *               the {@link LiteralUnlimitedNatural} which contains the
     *               unlimited natural value
     * @return the unlimited natural value as String
     */
    public String getLiteralUnlimitedNaturalValue(LiteralUnlimitedNatural target) {
		return propertiesUMLServices.convertUnlimitedNaturalToString(Integer.valueOf(target.getValue()));
	}

	/**
	 * Checks if the new value used for the {@link LiteralInteger} is valid.
	 * 
	 * @param target
	 *            the {@link LiteralInteger} which contains the value to check
	 * @return <code>true</code> if the new value is valid; <code>false</code> otherwise.
	 */
    public boolean validateLiteralIntegerField(EObject target) {
        EStructuralFeature featureValue = target.eClass().getEStructuralFeature(VALUE_FEATURE_NAME);
        return checkIntegerValue(target.eGet(featureValue));
	}

	/**
	 * Checks if the new value used for the {@link LiteralReal} is valid.
	 * 
	 * @param target
	 *            the {@link LiteralReal} which contains the value to check
	 * @return <code>true</code> if the new value is valid; <code>false</code> otherwise.
	 */
	public boolean validateLiteralRealField(EObject target) {
		EStructuralFeature featureValue = target.eClass().getEStructuralFeature(VALUE_FEATURE_NAME);
        return checkRealValue(target.eGet(featureValue));
	}

	/**
	 * Checks if the new value used for the {@link LiteralUnlimitedNatural} is valid.
	 * 
	 * @param target
	 *            the {@link LiteralUnlimitedNatural} which contains the value to check
	 * @return <code>true</code> if the new value is valid; <code>false</code> otherwise.
	 */
    public boolean validateLiteralUnlimitedNaturalField(EObject target) {
        EStructuralFeature featureValue = target.eClass().getEStructuralFeature(VALUE_FEATURE_NAME);
        return checkUnlimitedNaturalValue(target.eGet(featureValue));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	private boolean checkIntegerValue(Object value) {
		boolean result = false;
		if (value instanceof Integer) {
			result = true;
		} else {
			try {
				// The actual entry is not an Integer.
				Integer integerValue = propertiesUMLServices.convertStringToInteger(value);
				result = integerValue != null;
				// CHECKSTYLE:OFF
			} catch (Exception e) {
				// CHECKSTYLE:ON
				return false;
			}
		}
		return result;
	}

	/**
	 * @param actualValue
	 * @return
	 */
	private boolean checkRealValue(Object value) {
		boolean result = false;
		if (value instanceof Double) {
			result = true;
		} else {
			// The actual entry is not a Real.
			try {
				Double real = propertiesUMLServices.convertStringToReal(value);
				result = real != null;
				// CHECKSTYLE:OFF
			} catch (Exception e) {
				// CHECKSTYLE:ON
				return false;
			}
		}
		return result;
	}

	/**
	 * @param newValue
	 * @return
	 */
	private boolean checkUnlimitedNaturalValue(Object value) {
		boolean result = false;
		if (value instanceof Integer) {
			result = true;
		} else if (value instanceof String) {
			if ("*".equals(value)) { //$NON-NLS-1$
				result = true;
			} else {
				try {
					// The actual entry is not an UnlimitedNatural. An UnlimitedNatural must be either -1, * or >= 0
					result = propertiesUMLServices.convertStringToIUnlimitedNatural(value) != null;
                    // CHECKSTYLE:OFF
				} catch (Exception e) {
                    // CHECKSTYLE:ON
					return false;
				}
			}
		}
		return result;
	}
}
