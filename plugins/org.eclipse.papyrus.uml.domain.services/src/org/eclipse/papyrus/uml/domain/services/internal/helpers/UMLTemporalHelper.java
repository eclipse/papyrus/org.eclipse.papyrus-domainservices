/*******************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides interesting features for DurationConstraint objects.
 * Copy from
 * {@link org.eclipse.papyrus.uml.diagram.common.helper.DurationConstraintHelper}
 *
 * @author <a href="mailto:nicolas.peransin@obeo.fr">Nicolas Peransin</a>
 */
public class UMLTemporalHelper {

    private static final UMLPackage UML = UMLPackage.eINSTANCE;

    /**
     * Descriptions of temporal relationship.
     */
    private enum TemporalRelation {

        /** Reference of TimeObservation. */
        timeObservation(TimeObservation.class, UML.getTimeObservation_Event()),
        /** Reference of DurationObservation. */
        durationObservation(DurationObservation.class, UML.getDurationObservation_Event()),
        /** Reference of TimeConstraint. */
        timeConstraint(TimeConstraint.class, UML.getConstraint_ConstrainedElement()),
        /** Reference of DurationConstraint. */
        durationConstraint(DurationConstraint.class, UML.getConstraint_ConstrainedElement()),

        /** Reference of TimeObservation and TimeConstraint. */
        timeElement(List.of(TimeConstraint.class, TimeObservation.class), UML.getConstraint_ConstrainedElement(),
                UML.getTimeObservation_Event()),
        /** Reference of DurationConstraint and DurationObservation. */
        durationElement(List.of(DurationConstraint.class, DurationObservation.class),
                UML.getConstraint_ConstrainedElement(), UML.getTimeObservation_Event()),

        /** Reference of all temporal indications. */
        all(List.of(TimeConstraint.class, DurationConstraint.class, TimeObservation.class, DurationObservation.class), // PackageableElement
                UML.getConstraint_ConstrainedElement(), UML.getDurationObservation_Event(),
                UML.getTimeObservation_Event());

        /** Filter based on type of referencing element. */
        private Predicate<EObject> typeFilter;
        /** Filter based on reference of referencing element. */
        private final Predicate<Setting> referencefilter;

        /**
         * Constructor with single type.
         *
         * @param type
         *                  class of the relation
         * @param reference
         *                  EMF reference
         */
        TemporalRelation(Class<? extends PackageableElement> type, EReference reference) {
            this(Collections.singletonList(type), reference);
        }

        /**
         * Constructor for multiple type.
         *
         * @param subTypes
         *                  classes of the relation
         * @param reference
         *                  EMF reference
         */
        TemporalRelation(List<? extends Class<? extends PackageableElement>> subTypes,
                EReference... references) {

            Collection<EReference> refs = Arrays.asList(references);
            referencefilter = setting -> refs.contains(setting.getEStructuralFeature());
            typeFilter = value -> {
                for (Class<?> subType : subTypes) {
                    if (subType.isInstance(value)) {
                        return true;
                    }
                }
                return false;
            };
        }

        @SuppressWarnings("unchecked") // private call will ensure compatibility
        private <R extends PackageableElement> List<R> getRelatedElements(NamedElement element,
                ECrossReferenceAdapter crossRef) {
            return (List<R>) crossRef.getInverseReferences(element, false) // references
                    .stream() // filtering
                    .filter(referencefilter) // right relation
                    .map(Setting::getEObject) // To target
                    .filter(typeFilter) // With expected type
                    .collect(Collectors.toList());
        }
    }

    /**
     * Returns all {@link DurationConstraint}s constraining a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of DurationConstraints
     */
    public static List<DurationConstraint> getDurationConstraints(NamedElement element,
            ECrossReferenceAdapter crossRef) {
        return TemporalRelation.durationConstraint.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all {@link TimeConstraint}s constraining a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of TimeConstraints
     */
    public static List<TimeConstraint> getTimeConstraints(NamedElement element, ECrossReferenceAdapter crossRef) {
        return TemporalRelation.timeConstraint.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all {@link DurationObservation}s constraining a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of DurationObservations
     */
    public static List<DurationObservation> getDurationObservations(NamedElement element,
            ECrossReferenceAdapter crossRef) {
        return TemporalRelation.durationObservation.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all {@link TimeObservation}s constraining a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of TimeObservations
     */
    public static List<TimeObservation> getTimeObservations(NamedElement element, ECrossReferenceAdapter crossRef) {
        return TemporalRelation.timeObservation.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all {@link TimeConstraint}s constraining a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of TimeConstraints
     */
    public static List<PackageableElement> getTemporalElements(NamedElement element, ECrossReferenceAdapter crossRef) {
        return TemporalRelation.all.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all time {@link PackageableElement}s related to a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of PackageableElement
     */
    public static List<PackageableElement> getTimeElements(NamedElement element, ECrossReferenceAdapter crossRef) {
        return TemporalRelation.timeElement.getRelatedElements(element, crossRef);
    }

    /**
     * Returns all duration {@link PackageableElement}s related to a given element.
     *
     * @param element
     *                 the constrained element
     * @param crossRef
     *                 an adapter used to get inverse references
     *
     * @return list of PackageableElement
     */
    public static List<PackageableElement> getDurationElements(NamedElement element, ECrossReferenceAdapter crossRef) {
        return TemporalRelation.durationElement.getRelatedElements(element, crossRef);
    }

}
