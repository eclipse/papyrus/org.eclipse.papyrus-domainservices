/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.uml2.uml.AcceptCallAction;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.ActionExecutionSpecification;
import org.eclipse.uml2.uml.AddStructuralFeatureValueAction;
import org.eclipse.uml2.uml.AddVariableValueAction;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ClearAssociationAction;
import org.eclipse.uml2.uml.ClearStructuralFeatureAction;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.CreateLinkAction;
import org.eclipse.uml2.uml.CreateLinkObjectAction;
import org.eclipse.uml2.uml.CreateObjectAction;
import org.eclipse.uml2.uml.DestroyLinkAction;
import org.eclipse.uml2.uml.DestroyObjectAction;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Pin;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ReadExtentAction;
import org.eclipse.uml2.uml.ReadIsClassifiedObjectAction;
import org.eclipse.uml2.uml.ReadLinkAction;
import org.eclipse.uml2.uml.ReadSelfAction;
import org.eclipse.uml2.uml.ReadStructuralFeatureAction;
import org.eclipse.uml2.uml.ReadVariableAction;
import org.eclipse.uml2.uml.ReclassifyObjectAction;
import org.eclipse.uml2.uml.ReduceAction;
import org.eclipse.uml2.uml.SendObjectAction;
import org.eclipse.uml2.uml.SendSignalAction;
import org.eclipse.uml2.uml.StartClassifierBehaviorAction;
import org.eclipse.uml2.uml.StartObjectBehaviorAction;
import org.eclipse.uml2.uml.TestIdentityAction;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UnmarshallAction;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.ValueSpecificationAction;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Generic implementation of {@link IElementConfigurer} to be used for most UML
 * element.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementConfigurer implements IElementConfigurer {

    /**
     * Initialize a semantic element.
     *
     * @param toInit
     *               the element to init
     * @param parent
     *               the context parent
     * @return the given element for fluent API use
     */
    @Override
    public EObject configure(EObject toInit, EObject parent) {
        if (toInit == null) {
            return null;
        }
        new ElementInitializerImpl(parent).doSwitch(toInit);
        return toInit;
    }

    static class ElementInitializerImpl extends UMLSwitch<Void> {

        private static final String TARGET_ELEMENT_NAME = "target";
        private static final String INSERT_AT_ELEMENT_NAME = "insertAt";
        private static final String VALUE_ELEMENT_NAME = "value";
        private static final String RESULT_ELEMENT_NAME = "result";
        private static final String OBJECT_ELEMENT_NAME = "object";
        private final EObject parent;

        ElementInitializerImpl(EObject parent) {
            super();
            this.parent = parent;
        }

        @Override
        public Void caseAcceptCallAction(AcceptCallAction acceptCallAction) {
            OutputPin returnInformation = UMLFactory.eINSTANCE.createOutputPin();
            returnInformation.setName("return information");
            acceptCallAction.setReturnInformation(returnInformation);
            return super.caseAcceptCallAction(acceptCallAction);
        }

        @Override
        public Void caseAddStructuralFeatureValueAction(
                AddStructuralFeatureValueAction addStructuralFeatureValueAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            InputPin value = UMLFactory.eINSTANCE.createInputPin();
            InputPin insertAt = UMLFactory.eINSTANCE.createInputPin();
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            value.setName(VALUE_ELEMENT_NAME);
            insertAt.setName(INSERT_AT_ELEMENT_NAME);
            result.setName(RESULT_ELEMENT_NAME);
            addStructuralFeatureValueAction.setObject(object);
            addStructuralFeatureValueAction.setValue(value);
            addStructuralFeatureValueAction.setInsertAt(insertAt);
            addStructuralFeatureValueAction.setResult(result);
            return super.caseAddStructuralFeatureValueAction(addStructuralFeatureValueAction);
        }

        @Override
        public Void caseAddVariableValueAction(AddVariableValueAction addVariableValueAction) {
            InputPin value = UMLFactory.eINSTANCE.createInputPin();
            InputPin insertAt = UMLFactory.eINSTANCE.createInputPin();
            value.setName(VALUE_ELEMENT_NAME);
            insertAt.setName(INSERT_AT_ELEMENT_NAME);
            addVariableValueAction.setValue(value);
            addVariableValueAction.setInsertAt(insertAt);
            return super.caseAddVariableValueAction(addVariableValueAction);
        }

        @Override
        public Void caseCallOperationAction(CallOperationAction callOperationAction) {
            InputPin target = UMLFactory.eINSTANCE.createInputPin();
            target.setName(TARGET_ELEMENT_NAME);
            callOperationAction.setTarget(target);
            return super.caseCallOperationAction(callOperationAction);
        }

        @Override
        public Void caseConstraint(Constraint constraint) {
            // Avoid overriding existing specification (specification is already set for
            // TimeConstraint, DurationConstraint, IntervalConstraint)
            if (constraint.getSpecification() == null) {
                OpaqueExpression spec = UMLFactory.eINSTANCE.createOpaqueExpression();
                spec.setName("constraintSpec"); //$NON-NLS-1$
                spec.getLanguages().add("OCL"); //$NON-NLS-1$
                spec.getBodies().add("true"); //$NON-NLS-1$
                constraint.setSpecification(spec);
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public Void caseClearAssociationAction(ClearAssociationAction clearAssociationAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            clearAssociationAction.setObject(object);
            return super.caseClearAssociationAction(clearAssociationAction);
        }

        @Override
        public Void caseClearStructuralFeatureAction(ClearStructuralFeatureAction clearStructuralFeatureAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            result.setName(RESULT_ELEMENT_NAME);
            clearStructuralFeatureAction.setObject(object);
            clearStructuralFeatureAction.setResult(result);
            return super.caseClearStructuralFeatureAction(clearStructuralFeatureAction);
        }

        @Override
        public Void caseCreateLinkAction(CreateLinkAction createLinkAction) {
            InputPin inputValue = UMLFactory.eINSTANCE.createInputPin();
            inputValue.setName(new ElementDefaultNameProvider().getDefaultName(inputValue, createLinkAction));
            createLinkAction.getInputValues().add(inputValue);
            return super.caseCreateLinkAction(createLinkAction);
        }

        @Override
        public Void caseCreateLinkObjectAction(CreateLinkObjectAction createLinkObjectAction) {
            InputPin inputValue = UMLFactory.eINSTANCE.createInputPin();
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            inputValue.setName(new ElementDefaultNameProvider().getDefaultName(inputValue, createLinkObjectAction));
            result.setName(RESULT_ELEMENT_NAME);
            createLinkObjectAction.getInputValues().add(inputValue);
            createLinkObjectAction.setResult(result);
            return super.caseCreateLinkObjectAction(createLinkObjectAction);
        }

        @Override
        public Void caseCreateObjectAction(CreateObjectAction createObjectAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            result.setName(RESULT_ELEMENT_NAME);
            createObjectAction.setResult(result);
            return super.caseCreateObjectAction(createObjectAction);
        }

        @Override
        public Void caseDestroyLinkAction(DestroyLinkAction destroyLinkAction) {
            InputPin inputValue = UMLFactory.eINSTANCE.createInputPin();
            inputValue.setName(new ElementDefaultNameProvider().getDefaultName(inputValue, destroyLinkAction));
            destroyLinkAction.getInputValues().add(inputValue);
            return super.caseDestroyLinkAction(destroyLinkAction);
        }

        @Override
        public Void caseDestroyObjectAction(DestroyObjectAction destroyObjectAction) {
            InputPin target = UMLFactory.eINSTANCE.createInputPin();
            target.setName(TARGET_ELEMENT_NAME);
            destroyObjectAction.setTarget(target);
            return super.caseDestroyObjectAction(destroyObjectAction);
        }

        /**
         * When a DurationConstraint is created, set its specification with a new
         * DurationInterval and initialize the min/max feature with Durations. These
         * Durations have both a LiteralInteger as expr.
         *
         * @see org.eclipse.papyrus.uml.diagram.timing.custom.edit.commands.CustomDurationConstraintCreateCommand
         *
         * @param durationConstraint
         *                           the element to configure
         * @return
         */
        @Override
        public Void caseDurationConstraint(DurationConstraint durationConstraint) {
            addParentToConstrained(durationConstraint);
            fillInterval(durationConstraint, UMLPackage.eINSTANCE.getDurationInterval(),
                    UMLPackage.eINSTANCE.getDuration(),
                    terminal -> ((Duration) terminal).setExpr(UMLFactory.eINSTANCE.createLiteralInteger()));
            return super.caseDurationConstraint(durationConstraint);
        }

        private void addParentToConstrained(Constraint constraint) {
            if (this.parent instanceof Element) {
                constraint.getConstrainedElements().clear();
                constraint.getConstrainedElements().add((Element) this.parent);
            }
        }

        /**
         * When an IntervalConstraint is created, set its specification with a new
         * Interval and initialize the min/max feature. There is no specific code for
         * the initialization of min/max features, this was done to homogenize code in
         * comparison to DurationConstraint and TimeConstraint.
         *
         * @see org.eclipse.papyrus.uml.service.types.helper.IntervalConstraintEditHelper
         *
         * @param intervalConstraint
         *                           the element to configure
         * @return
         */
        @Override
        public Void caseIntervalConstraint(IntervalConstraint intervalConstraint) {
            fillInterval(intervalConstraint, UMLPackage.eINSTANCE.getInterval(),
                    UMLPackage.eINSTANCE.getLiteralInteger(), null);
            return super.caseIntervalConstraint(intervalConstraint);
        }

        private ValueSpecification fillInterval(IntervalConstraint container, EClass intervalType,
                EClass terminalType, Consumer<ValueSpecification> terminalInit) {
            caseNamedElement(container);
            ValueSpecification existing = container.getSpecification();
            BiFunction<String, EClass, PackageableElement> creator = UMLHelper.getPackagedCreator(container);
            if (existing != null) { // exists or impossible to create.
                return existing;
            }

            Interval result = (Interval) UMLFactory.eINSTANCE.create(intervalType);
            container.setSpecification(result); // containment

            if (creator != null) {
                result.setMin((ValueSpecification) creator.apply("Min" + container.getName(), terminalType));
                result.setMax((ValueSpecification) creator.apply("Max" + container.getName(), terminalType));
                if (terminalInit != null) {
                    terminalInit.accept(result.getMin());
                    terminalInit.accept(result.getMax());
                }
            }

            return result;
        }

        @Override
        public Void caseReadExtentAction(ReadExtentAction readExtentAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            result.setName(RESULT_ELEMENT_NAME);
            readExtentAction.setResult(result);
            return super.caseReadExtentAction(readExtentAction);
        }

        @Override
        public Void caseReadIsClassifiedObjectAction(ReadIsClassifiedObjectAction readIsClassifiedObjectAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            result.setName(RESULT_ELEMENT_NAME);
            object.setName(OBJECT_ELEMENT_NAME);
            readIsClassifiedObjectAction.setResult(result);
            readIsClassifiedObjectAction.setObject(object);
            return super.caseReadIsClassifiedObjectAction(readIsClassifiedObjectAction);
        }

        @Override
        public Void caseReadLinkAction(ReadLinkAction readLinkAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            InputPin inputValue = UMLFactory.eINSTANCE.createInputPin();
            result.setName(RESULT_ELEMENT_NAME);
            inputValue.setName(new ElementDefaultNameProvider().getDefaultName(inputValue, readLinkAction));
            readLinkAction.setResult(result);
            readLinkAction.getInputValues().add(inputValue);
            return super.caseReadLinkAction(readLinkAction);
        }

        @Override
        public Void caseReadSelfAction(ReadSelfAction readSelfAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            result.setName(RESULT_ELEMENT_NAME);
            readSelfAction.setResult(result);
            return super.caseReadSelfAction(readSelfAction);
        }

        @Override
        public Void caseReadStructuralFeatureAction(ReadStructuralFeatureAction readStructuralFeatureAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            result.setName(RESULT_ELEMENT_NAME);
            object.setName(OBJECT_ELEMENT_NAME);
            readStructuralFeatureAction.setResult(result);
            readStructuralFeatureAction.setObject(object);
            return super.caseReadStructuralFeatureAction(readStructuralFeatureAction);
        }

        @Override
        public Void caseReadVariableAction(ReadVariableAction readVariableAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            result.setName(RESULT_ELEMENT_NAME);
            readVariableAction.setResult(result);
            return super.caseReadVariableAction(readVariableAction);
        }

        @Override
        public Void caseReclassifyObjectAction(ReclassifyObjectAction reclassifyObjectAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            reclassifyObjectAction.setObject(object);
            return super.caseReclassifyObjectAction(reclassifyObjectAction);
        }

        @Override
        public Void caseReduceAction(ReduceAction reduceAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            InputPin collection = UMLFactory.eINSTANCE.createInputPin();
            result.setName(RESULT_ELEMENT_NAME);
            collection.setName("collection");
            reduceAction.setResult(result);
            reduceAction.setCollection(collection);
            return super.caseReduceAction(reduceAction);
        }

        @Override
        public Void caseSendObjectAction(SendObjectAction sendObjectAction) {
            InputPin request = UMLFactory.eINSTANCE.createInputPin();
            InputPin target = UMLFactory.eINSTANCE.createInputPin();
            request.setName("request");
            target.setName(TARGET_ELEMENT_NAME);
            sendObjectAction.setRequest(request);
            sendObjectAction.setTarget(target);
            return super.caseSendObjectAction(sendObjectAction);
        }

        @Override
        public Void caseSendSignalAction(SendSignalAction sendSignalAction) {
            InputPin target = UMLFactory.eINSTANCE.createInputPin();
            target.setName(TARGET_ELEMENT_NAME);
            sendSignalAction.setTarget(target);
            return super.caseSendSignalAction(sendSignalAction);
        }

        @Override
        public Void caseStartClassifierBehaviorAction(StartClassifierBehaviorAction startClassifierBehaviorAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            startClassifierBehaviorAction.setObject(object);
            return super.caseStartClassifierBehaviorAction(startClassifierBehaviorAction);
        }

        @Override
        public Void caseStartObjectBehaviorAction(StartObjectBehaviorAction startObjectBehaviorAction) {
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            object.setName(OBJECT_ELEMENT_NAME);
            startObjectBehaviorAction.setObject(object);
            return super.caseStartObjectBehaviorAction(startObjectBehaviorAction);
        }

        @Override
        public Void caseTestIdentityAction(TestIdentityAction testIdentityAction) {
            InputPin first = UMLFactory.eINSTANCE.createInputPin();
            InputPin second = UMLFactory.eINSTANCE.createInputPin();
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            first.setName("first");
            second.setName("second");
            result.setName(RESULT_ELEMENT_NAME);
            testIdentityAction.setFirst(first);
            testIdentityAction.setSecond(second);
            testIdentityAction.setResult(result);
            return super.caseTestIdentityAction(testIdentityAction);
        }

        @Override
        public Void caseUnmarshallAction(UnmarshallAction unmarshallAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            InputPin object = UMLFactory.eINSTANCE.createInputPin();
            result.setName(new ElementDefaultNameProvider().getDefaultName(result, unmarshallAction));
            object.setName(OBJECT_ELEMENT_NAME);
            unmarshallAction.getResults().add(result);
            unmarshallAction.setObject(object);
            return super.caseUnmarshallAction(unmarshallAction);
        }

        /**
         * When a TimeConstraint is created, set its specification with a new
         * TimeInterval and initialize the min/max features with TimeExpressions. These
         * TimeExpressions have both a LiteralInteger as expr.
         *
         * @see org.eclipse.papyrus.uml.diagram.timing.custom.edit.commands.CustomTimeConstraintCreateCommand
         *
         * @param timeConstraint
         *                       the element to configure
         * @return
         */
        @Override
        public Void caseTimeConstraint(TimeConstraint timeConstraint) {
            addParentToConstrained(timeConstraint);
            fillInterval(timeConstraint, UMLPackage.eINSTANCE.getTimeInterval(),
                    UMLPackage.eINSTANCE.getTimeExpression(),
                    terminal -> ((TimeExpression) terminal).setExpr(UMLFactory.eINSTANCE.createLiteralInteger()));
            return super.caseTimeConstraint(timeConstraint);
        }

        @Override
        public Void caseValueSpecificationAction(ValueSpecificationAction valueSpecificationAction) {
            OutputPin result = UMLFactory.eINSTANCE.createOutputPin();
            result.setName(RESULT_ELEMENT_NAME);
            valueSpecificationAction.setResult(result);
            return super.caseValueSpecificationAction(valueSpecificationAction);
        }

        /**
         * Sets the name of the <code>actionExecutionSpecification</code> and creates
         * its start/finish {@link ExecutionOccurrenceSpecification}.
         * <p>
         * The created {@link ExecutionOccurrenceSpecification} are added to the
         * <code>actionExecutionSpecification</code> owner's fragments.
         *
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseActionExecutionSpecification(ActionExecutionSpecification)
         *
         * @param actionExecutionSpecification
         *                                     the {@link ActionExecutionSpecification}
         *                                     to configure
         * @return
         */
        @Override
        public Void caseActionExecutionSpecification(ActionExecutionSpecification actionExecutionSpecification) {
            /*
             * Need to get the name of the ExecutionSpecification first, otherwise we can't set the
             * ExecutionOccurrenceSpecification names.
             */
            caseNamedElement(actionExecutionSpecification);
            addExecutionEvent(actionExecutionSpecification, UMLPackage.eINSTANCE.getExecutionSpecification_Start());
            addExecutionEvent(actionExecutionSpecification, UMLPackage.eINSTANCE.getExecutionSpecification_Finish());

            return super.caseExecutionSpecification(actionExecutionSpecification);
        }

        private static void addExecutionEvent(ExecutionSpecification execution, EReference eventReference) {
            List<InteractionFragment> containment;
            if (execution.getEnclosingOperand() != null) {
                containment = execution.getEnclosingOperand().getFragments();
            } else if (execution.getEnclosingInteraction() != null) {
                containment = execution.getEnclosingInteraction().getFragments();
            } else { // no container
                return;
            }
            ExecutionOccurrenceSpecification end = UMLFactory.eINSTANCE.createExecutionOccurrenceSpecification();
            containment.add(end);
            String suffix = "Start";
            if (eventReference == UMLPackage.eINSTANCE.getExecutionSpecification_Finish()) {
                suffix = "Finish";
            }
            end.setName(execution.getName() + suffix);
            end.setExecution(execution);
            execution.eSet(eventReference, end);

            List<Lifeline> covereds = execution.getCovereds();
            if (!covereds.isEmpty()) {
                end.getCovereds().add(covereds.get(0));
            }
        }

        /**
         * UML spec indicates that an ExtensionEnd should have its Aggregation set to
         * "Composite".
         *
         * @param extensionEnd
         *                     the {@link ExtensionEnd} to configure
         */
        @Override
        public Void caseExtensionEnd(ExtensionEnd extensionEnd) {
            extensionEnd.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            return super.caseExtensionEnd(extensionEnd);
        }

        /**
         * See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.InteractionOperandEditHelper.getConfigureCommand(ConfigureRequest).
         */
        @Override
        public Void caseInteractionOperand(InteractionOperand interactionOperand) {
            interactionOperand.createGuard("guard");
            return super.caseInteractionOperand(interactionOperand);
        }

        @Override
        public Void casePin(Pin pin) {
            if (this.parent instanceof Action action) {
                pin.getInPartitions().addAll(action.getInPartitions());
            }
            return super.casePin(pin);
        }

        @Override
        public Void casePort(Port port) {
            port.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            return super.casePort(port);
        }

        @Override
        public Void caseUseCase(UseCase useCase) {
            Element owner = useCase.getOwner();
            if (owner instanceof Classifier) {
                ((Classifier) owner).getUseCases().add(useCase);
            }
            return super.caseUseCase(useCase);
        }

        @Override
        public Void caseProperty(Property property) {
            Element owner = property.getOwner();
            if (owner instanceof Collaboration) {
                ((Collaboration) owner).getCollaborationRoles().add(property);
            }
            return super.caseProperty(property);
        }

        @Override
        public Void caseNamedElement(NamedElement namedElement) {
            namedElement.setName(new ElementDefaultNameProvider().getDefaultName(namedElement, this.parent));
            return super.caseNamedElement(namedElement);
        }
    }
}
