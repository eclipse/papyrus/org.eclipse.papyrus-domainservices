/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, OBEO, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *  Aurelien Didier (Artal Technologies) - Issue 190
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Checks if a graphical D&D is possible in the Class diagram.
 *
 * @author Aurelien Didier
 */
public class StateMachineInternalSourceToRepresentationDropChecker
        implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new StateMachineDropOutsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class StateMachineDropOutsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private EObject newSemanticContainer;

        StateMachineDropOutsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseRegion(Region region) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof StateMachine || this.newSemanticContainer instanceof State)) {
                result = CheckStatus.no("Region can only be drag and drop on State or StateMachine.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseState(State state) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Region)) {
                result = CheckStatus.no("State can only be drag and drop on Region.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePseudostate(Pseudostate pseudostate) {
            final CheckStatus result;
            // Entry and Exit point can be on StateMachine or State
            if (pseudostate.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)
                    || pseudostate.getKind().equals(PseudostateKind.EXIT_POINT_LITERAL)) {
                if (!(this.newSemanticContainer instanceof Region || this.newSemanticContainer instanceof StateMachine
                        || this.newSemanticContainer instanceof State)) {
                    result = CheckStatus.no("Entry or Exit Point can only be drag and drop on StateMachine or State.");
                } else {
                    result = CheckStatus.YES;
                }
            }
            // Other Pseudostates are only on Region
            else {
                if (!(this.newSemanticContainer instanceof Region)) {
                    result = CheckStatus.no("This Pseudostate can only be drag and drop on Region.");
                } else {
                    result = CheckStatus.YES;
                }
            }
            return result;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            return this.handleContainer();
        }

        private CheckStatus handleContainer() {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof Region || this.newSemanticContainer instanceof StateMachine
                    || this.newSemanticContainer instanceof State)) {
                result = CheckStatus.no(MessageFormat.format("{0} can only be drag and drop on a StateMachine kind element.",
                        this.newSemanticContainer.eClass().getName()));
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }

    }
}
