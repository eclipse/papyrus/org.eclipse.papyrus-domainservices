/**
 * Copyright (c) 2014, 2023 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 */
package org.eclipse.papyrus.uml.domain.services.labels.domains;

/**
 * This class corresponds to preferences defined in
 * {@link org.eclipse.papyrus.uml.diagram.statemachine.custom.preferences.PreferenceConstants}.<br/>
 * The values are the default values initialized in
 * {@link org.eclipse.papyrus.uml.diagram.statemachine.custom.preferences.CustomTransitionPreferenceInitializer}.
 *
 * Constants for Transition preferences (use same constants as for CSS options)
 * 
 * @since 3.1
 * @author Ansgar Radermacher
 */
public class PreferenceConstants {
    /**
     * integer: Maximum length of displayed bodies.
     */
    public static final int BODY_CUT_LENGTH = 1;

    /**
     * Boolean: if true, indicate presence of parameters (attributes) by adding
     * (...) to call or signal event.
     */
    public static final Boolean INDICATE_PARAMETERS = true;

    /**
     * Boolean: if true, add a line-break to the transition label before the effect.
     */
    public static final Boolean LINEBREAK_BEFORE_EFFECT = true;
}
