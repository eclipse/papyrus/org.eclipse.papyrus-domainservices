/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA List, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import java.util.List;

import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.TimeObservation;

/**
 * This helper provides interesting features for TimeObservation objects. Copy
 * from
 * {@link org.eclipse.papyrus.uml.diagram.common.helper.TimeObservationHelper}
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class TimeObservationHelper {

    /**
     * Get the list of all TimeObservation observing a given element.
     *
     * @deprecated Use UMLTemporalHelper instead.
     * @param element
     *                              the observed element
     * @param crossReferenceAdapter
     *                              an adapter used to get inverse references
     *
     * @return list of TimeObservation
     */
    @Deprecated
    public static List<TimeObservation> getTimeObservations(NamedElement element,
            ECrossReferenceAdapter crossReferenceAdapter) {
        return UMLTemporalHelper.getTimeObservations(element, crossReferenceAdapter);
    }
}
