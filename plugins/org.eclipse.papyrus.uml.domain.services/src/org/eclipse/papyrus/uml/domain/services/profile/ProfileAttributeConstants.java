/*****************************************************************************
 * Copyright (c) 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Chokri Mraidha (CEA LIST) Chokri.Mraidha@cea.fr - Initial API and implementation
 *  Patrick Tessier (CEA LIST) Patrick.Tessier@cea.fr - modification
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

/**
 * Constant fields for profile version.<br/>
 * 
 * Copied from
 * org.eclipse.papyrus.uml.tools.profile.definition.IPapyrusVersionConstants
 * 
 * @author Chokri Mraidha
 */
public class ProfileAttributeConstants {

    /** source for eAnnotation that qualifies the profile definition. */
    public static final String PROFILE_EANNOTATION_SOURCE = "PapyrusVersion";

    /** key for version detail. */
    public static final String PROFILE_VERSION_KEY = "Version";

    /** key for author detail. */
    public static final String PROFILE_AUTHOR_KEY = "Author";

    /** key for copyright detail. */
    public static final String PROFILE_COPYRIGHT_KEY = "Copyright";

    /** key for date detail. */
    public static final String PROFILE_DATE_KEY = "Date";

    /** key for comment detail. */
    public static final String PROFILE_COMMENT_KEY = "Comment";

}
