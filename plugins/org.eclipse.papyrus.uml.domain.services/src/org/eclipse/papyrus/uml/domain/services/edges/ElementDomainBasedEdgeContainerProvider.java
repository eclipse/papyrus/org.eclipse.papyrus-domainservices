/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.eclipse.papyrus.uml.domain.services.EMFUtils.getAncestors;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ActivityEdgeHelper;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.util.UMLSwitch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Computes the semantic container of a domain based edge.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementDomainBasedEdgeContainerProvider implements IDomainBasedEdgeContainerProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElementDomainBasedEdgeContainerProvider.class);

    private final IEditableChecker editableChecker;

    public ElementDomainBasedEdgeContainerProvider(IEditableChecker editableChecker) {
        super();
        this.editableChecker = editableChecker;
    }

    /**
     * Gets the container of a semantic element represented as an Edge.
     *
     * @param semanticSource
     *                       the source of the edge
     * @param semanticTarget
     *                       the target of the edge
     * @param semanticEdge
     *                       the semantic element of the edge
     * @return a container or <code>null</code> is enable to compute a container
     */
    @Override
    public EObject getContainer(EObject semanticSource, EObject semanticTarget, EObject semanticEdge,
            IViewQuerier querier, Object sourceNode, Object targetNode) {
        EObject container = new ElementDomainBasedEdgeContainerProviderSwitch(semanticSource, semanticTarget,
                this.editableChecker, querier).doSwitch(semanticEdge);

        if (this.editableChecker.canEdit(container)) {
            return container;
        } else {
            return null;
        }
    }

    static class ElementDomainBasedEdgeContainerProviderSwitch extends UMLSwitch<EObject> {

        private final EObject source;

        private final EObject target;

        private final IEditableChecker editableChecker;

        private final IViewQuerier querier;

        ElementDomainBasedEdgeContainerProviderSwitch(EObject source, EObject target, IEditableChecker editableChecker,
                IViewQuerier querier) {
            super();
            this.source = source;
            this.target = target;
            this.editableChecker = editableChecker;
            this.querier = querier;
        }

        @Override
        public EObject caseActivityEdge(ActivityEdge activityEdge) {
            return new ActivityEdgeHelper().deduceContainer(this.source, this.target);
        }

        @Override
        public EObject caseAssociation(Association object) {
            EObject diagramElement = this.querier.getSemanticElement(this.querier.getDiagram());
            for (EObject parent = diagramElement; parent != null; parent = parent.eContainer())
                if (parent instanceof Package)
                    return parent;
            return null;
        }

        @Override
        public EObject caseComponentRealization(ComponentRealization object) {
            return this.target;
        }

        @Override
        public EObject caseConnector(Connector object) {

            List<StructuredClassifier> sourceStructureClassifiers = getAncestors(StructuredClassifier.class,
                    this.source);
            List<StructuredClassifier> targetStructureClassifiers = getAncestors(StructuredClassifier.class,
                    this.target);

            Iterator<StructuredClassifier> targetIte = targetStructureClassifiers.iterator();
            while (targetIte.hasNext()) {
                StructuredClassifier structuredClassifier = targetIte.next();
                if (sourceStructureClassifiers.contains(structuredClassifier)) {
                    return structuredClassifier;
                }
            }
            return null;

        }

        @Override
        public EObject caseDeployment(Deployment deployment) {
            return this.target;
        }

        @Override
        public EObject caseDirectedRelationship(DirectedRelationship object) {

            List<EReference> containmentRefCandidates = this.source.eClass().getEAllReferences().stream()//
                    .filter(ref -> ref.isContainment() && ref.getEType().isInstance(object))//
                    .collect(toList());

            EObject proposedContainer;

            if (containmentRefCandidates.isEmpty()) {

                // Propose a container.
                proposedContainer = EMFUtils.getLeastCommonContainer(Package.class, this.source, this.target);

                // If no common container is found try source nearest package
                if (proposedContainer == null || !this.editableChecker.canEdit(proposedContainer)) {
                    proposedContainer = EMFUtils.getAncestor(Package.class, this.source);
                }

                // If no common container is found try target nearest package
                if (proposedContainer == null || !this.editableChecker.canEdit(proposedContainer)) {
                    proposedContainer = EMFUtils.getAncestor(Package.class, this.target);
                }
            } else {
                if (containmentRefCandidates.size() > 1) {
                    LOGGER.warn("More than one containment reference candidates:"
                            + containmentRefCandidates.stream().map(EReference::getName).collect(joining(",")));
                }
                proposedContainer = this.source;
            }

            if (this.editableChecker.canEdit(proposedContainer)) {
                return proposedContainer;
            } else {
                return super.caseDirectedRelationship(object);
            }
        }

        @Override
        public EObject caseElementImport(ElementImport elementImport) {
            return this.source;
        }

        @Override
        public EObject caseExtend(Extend extend) {
            return this.source;
        }

        @Override
        public EObject caseExtension(Extension extension) {
            for (EObject element = this.source; element != null; element = element.eContainer()) {
                if (element instanceof Package) {
                    return element;
                }
            }
            return null;
        }

        @Override
        public EObject caseGeneralization(Generalization object) {
            return this.source;
        }

        @Override
        public EObject caseInclude(Include object) {
            return this.source;
        }

        @Override
        public EObject caseInterfaceRealization(InterfaceRealization object) {
            return this.source;
        }

        @Override
        public EObject caseMessage(Message message) {
            for (EObject element = this.source; element != null; element = element.eContainer()) {
                if (element instanceof Interaction) {
                    return element;
                }
            }
            return null;
        }

        @Override
        public EObject casePackageImport(PackageImport object) {
            return this.source;
        }

        @Override
        public EObject casePackageMerge(PackageMerge object) {
            return this.source;
        }

        @Override
        public EObject caseSubstitution(Substitution object) {
            return this.source;
        }

        @Override
        public EObject caseTransition(Transition transition) {
            Region container = EMFUtils.getAncestor(Region.class, this.source);
            if (null == container) {
                container = EMFUtils.getAncestor(Region.class, this.target);
                if (null == container) {
                    // If neither the source nor the Target are contained in a Region, create the
                    // transition in the first Region of StateMachine.
                    container = this.getFirstRegionStateMachine();
                }
            }
            return container;
        }

        private Region getFirstRegionStateMachine() {

            final EObject eContainer = this.source.eContainer();
            if (eContainer instanceof StateMachine) {
                final List<Region> regions = ((StateMachine) eContainer).getRegions();
                if (!regions.isEmpty()) {
                    return regions.get(0);
                }
            }
            return null;
        }
    }
}
