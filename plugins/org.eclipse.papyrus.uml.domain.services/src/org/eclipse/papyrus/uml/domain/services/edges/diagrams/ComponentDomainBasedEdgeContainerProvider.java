/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.edges.IDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.ConnectorHelper;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provides containers for elements in the Component diagram.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal Daniel</a>
 */
public class ComponentDomainBasedEdgeContainerProvider implements IDomainBasedEdgeContainerProvider {

    private final IDomainBasedEdgeContainerProvider delegate;

    /**
     * Initializes the provider with the given {@code delegate}.
     *
     * @param delegate
     *                 the container provider used to delegate calls
     */
    public ComponentDomainBasedEdgeContainerProvider(IDomainBasedEdgeContainerProvider delegate) {
        super();
        this.delegate = delegate;
    }

    /**
     * Build a {@link ComponentDomainBasedEdgeContainerProvider} with default
     * parameters.
     *
     * @param editableChecker
     *                        the {@link IEditableChecker}
     * @return the created {@link ComponentDomainBasedEdgeContainerProvider}
     */
    public static ComponentDomainBasedEdgeContainerProvider buildDefault(IEditableChecker editableChecker) {
        return new ComponentDomainBasedEdgeContainerProvider(
                new ElementDomainBasedEdgeContainerProvider(editableChecker));
    }

    @Override
    public EObject getContainer(EObject semanticSource, EObject semanticTarget, EObject semanticEdge,
            IViewQuerier querier, Object sourceNode, Object targetNode) {
        EObject container = new ComponentDomainBasedEdgeContainerProviderSwitch(querier, sourceNode, targetNode)
                .doSwitch(semanticEdge);

        // If the semantic Edge is a connector, we don't want to call the delegate if
        // the container is null. If the container is null, it means that we did not
        // find a common ancestor and the edge should not be created.
        if (container == null && !(semanticEdge instanceof Connector)) {
            return this.delegate.getContainer(semanticSource, semanticTarget, semanticEdge, querier, sourceNode,
                    targetNode);
        } else {
            return container;
        }
    }

    class ComponentDomainBasedEdgeContainerProviderSwitch extends UMLSwitch<EObject> {

        private final Object sourceView;

        private final Object targetView;

        private final IViewQuerier representationQuery;

        ComponentDomainBasedEdgeContainerProviderSwitch(IViewQuerier representationQuery, Object sourceView,
                Object targetView) {
            super();
            this.sourceView = sourceView;
            this.targetView = targetView;
            this.representationQuery = representationQuery;
        }

        @Override
        public EObject caseConnector(Connector connector) {
            ConnectorHelper connectorHelper = new ConnectorHelper();
            EObject connectorContainer = connectorHelper.getConnectorContainer(connector, this.representationQuery,
                    this.sourceView, this.targetView);
            if (connectorContainer != null) {
                return connectorContainer;
            }
            return super.caseConnector(connector);
        }
    }
}
