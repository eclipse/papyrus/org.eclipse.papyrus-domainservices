/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.scope;

import java.util.List;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Interface used to compute the list of root element in which candidate for a
 * reference can be located.
 * 
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public interface IRootCandidateSearchProvider {

    /**
     * Gets all reachable root elements containing reachable elements from self.
     * 
     * @param self
     *             the source of the search
     * @return list of root elements (can be {@link EObject} or {@link Resource}
     */
    List<Notifier> getReachableRoots(EObject self);

}
