/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;

/**
 * Object in charge of configuring a semantic element during its creation
 * process.
 *
 * @author Arthur Daussy
 */
public interface IElementConfigurer {

    /**
     * Configure a semantic element just after its creation.
     *
     * @param toConfigure
     *                    the element to configure
     * @param parent
     *                    the context parent
     * @return the given element for fluent API use
     */
    EObject configure(EObject toConfigure, EObject parent);

    /**
     * Implementation which does nothing, used for mocks in unit tests.
     *
     * @author Arthur Daussy
     */
    class NoOp implements IElementConfigurer {

        @Override
        public EObject configure(EObject toInit, EObject parent) {
            return null;
        }

    }

}
