/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - initial API and implementation
 *  Titouan BOUETE-GIRAUD (Artal Technologies) - titouan.bouete-giraud@artal.fr - Issue 17
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_ANGLE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.D_DOTS;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EQL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_ANGLE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SPACE;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.CollaborationUseLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.OperationLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ParameterLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.PropertyLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ReceptionLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.TransitionPropertiesParser;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ValueSpecificationLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.VisibilityLabelHelper;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.ConditionalNode;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExpansionKind;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InteractionConstraint;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionUse;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LoopNode;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Reception;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.SequenceNode;
import org.eclipse.uml2.uml.StateInvariant;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Object in charge of providing a label for a semantic element.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public final class ElementLabelProvider implements IViewLabelProvider {

    private ElementLabelProviderSwitch nameRenderer;

    private final Function<EObject, String> prefixLabelProvider;

    private final Function<EObject, String> keywordLabelProvider;

    private final String prefixSeparator;

    private final String keywordSeparator;

    private ElementLabelProvider(Builder builder) {
        if (builder.prefixLabelProvider != null) {
            this.prefixLabelProvider = builder.prefixLabelProvider;
        } else {
            this.prefixLabelProvider = e -> null;
        }
        if (builder.keywordLabelProvider != null) {
            this.keywordLabelProvider = builder.keywordLabelProvider;
        } else {
            this.keywordLabelProvider = e -> null;
        }
        INamedElementNameProvider nameProvider;
        if (builder.nameProvider == null) {
            nameProvider = new DefaultNamedElementNameProvider();
        } else {
            nameProvider = builder.nameProvider;
        }
        this.nameRenderer = new ElementLabelProviderSwitch(nameProvider);
        this.prefixSeparator = builder.prefixSeparator;
        this.keywordSeparator = builder.keywordSeparator;
    }

    public static ElementLabelProvider buildDefault() {
        return ElementLabelProvider.builder()//
                .withKeywordLabelProvider(new KeywordLabelProvider())//
                .withNameProvider(new DefaultNamedElementNameProvider())//
                .withPrefixLabelProvider(new StereotypeLabelPrefixProvider())//
                .build();
    }

    /**
     * Gets the label for the given element.
     *
     * @param element
     *                an element
     * @return a label (never returns <code>null</code> instead return an empty
     *         string)
     */
    @Override
    public String getLabel(EObject element) {
        if (element == null) {
            return ""; //$NON-NLS-1$
        }

        StringBuilder label = new StringBuilder();

        // add keywords
        String keyword = this.keywordLabelProvider.apply(element);
        if (keyword != null && !keyword.isBlank()) {
            label.append(keyword);
        }

        // add prefix
        String prefix = this.prefixLabelProvider.apply(element);
        if (prefix != null && !prefix.isBlank()) {
            if (label.length() > 0) {
                label.append(this.keywordSeparator);
            }
            label.append(prefix);
        }

        // add element name
        String baseLabel = this.nameRenderer.doSwitch(element);
        if (baseLabel != null && !baseLabel.isBlank()) {
            if (label.length() > 0) {
                label.append(this.prefixSeparator);
            }
            label.append(baseLabel);
        }

        return label.toString();
    }

    static final class ElementLabelProviderSwitch extends UMLSwitch<String> {

        private static final String REF = "ref";

        private static final String ASSIGNED = SPACE + EQL + SPACE;

        private static final String WEIGHT = "weight" + EQL;

        private static final String CONDITIONAL = ST_LEFT + "conditional" + ST_RIGHT;

        private static final String SEQUENCE = ST_LEFT + "sequence" + ST_RIGHT;

        private static final String LOOP_NODE = ST_LEFT + "loop node" + ST_RIGHT;

        private static final String STRUCTURED = ST_LEFT + "structured" + ST_RIGHT;

        private static final String JOIN_SPEC = "joinSpec" + ASSIGNED;

        private static final String NATURAL = "NATURAL";

        private static final String NULL_CONSTRAINT = "<NULL Constraint>";

        private static final String TIME_OBSERVATION = ASSIGNED + "now";

        private static final String DURATION_OBSERVATION = ASSIGNED + "duration";

        private static final String TYPED = SPACE + D_DOTS + SPACE;

        private CollaborationUseLabelHelper collaborationUseLabelHelper;

        private PropertyLabelHelper propertyLabelHelper;

        private ParameterLabelHelper parameterLabelHelper;

        private INamedElementNameProvider namedElementNameProvider;

        private VisibilityLabelHelper visibilityLabelHelper;

        private ValueSpecificationLabelHelper valueSpecificationHelper;

        private final OperationLabelHelper operationLabelHelper;

        private final ReceptionLabelHelper receptionLabelHelper;


        ElementLabelProviderSwitch(INamedElementNameProvider namedElementNameProvider) {
            this.namedElementNameProvider = Objects.requireNonNull(namedElementNameProvider);
            this.visibilityLabelHelper = new VisibilityLabelHelper();
            this.collaborationUseLabelHelper = new CollaborationUseLabelHelper(namedElementNameProvider,
                    this.visibilityLabelHelper);
            this.propertyLabelHelper = new PropertyLabelHelper(false, true, namedElementNameProvider,
                    this.visibilityLabelHelper);
            this.parameterLabelHelper = new ParameterLabelHelper(false, true, namedElementNameProvider);
            this.valueSpecificationHelper = new ValueSpecificationLabelHelper(namedElementNameProvider);
            this.operationLabelHelper = new OperationLabelHelper(this.parameterLabelHelper, this.visibilityLabelHelper,
                    this.namedElementNameProvider);
            this.receptionLabelHelper = new ReceptionLabelHelper(this.parameterLabelHelper, this.visibilityLabelHelper,
                    this.namedElementNameProvider);
        }

        @Override
        public String caseCollaborationUse(CollaborationUse collaborationUse) {
            return this.collaborationUseLabelHelper.getLabel(collaborationUse);
        }

        @Override
        public String caseCombinedFragment(CombinedFragment combinedFragment) {
            return combinedFragment.getInteractionOperator().getName();
        }

        @Override
        public String caseComment(Comment comment) {
            return comment.getBody();
        }

        @Override
        public String caseCommunicationPath(CommunicationPath communicationPath) {
            String result = "";
            String name = this.namedElementNameProvider.getName(communicationPath);
            if (communicationPath.isDerived()) {
                result += UMLCharacters.SLASH;
            }
            result += name;
            return result;
        }

        @Override
        public String caseInformationFlow(InformationFlow flow) {
            String result = "";
            if (flow != null) {
                result = this.getConveyeds(flow.getConveyeds());
                String flowName = this.namedElementNameProvider.getName(flow);
                if (flowName != null && !flowName.isBlank()) {
                    if (!result.isBlank()) {
                        result += EOL;
                    }
                    result += flowName;
                }
            }
            return result;
        }

        @Override
        public String caseInteractionOperand(InteractionOperand interactionOperand) {
            String result = "";
            InteractionConstraint guard = interactionOperand.getGuard();
            if (guard != null && guard.getSpecification() != null) {
                String specificationValue = this.valueSpecificationHelper
                        .getSpecificationValue(guard.getSpecification(), true);
                result += OPEN_ANGLE_BRACKET + specificationValue + CLOSE_ANGLE_BRACKET;
            }
            return result;
        }

        @Override
        public String caseInteractionUse(InteractionUse interactionUse) {
            return REF;
        }

        private String getConveyeds(EList<Classifier> conveyeds) {
            String result = "";
            if (!conveyeds.isEmpty()) {
                result += conveyeds.stream().map(NamedElement::getName).collect(Collectors.joining(", "));
            }
            return result;
        }

        @Override
        public String caseNamedElement(NamedElement object) {
            return this.namedElementNameProvider.getName(object);
        }

        @Override
        public String caseRegion(Region object) {
            return "";
        }

        @Override
        public String caseProperty(Property property) {
            return this.propertyLabelHelper.getLabel(property);
        }

        @Override
        public String caseParameter(Parameter parameter) {
            return this.parameterLabelHelper.getLabel(parameter);
        }

        /**
         * Copied from
         * org.eclipse.papyrus.uml.diagram.common.parser.ConstraintParser.getEditString(IAdaptable,
         * int)
         */
        @Override
        public String caseConstraint(Constraint constraint) {
            StringBuilder constLabel = new StringBuilder();
            constLabel.append(constraint.getName());
            constLabel.append(EOL);

            ValueSpecification value = constraint.getSpecification();
            String specLabel;
            if (value == null) {
                specLabel = NULL_CONSTRAINT;
            } else if (value instanceof OpaqueExpression expression) {
                if (!expression.getBodies().isEmpty()) {
                    String lang = expression.getLanguages().get(0);
                    specLabel = expression.getBodies().get(0);
                    if (!lang.isEmpty()) { // if language is available, prefix the label.
                        specLabel = encloseInBrackets(lang) + SPACE + specLabel;
                    }
                } else {
                    specLabel = encloseInBrackets(NATURAL) + SPACE;
                }
            } else {
                specLabel = valueSpecificationHelper.getSpecificationValue(value, false);
            }

            return encloseInBrackets(specLabel, constLabel).toString();
        }

        @Override
        public String caseOperation(Operation operation) {
            return this.operationLabelHelper.getLabel(operation);
        }

        @Override
        public String caseReception(Reception reception) {
            return this.receptionLabelHelper.getLabel(reception);
        }

        @Override
        public String caseStateInvariant(StateInvariant stateInvariant) {
            ValueSpecification value = null;
            if (stateInvariant.getInvariant() != null) {
                value = stateInvariant.getInvariant().getSpecification();
            }
            return getValueBasedLabel(stateInvariant, value);
        }

        @Override
        public String caseDurationObservation(DurationObservation object) {
            return namedElementNameProvider.getName(object) + DURATION_OBSERVATION;
        }

        @Override
        public String caseTimeObservation(TimeObservation object) {
            return namedElementNameProvider.getName(object) + TIME_OBSERVATION;
        }

        @Override
        public String caseIntervalConstraint(IntervalConstraint object) {
            // Time and Duration constraint.
            return getValueBasedLabel(object, object.getSpecification());
        }

        @Override
        public String caseTimeExpression(TimeExpression timeExpression) {
            StringBuilder constLabel = new StringBuilder();
            constLabel.append(this.namedElementNameProvider.getName(timeExpression));
            ValueSpecification expr = timeExpression.getExpr();
            if (expr != null) {
                constLabel.append(EQL);
                constLabel.append(this.valueSpecificationHelper.getSpecificationValue(expr, true));
            }
            return constLabel.toString();
        }

        @Override
        public String caseDuration(Duration duration) {
            StringBuilder constLabel = new StringBuilder();
            constLabel.append(this.namedElementNameProvider.getName(duration));
            ValueSpecification expr = duration.getExpr();
            if (expr != null) {
                constLabel.append(EQL);
                constLabel.append(this.valueSpecificationHelper.getSpecificationValue(expr, true));
            }
            return constLabel.toString();
        }

        @Override
        public String caseActivityPartition(ActivityPartition activityPartition) {
            String label;
            Element representedElement = activityPartition.getRepresents();
            if (representedElement instanceof NamedElement) {
                label = this.namedElementNameProvider.getName((NamedElement) representedElement);
            } else {
                label = super.caseActivityPartition(activityPartition);
            }
            return label;
        }

        @Override
        public String caseTransition(Transition transition) {
            return new TransitionPropertiesParser(this.namedElementNameProvider).getValueString(transition);
        }

        @Override
        public String caseExpansionRegion(ExpansionRegion expansionRegion) {
            ExpansionKind mode = expansionRegion.getMode();
            return ST_LEFT + mode.getName() + ST_RIGHT;
        }

        @Override
        public String caseJoinNode(JoinNode joinNode) {
            StringBuilder constLabel = new StringBuilder();
            ValueSpecification joinSpec = joinNode.getJoinSpec();
            if (joinSpec != null) {
                encloseInBrackets(JOIN_SPEC + this.valueSpecificationHelper.getSpecificationValue(joinSpec, true),
                        constLabel);
                constLabel.append(EOL);
            }
            constLabel.append(this.namedElementNameProvider.getName(joinNode));
            return constLabel.toString();
        }

        @Override
        public String caseStructuredActivityNode(StructuredActivityNode structuredActivityNode) {
            return STRUCTURED;
        }

        @Override
        public String caseLoopNode(LoopNode loopNode) {
            return LOOP_NODE;
        }

        @Override
        public String caseSequenceNode(SequenceNode sequenceNode) {
            return SEQUENCE;
        }

        @Override
        public String caseConditionalNode(ConditionalNode conditionalNode) {
            return CONDITIONAL;
        }

        @Override
        public String caseLifeline(Lifeline lifeline) {
            StringBuilder lifelineLabel = new StringBuilder();
            ConnectableElement connectableElement = lifeline.getRepresents();
            ValueSpecification selector = lifeline.getSelector();
            if (connectableElement == null) {

                String lifelineName = this.namedElementNameProvider.getName(lifeline);
                if (lifelineName != null) {
                    lifelineLabel.append(lifelineName);
                }

            } else {
                String connectableElementName = this.namedElementNameProvider.getName(connectableElement);
                if (connectableElementName != null) {
                    lifelineLabel.append(connectableElementName);
                }
                if (selector != null) {
                    lifelineLabel.append(OPEN_ANGLE_BRACKET);
                    lifelineLabel.append(this.valueSpecificationHelper.getSpecificationValue(selector, true));
                    lifelineLabel.append(CLOSE_ANGLE_BRACKET);
                }
                Type type = connectableElement.getType();
                if (type != null) {
                    lifelineLabel.append(TYPED);
                    lifelineLabel.append(this.namedElementNameProvider.getName(type));
                }
            }
            return lifelineLabel.toString();
        }

        @Override
        public String caseActivityEdge(ActivityEdge activityEdge) {
            StringBuilder objectFlowStringBuilder = new StringBuilder();
            ValueSpecification weight = activityEdge.getWeight();
            ValueSpecification guard = activityEdge.getGuard();
            objectFlowStringBuilder.append(this.namedElementNameProvider.getName(activityEdge));
            if (weight != null) {
                if (!objectFlowStringBuilder.isEmpty()) {
                    objectFlowStringBuilder.append(EOL);
                }
                encloseInBrackets(WEIGHT + valueSpecificationHelper.getSpecificationValue(weight, true),
                        objectFlowStringBuilder);
            }
            if (guard != null) {
                if (!objectFlowStringBuilder.isEmpty()) {
                    objectFlowStringBuilder.append(EOL);
                }
                objectFlowStringBuilder.append(OPEN_ANGLE_BRACKET);
                objectFlowStringBuilder.append(this.valueSpecificationHelper.getSpecificationValue(guard, true));
                objectFlowStringBuilder.append(CLOSE_ANGLE_BRACKET);
            }
            return objectFlowStringBuilder.toString();
        }

        private String getValueBasedLabel(NamedElement element, ValueSpecification value) {
            // Without content, show the name.
            if (value == null) {
                return namedElementNameProvider.getName(element);
            }
            return encloseInBrackets(valueSpecificationHelper.getSpecificationValue(value, false));
        }
    }

    private static StringBuilder encloseInBrackets(String content, StringBuilder builder) {
        builder.append(OPEN_BRACKET);
        builder.append(content);
        builder.append(CLOSE_BRACKET);
        return builder;
    }

    private static String encloseInBrackets(String content) {
        return encloseInBrackets(content, new StringBuilder()).toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private INamedElementNameProvider nameProvider;
        private Function<EObject, String> prefixLabelProvider;
        private Function<EObject, String> keywordLabelProvider;
        private String prefixSeparator = EOL;
        private String keywordSeparator = EOL;

        private Builder() {
        }

        /**
         * Provider of the base name of an element.
         *
         * @param aNameProvider
         * @return this for convenience.
         */
        public Builder withNameProvider(INamedElementNameProvider aNameProvider) {
            this.nameProvider = aNameProvider;
            return this;
        }

        /**
         * Provider of a prefix for the element label (optional).
         *
         * @param aPrefixLabelProvider
         * @return this for convenience.
         */
        public Builder withPrefixLabelProvider(Function<EObject, String> aPrefixLabelProvider) {
            this.prefixLabelProvider = aPrefixLabelProvider;
            return this;
        }

        /**
         * Provider of a keywords for the element label (optional).
         *
         * @param aKeywordLabelProvider
         * @return this for convenience.
         */
        public Builder withKeywordLabelProvider(Function<EObject, String> aKeywordLabelProvider) {
            this.keywordLabelProvider = aKeywordLabelProvider;
            return this;
        }

        /**
         * The string separator used between the prefix and the subsequent part of the
         * label.
         *
         * @param aPrefixSeparator
         * @return this for convenience.
         */
        public Builder withPrefixSeparator(String aPrefixSeparator) {
            this.prefixSeparator = aPrefixSeparator;
            return this;
        }

        /**
         * The string separator used between the keyword and the subsequent part of the
         * label.
         *
         * @param aKeywordSeparator
         * @return this for convenience.
         */
        public Builder withKeywordSeparator(String aKeywordSeparator) {
            this.keywordSeparator = aKeywordSeparator;
            return this;
        }

        public ElementLabelProvider build() {
            return new ElementLabelProvider(this);
        }
    }
}
