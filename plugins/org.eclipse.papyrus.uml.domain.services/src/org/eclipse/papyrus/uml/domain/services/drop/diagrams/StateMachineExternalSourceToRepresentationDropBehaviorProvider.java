/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, OBEO, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *  Aurelien Didier (Artal Technologies) - Issue 190
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * StateMachine Diagram Element (or the root of the diagram itself).
 *
 * @author Aurelien Didier
 */
public class StateMachineExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new StateMachineInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class StateMachineInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject newSemanticContainer;

        StateMachineInsideRepresentationBehaviorProviderSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof Region || this.newSemanticContainer instanceof StateMachine
                    || this.newSemanticContainer instanceof State) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseRegion(Region region) {
            if (this.newSemanticContainer instanceof State || this.newSemanticContainer instanceof StateMachine) {
                return DnDStatus.createNothingStatus(Set.of(region));
            }
            return super.caseRegion(region);
        }

        @Override
        public DnDStatus caseState(State state) {
            if (this.newSemanticContainer instanceof Region) {
                return DnDStatus.createNothingStatus(Set.of(state));
            }
            return super.caseState(state);
        }

        @Override
        public DnDStatus casePseudostate(Pseudostate pseudostate) {
            DnDStatus result = null;
            if (pseudostate.getKind().equals(PseudostateKind.ENTRY_POINT_LITERAL)
                    || pseudostate.getKind().equals(PseudostateKind.EXIT_POINT_LITERAL)) {
                if (this.newSemanticContainer instanceof Region || this.newSemanticContainer instanceof StateMachine
                        || this.newSemanticContainer instanceof State) {
                    result = DnDStatus.createNothingStatus(Set.of(pseudostate));
                }
            } else if (this.newSemanticContainer instanceof Region) {
                result = DnDStatus.createNothingStatus(Set.of(pseudostate));
            } else {
                result = super.casePseudostate(pseudostate);
            }
            return result;
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }
    }
}
