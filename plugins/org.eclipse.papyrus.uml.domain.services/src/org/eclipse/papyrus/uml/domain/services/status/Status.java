/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.status;

import org.eclipse.emf.ecore.EObject;

/**
 * General purpose status.
 * 
 * @author Arthur Daussy
 *
 */
public class Status {

    public static final Status DONE = new Status(State.DONE, null);

    private final State state;

    private final String message;

    public Status(State state, String message) {
        super();
        this.state = state;
        this.message = message;
    }

    public static Status createFailingStatus(String message) {
        return new Status(State.FAILED, message);
    }

    public static Status createOKStatus(EObject eObject) {
        return new Status(State.DONE, null);
    }

    public State getState() {
        return this.state;
    }

    public String getMessage() {
        return this.message;
    }

}
