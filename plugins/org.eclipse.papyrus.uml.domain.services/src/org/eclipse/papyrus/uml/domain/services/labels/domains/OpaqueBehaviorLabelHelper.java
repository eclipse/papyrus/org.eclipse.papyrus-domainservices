/*****************************************************************************
 * Copyright (c) 2008, 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Remi SCHNEKENBURGER (CEA LIST) Remi.schnekenburger@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import java.util.Iterator;

import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;

//CHECKSTYLE:OFF
/**
 * This class is
 * <li>copied from
 * {@link org.eclipse.papyrus.uml.domain.services.labels.domains.OpaqueBehaviorLabelHelper.utils.OpaqueBehaviorUtil}</li>
 * <li>copied and adapted from
 * {@link org.eclipse.papyrus.uml.diagram.statemachine.custom.parsers.OpaqueBehaviorViewUtil}</li>
 * 
 * Utility class for opaque behaviors that are partially shown in transitions or
 * entry/exit/do actions This class encapsulates an
 * <code>org.eclipse.uml2.uml.OpaqueBehavior</code><BR>
 */
public class OpaqueBehaviorLabelHelper {
//
    /**
     * Gets the value of the 'Body' attribute for the <code>OpaqueBehavior</code>
     * for a given language.
     *
     * @param language
     *                 the language in which the body is written
     * @return the value of 'Body' at the index i
     */
    // @unused
    public static String getBody(OpaqueBehavior behavior, String language) {
        int index = 0;
        String tmp = "";

        index = getBodyIndex(behavior, language);
        if ((index > -1) && (index < behavior.getBodies().size())) {
            tmp = behavior.getBodies().get(index);
        }

        return tmp;
    }

    /**
     * Returns the index of the body for a given language.
     *
     * @param language
     *                 the language of the body to find
     * @return the index of the body in the list or -1 if not found
     */
    public static int getBodyIndex(OpaqueBehavior behavior, String language) {
        int index = 0;
        boolean isFound = false;

        // test if the language exists
        Iterator<String> it = behavior.getLanguages().iterator();
        while (it.hasNext() && !isFound) {
            String lang = it.next();
            if (lang.equalsIgnoreCase(language)) {
                isFound = true;
            } else {
                index++;
            }
        }
        // returns -1 if not found
        if (!isFound) {
            index = -1;
        }
        return index;
    }

    public static final String DOTS = "..."; //$NON-NLS-1$
    public static final String PARAM_DOTS = "(...)"; //$NON-NLS-1$
    public static final String EMPTY_STRING = ""; //$NON-NLS-1$

    /**
     * Cut a body string after a predefined number of lines (taken from preference
     * store).
     *
     * @param body
     *             the body string
     * @return
     */
    public static String cutBodyString(String body) {
        int cutLength = PreferenceConstants.BODY_CUT_LENGTH;
        if (cutLength == 0) {
            return DOTS;
        } else {
            int start = 0;
            int newStart = 0;
            while (cutLength > 0) {
                // use "\n" instead of System.lineSeparator, since code embedded into a model
                // might not be destined for the development machine, e.g. contain eventually
                // only
                // \n, although the model is opened on a windows machine.
                newStart = body.indexOf("\n", start); //$NON-NLS-1$
                if (newStart > 0) {
                    cutLength--;
                    start = newStart + 1;
                } else {
                    return body;
                }
            }
            if (newStart > 0) {
                // handle case that \n is preceded by a \r
                if (newStart >= 1 && body.charAt(newStart - 1) == '\r') {
                    return body.substring(0, start - 1) + DOTS;
                }
                return body.substring(0, newStart) + DOTS;
            }
            return body;
        }
    }

    /**
     * Return the body of an opaque behavior. Retrieve the "Natural Language" body
     * with priority, i.e. return this body if it exists, otherwise return the first
     * body.
     *
     * @param exp
     *            an opaque expression
     * @return the associated body
     */
    public static String retrieveBody(OpaqueBehavior ob) {
        String body = OpaqueBehaviorLabelHelper.getBody(ob, "Natural Language"); //$NON-NLS-1$
        if (body.equals(EMPTY_STRING) && ob.getBodies().size() > 0) {
            body = ob.getBodies().get(0);
        }
        return cutBodyString(body);
    }

    /**
     * Return the body of an expression. Retrieve the "Natural Language" body with
     * priority, i.e. return this body if it exists, otherwise return the first
     * body.
     *
     * @param exp
     *            an opaque expression
     * @return the associated body
     */
    public static String retrieveBody(OpaqueExpression exp) {
        String body = OpaqueExpressionLabelHelper.getBodyForLanguage(exp, "Natural Language"); //$NON-NLS-1$
        if (body.equals(EMPTY_STRING)) {
            body = OpaqueExpressionLabelHelper.getBodyForLanguage(exp, null);
        }
        return cutBodyString(body);
    }

}
