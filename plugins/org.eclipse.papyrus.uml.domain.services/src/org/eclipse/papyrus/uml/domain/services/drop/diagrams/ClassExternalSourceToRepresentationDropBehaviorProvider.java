/*****************************************************************************
 * Copyright (c) 2024 CEA LIST, OBEO, Artal Technologies.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *  Aurelien Didier (Artal Technologies) - Issue 190
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Class Diagram Element (or the root of the diagram itself).
 *
 * @author Aurelien Didier
 */
public class ClassExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new ClassDropInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class ClassDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private EObject newSemanticContainer;

        ClassDropInsideRepresentationBehaviorProviderSwitch(EObject target) {
            this.newSemanticContainer = target;
        }

        @Override
        public DnDStatus caseClassifier(Classifier classifier) {
            if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Classifier) {
                return DnDStatus.createNothingStatus(Set.of(classifier));
            }
            return super.caseClassifier(classifier);
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseEnumerationLiteral(EnumerationLiteral enumerationLiteral) {
            if (this.newSemanticContainer instanceof Enumeration) {
                return DnDStatus.createNothingStatus(Set.of(enumerationLiteral));
            }
            return super.caseEnumerationLiteral(enumerationLiteral);
        }

        @Override
        public DnDStatus caseOperation(Operation operation) {
            EObject target = this.newSemanticContainer;
            if (target instanceof Operation) {
                target = target.eContainer();
            }
            if (target instanceof Class || target instanceof Interface
                    || (target instanceof DataType && !(target instanceof Enumeration))) {
                return DnDStatus.createNothingStatus(Set.of(operation));
            }
            return super.caseOperation(operation);
        }

        @Override
        public DnDStatus casePackage(Package pack) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(pack));
            }
            return super.casePackage(pack);
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            EObject target = this.newSemanticContainer;
            if (target instanceof Property) {
                target = target.eContainer();
            }
            boolean isPropertyHolder = target instanceof org.eclipse.uml2.uml.Class || target instanceof Interface
                    || target instanceof DataType || target instanceof Signal;
            if (isPropertyHolder && !(target instanceof Enumeration)) {
                return DnDStatus.createNothingStatus(Set.of(property));
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseRelationship(Relationship relationship) {
            return DnDStatus.createNothingStatus(Set.of(relationship));
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }
}
