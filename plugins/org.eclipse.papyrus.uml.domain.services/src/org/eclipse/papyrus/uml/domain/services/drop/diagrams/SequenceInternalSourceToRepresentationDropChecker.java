/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * SourceToRepresentationDropChecker for Sequence diagram.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class SequenceInternalSourceToRepresentationDropChecker implements IInternalSourceToRepresentationDropChecker {

    private static final String DROP_ERROR_MSG = "{0} can only be drag and drop on {1}.";

    private static final String COMA = ", ";

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new SequenceDropOutsideRepresentationCheckerSwitch(newSemanticContainer).doSwitch(elementToDrop);
    }

    static class SequenceDropOutsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private EObject newSemanticContainer;

        SequenceDropOutsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Interaction
                    || this.newSemanticContainer instanceof InteractionOperand) {
                result = CheckStatus.YES;
            } else {

                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, comment.eClass().getName(),
                        UMLPackage.eINSTANCE.getInteraction().getName() + COMA
                                + UMLPackage.eINSTANCE.getInteractionOperand().getName()));
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Interaction
                    || this.newSemanticContainer instanceof InteractionOperand) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(MessageFormat.format(DROP_ERROR_MSG, constraint.eClass().getName(),
                        UMLPackage.eINSTANCE.getInteraction().getName() + COMA
                                + UMLPackage.eINSTANCE.getInteractionOperand().getName()));
            }
            return result;
        }
    }
}
