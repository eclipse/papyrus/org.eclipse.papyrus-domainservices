/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.create.ElementConfigurer;
import org.eclipse.papyrus.uml.domain.services.create.ElementCreator;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Sequence Diagram Element.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class SequenceExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new SequenceDropInsideRepresentationBehaviorProviderSwitch(target, crossRef, editableChecker)
                .doSwitch(droppedElement);
    }

    static class SequenceDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject target;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        SequenceDropInsideRepresentationBehaviorProviderSwitch(EObject target, ECrossReferenceAdapter crossRef,
                IEditableChecker editableChecker) {
            super();
            this.target = target;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        @Override
        public DnDStatus caseProperty(Property property) {
            if (this.target instanceof Interaction || this.target instanceof Lifeline) {
                Lifeline lifeline = null;
                Set<EObject> resultStatusElements;
                if (this.target instanceof Interaction interaction) {
                    // Drop a Property on an Interaction will create a Lifeline and type the Lifeline
                    lifeline = createLifeline(interaction);
                    resultStatusElements = Set.of(lifeline);
                } else {
                    // Drop a Property on a Lifeline will type the Lifeline
                    lifeline = (Lifeline) this.target;
                    resultStatusElements = Collections.emptySet();
                }
                lifeline.setRepresents(property);

                return DnDStatus.createOKStatus(resultStatusElements);
            }
            return super.caseProperty(property);
        }

        @Override
        public DnDStatus caseType(Type type) {
            if (this.target instanceof Interaction || this.target instanceof Lifeline) {
                Lifeline lifeline = null;
                Property property = null;
                Set<EObject> resultStatusElements;
                if (this.target instanceof Interaction interaction) {
                    // Drop a Classifier on an Interaction : create a new lifeline, a new Property
                    // in the interaction typed by the dropped classifier and the new Lifeline will represent this property.
                    property = createProperty(interaction);
                    lifeline = createLifeline(interaction);
                    resultStatusElements = Set.of(lifeline);
                } else {
                    // Drop a Classifier on a Lifeline : create a new Property in the interaction
                    // typed by the dropped classifier and the target Lifeline will represent this property.
                    property = createProperty(EMFUtils.getAncestor(Interaction.class, this.target));
                    lifeline = (Lifeline) this.target;
                    resultStatusElements = Collections.emptySet();
                }
                property.setType(type);
                lifeline.setRepresents(property);

                return DnDStatus.createOKStatus(resultStatusElements);
            }
            return super.caseType(type);
        }

        private ElementCreator initElementCreator() {
            ElementFeatureModifier featureModifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
            ElementCreator elementCreator = new ElementCreator(new ElementConfigurer(), featureModifier);
            return elementCreator;
        }

        private Lifeline createLifeline(Interaction interaction) {
            ElementCreator elementCreator = initElementCreator();
            return (Lifeline) elementCreator.create(interaction, UMLPackage.eINSTANCE.getLifeline().getName(),
                    UMLPackage.eINSTANCE.getInteraction_Lifeline().getName()).getElement();
        }

        private Property createProperty(Interaction parentInteraction) {
            ElementCreator elementCreator = initElementCreator();
            return (Property) elementCreator.create(parentInteraction, UMLPackage.eINSTANCE.getProperty().getName(),
                    UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute().getName()).getElement();
        }

        @Override
        public DnDStatus defaultCase(EObject obj) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }
    }
}
