/*******************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import org.eclipse.emf.ecore.EObject;

/**
 * Gets the end label (usually source or target) of a domain based edge.
 * 
 * @author Arthur Daussy
 */
public interface IDomainBasedEdgeEndLabelProvider {

    /**
     * Gets the end label of the edge.
     * 
     * @param element
     *                    the semantic element of the edge
     * @param semanticEnd
     *                    semantic end used for the label (usually source or target)
     * @return a label
     */
    String getLabel(EObject element, EObject semanticEnd);
}
