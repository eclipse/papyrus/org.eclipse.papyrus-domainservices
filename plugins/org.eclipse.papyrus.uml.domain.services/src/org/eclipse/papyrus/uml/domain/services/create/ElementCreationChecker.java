/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Class in charge to check if the creation of a semanticObject is possible.
 * 
 * @author lfasani
 *
 */
public class ElementCreationChecker implements ICreatorChecker {

    @Override
    public CheckStatus canCreate(EObject parent, String type, String containmentReferenceName) {
        CheckStatus result = CheckStatus.YES;
        String message = null;
        if (parent == null) {
            message = "A parent container should be specified.";
        }
        if (type == null || type.isBlank()) {
            String prefix = Optional.ofNullable(message).map(msg -> msg + UMLCharacters.SPACE)
                    .orElse(UMLCharacters.EMPTY);
            message = prefix + "A type should be specified.";
        }
        if (containmentReferenceName == null || containmentReferenceName.isBlank()) {
            String prefix = Optional.ofNullable(message).map(msg -> msg + UMLCharacters.SPACE)
                    .orElse(UMLCharacters.EMPTY);
            message = prefix + "A reference should be specified.";
        }
        if (message == null) {
            result = genericChecking(parent, containmentReferenceName);
            if (result.isValid()) {
                EClass eClass = UMLHelper.toEClass(type);
                if (eClass != null) {
                    switch (eClass.getClassifierID()) {
                    case UMLPackage.REGION:
                        result = handleRegion(parent);
                        break;
                    default:
                        result = CheckStatus.YES;
                        break;
                    }
                }
            }
        } else {
            result = CheckStatus.no(message);
        }
        return result;
    }

    private CheckStatus genericChecking(EObject parent, String containmentReferenceName) {
        CheckStatus result = CheckStatus.YES;
        EStructuralFeature feature = parent.eClass().getEStructuralFeature(containmentReferenceName);
        if (feature != null) {
            Object featureContent = parent.eGet(feature);
            if (feature.isMany()) {
                if (feature.getUpperBound() == -1) {
                    result = CheckStatus.YES;
                } else {
                    if (((List<?>) featureContent).size() < feature.getUpperBound()) {
                        result = CheckStatus.YES;
                    } else {
                        result = CheckStatus.no(MessageFormat.format("{0} feature has already {1} elements.",
                                containmentReferenceName, feature.getUpperBound()));
                    }
                }
            } else {
                if (parent.eIsSet(feature)) {
                    result = CheckStatus
                            .no(MessageFormat.format("{0} feature is already set.", containmentReferenceName));
                } else {
                    result = CheckStatus.YES;
                }
            }
        } else {
            result = CheckStatus.no(MessageFormat.format("{0} feature has not been found for the EClass {1}.",
                    containmentReferenceName, parent.eClass().getName()));
        }
        return result;
    }

    private CheckStatus handleRegion(EObject parent) {
        CheckStatus result = CheckStatus.YES;
        if (parent instanceof FinalState) {
            result = CheckStatus.no("A Final State can not have a region.");
        }
        return result;
    }
}
