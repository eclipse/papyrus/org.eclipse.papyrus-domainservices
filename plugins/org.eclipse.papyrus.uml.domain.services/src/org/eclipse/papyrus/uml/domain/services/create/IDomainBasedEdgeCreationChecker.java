/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Object in charge of checking if a creation is possible for a domain based
 * edge.
 * 
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public interface IDomainBasedEdgeCreationChecker {

    /**
     * Check if we can create the semantic element for a domain based edge.
     *
     * @param semanticEdgeSource
     *                           the source of the edge
     * @param semanticEdgeTarget
     *                           the target of the edge
     * @param type
     *                           the semantic type
     * @param referenceName
     *                           the name of the containment reference
     * @param representionQuery
     *                           a {@link IViewQuerier}
     * @param sourceView
     *                           the representation of the semanticEdgeSource
     * @param targetView
     *                           the representation of the semanticEdgeTarget
     * @return a {@link CheckStatus}
     */
    CheckStatus canCreate(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type, String referenceName,
            IViewQuerier representionQuery, Object sourceView, Object targetView);

    /**
     * Check if we can create the edge from the given source.
     *
     * @param semanticEdgeSource
     *                           the source of the edge
     * @param type
     *                           the semantic type
     * @param referenceName
     *                           the name of the containment reference
     * @param representionQuery
     *                           a {@link IViewQuerier}
     * @param sourceView
     *                           the representation of the semanticEdgeSource
     * @return a {@link CheckStatus}
     */
    CheckStatus canCreateFromSource(EObject semanticEdgeSource, String type, String referenceName,
            IViewQuerier representionQuery, Object sourceView);

}
