/*******************************************************************************
 * Copyright (c) 2013, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

/**
 * A Helper class to helper manipulating UML multiplicities as Strings. (copied
 * from org.eclipse.papyrus.uml.tools.util.MultiplicityParser)
 *
 * @author Camille Letavernier
 * @author <a href="mailto:jerome.gout@obeosoft.com">Jerome Gout</a>
 *
 */
public class MultiplicityParser {

    /**
     * The 0..* multiplicity (Any).
     */
    public static final String ANY = "0..*"; //$NON-NLS-1$

    /**
     * The * multiplicity (Any) Equivalent to 0..*.
     */
    public static final String STAR = "*"; //$NON-NLS-1$

    /***
     * The 1 multiplicity (One).
     */
    public static final String ONE = "1"; //$NON-NLS-1$

    /**
     * The 0..1 multiplicity (Optional).
     */
    public static final String OPTIONAL = "0..1"; //$NON-NLS-1$

    /**
     * The 1..* multiplicity (One or more).
     */
    public static final String ONE_OR_MORE = "1..*"; //$NON-NLS-1$

    /**
     * The multiplicity separator (..).
     */
    public static final String SEPARATOR = ".."; //$NON-NLS-1$

    /**
     * Parse the multiplicity and returns an array of int values [lower, upper].
     *
     * @param value
     *              A string representing a multiplicity. Example of valid formats:
     *              1, 0..12, 1..*, *
     * @return null if the string value cannot be parsed
     */
    public static int[] getBounds(String value) {
        int[] bounds = null;
        if (value == null) {
            bounds = new int[] { 1, 1 }; // Default multiplicity is 1
        }
        String multiplicityValue = value;
        if (multiplicityValue.matches("([0-9]+\\.\\.)?([1-9][0-9]*|\\*)")) { // ([0-9]+..)?([1-9][0-9]*|\\*)
                                                                             // //$NON-NLS-1
            bounds = parseValidMultiplicity(multiplicityValue);
        }
        return bounds;
    }

    private static int[] parseValidMultiplicity(String multiplicityValue) {
        int[] bounds = null;
        if (multiplicityValue.equals(ANY) || multiplicityValue.equals(STAR)) {
            bounds = new int[] { 0, -1 };
        } else if (multiplicityValue.equals(OPTIONAL)) {
            bounds = new int[] { 0, 1 };
        } else if (multiplicityValue.equals(ONE_OR_MORE)) {
            bounds = new int[] { 1, -1 };
        } else if (multiplicityValue.equals(ONE)) {
            bounds = new int[] { 1, 1 };
        } else {
            try {
                int lower;
                int upper;
                if (multiplicityValue.contains(SEPARATOR)) {
                    lower = Integer.parseInt(multiplicityValue.substring(0, multiplicityValue.indexOf(SEPARATOR)));
                    String upperString = multiplicityValue.substring(
                            multiplicityValue.indexOf(SEPARATOR) + SEPARATOR.length(), multiplicityValue.length());
                    if (STAR.equals(upperString)) {
                        upper = -1;
                    } else {
                        upper = Integer.parseInt(upperString);
                    }
                } else {
                    lower = Integer.parseInt(multiplicityValue);
                    upper = lower;
                }
                bounds = new int[] { lower, upper };
            } catch (NumberFormatException ex) {
                // do nothing: return null by default
            }
        }
        return bounds;
    }

    public static String getMultiplicity(int lower, int upper) {
        String res = ""; //$NON-NLS-1$
        if (lower == 0 && upper == -1) {
            res = ANY;
        } else if (lower == 0 && upper == 1) {
            res = OPTIONAL;
        } else if (lower == 1 && upper == -1) {
            res = ONE_OR_MORE;
        } else if (lower == 1 && upper == 1) {
            res = ONE;
        } else if (lower == upper) {
            res = Integer.toString(lower);
        } else if (upper < 0) {
            res = lower + SEPARATOR + STAR;
        } else {
            res = lower + SEPARATOR + upper;
        }
        return res;
    }

    /**
     * Check if the {@link String} value is a valid representation of an UML
     * Multiplicity.
     *
     * @param value
     *              A {@link String} representing a multiplicity
     * @return <code>true</code> if the multiplicity has a valid format,
     *         <code>false</code> otherwise.
     */
    public static boolean isValidMultiplicity(String value) {
        int[] lowerUpper = getBounds(value);
        if (lowerUpper == null || lowerUpper.length < 2) {
            return false;
        }

        int lower = lowerUpper[0];
        int upper = lowerUpper[1];
        return isValidMultiplicity(lower, upper);
    }

    public static boolean isValidMultiplicity(int lower, int upper) {
        boolean isValid = true;
        if (lower < 0) {
            isValid = false;
        } else if ((upper > 0 && upper < lower) || upper == 0) {
            isValid = false;
        }
        return isValid;
    }

}
