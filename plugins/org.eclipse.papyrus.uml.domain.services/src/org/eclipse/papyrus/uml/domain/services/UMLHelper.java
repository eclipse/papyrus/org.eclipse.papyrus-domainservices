/*******************************************************************************
 * Copyright (c) 2022, 2024 CEA List, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services;

import java.util.function.BiFunction;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Gather helper methods around the UML meta model.
 *
 * @author Arthur Daussy
 */
public final class UMLHelper {

    private static final String UML_PACKAGE_PREFIX = "uml::"; //$NON-NLS-1$

    private UMLHelper() {
    }

    /**
     * Gets the {@link EClass} from the {@link UMLPackage} using the simple or
     * qualified name ("Class" vs "uml::Class").
     *
     * @param type
     *             the searched type
     * @return a {@link EClass} or <code>null</code>
     */
    public static EClass toEClass(String type) {
        if (type != null && type.startsWith(UML_PACKAGE_PREFIX)) {
            return toEClass(type.replace(UML_PACKAGE_PREFIX, "")); //$NON-NLS-1$
        }
        final EClass eClass;
        EClassifier classifier = UMLPackage.eINSTANCE.getEClassifier(type);
        if (classifier instanceof EClass) {
            eClass = (EClass) classifier;
        } else {
            eClass = null;
        }
        return eClass;
    }

    /**
     * Returns the closest container of {@link PackageableElement} in containment.
     * <p>
     * Result can be {@link Package} or {@link Component}.
     * </p>
     *
     * @param element
     *                object to get container from
     * @return {@link Package} or {@link Component} or null.
     */
    public static Namespace getPackagedContainer(Element element) {
        Namespace result = null;
        if (element instanceof Package pkg) {
            result = pkg;
        } else if (element instanceof Component cmp) {
            result = cmp;
        } else if (element != null) {
            result = getPackagedContainer(element.getOwner());
        }
        return result;
    }

    /**
     * Returns the closest {@link PackageableElement} containment list.
     * <p>
     * Result can be 'packagedElement' of {@link Package} or {@link Component}.
     * </p>
     *
     * @param element
     *                object to get container from
     * @return containment list or null.
     */
    public static EList<PackageableElement> getPackagedContainment(Element element) {
        Namespace container = getPackagedContainer(element);
        EList<PackageableElement> result = null;
        if (container instanceof Package pkg) {
            result = pkg.getPackagedElements();
        } else if (container instanceof Component cmp) {
            result = cmp.getPackagedElements();
        }
        return result;
    }

    /**
     * Returns a factory to create new {@link PackageableElement} in.
     *
     * @param element
     *                object to get container factory from
     * @return factory of PackageableElement
     */
    public static BiFunction<String, EClass, PackageableElement> getPackagedCreator(Element element) {
        Namespace container = getPackagedContainer(element);
        BiFunction<String, EClass, PackageableElement> result = null;
        if (container instanceof Package pkg) {
            result = (name, type) -> pkg.createPackagedElement(name, type);
        } else if (container instanceof Component cmp) {
            result = (name, type) -> cmp.createPackagedElement(name, type);
        }
        return result;
    }

}
