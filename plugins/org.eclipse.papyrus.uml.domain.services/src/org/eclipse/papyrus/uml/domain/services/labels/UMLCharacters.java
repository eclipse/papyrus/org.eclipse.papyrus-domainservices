/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

public final class UMLCharacters {

    public static final String CARD_SEP = ".."; //$NON-NLS-1$

    public static final String MANY = "*"; //$NON-NLS-1$

    public static final String CLOSE_ANGLE_BRACKET = "]"; //$NON-NLS-1$

    public static final String OPEN_ANGLE_BRACKET = "["; //$NON-NLS-1$

    public static final String EMPTY = ""; //$NON-NLS-1$

    public static final String BACK_SLASH = "\""; //$NON-NLS-1$

    public static final String TRUE = "true"; //$NON-NLS-1$

    public static final String COMMA = ","; //$NON-NLS-1$

    public static final String CLOSE_PARENTHESE = ")"; //$NON-NLS-1$

    public static final String OPEN_PARENTHESE = "("; //$NON-NLS-1$

    public static final String D_DOTS = ":"; //$NON-NLS-1$

    public static final String SLASH = "/"; //$NON-NLS-1$

    public static final String SPACE = " "; //$NON-NLS-1$

    public static final String EQL = "="; //$NON-NLS-1$

    // Do not use System line separator.
    // Linux and Windows must edit model in the same way.
    public static final String EOL = "\n"; //$NON-NLS-1$

    public static final String TILDE = "\u007E"; //$NON-NLS-1$

    public static final String CLOSE_BRACKET = "}"; //$NON-NLS-1$

    public static final String OPEN_BRACKET = "{"; //$NON-NLS-1$

    /** Open guillemet quote mark. */
    public static final String ST_LEFT = "\u00AB"; //$NON-NLS-1$

    /** Close guillemet quote mark. */
    public static final String ST_RIGHT = "\u00BB"; //$NON-NLS-1$

    /** Open quote mark. */
    public static final String Q_LEFT = "\u201C"; //$NON-NLS-1$

    /** Close quote mark. */
    public static final String Q_RIGHT = "\u201D"; //$NON-NLS-1$

    /** The * KeyWord to represent an unlimited integer (infinite). */
    public static final String UNLIMITED_KEYWORD = "*"; //$NON-NLS-1$

    /**
     * Less than sign.
     */
    public static final String LESS_THAN = "\u003C"; //$NON-NLS-1$

    /**
     * Greater than sign.
     */
    public static final String GREATER_THAN = "\u003E"; //$NON-NLS-1$

    private UMLCharacters() {
    }
}
