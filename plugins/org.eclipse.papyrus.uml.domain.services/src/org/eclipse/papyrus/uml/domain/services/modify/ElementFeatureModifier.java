/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import static java.util.stream.Collectors.toList;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.destroy.ElementDestroyer;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.CollaborationHelper;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.Action;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Most generic UML {@link IFeatureModifier}.
 *
 * @author Arthur Daussy
 *
 */
public class ElementFeatureModifier implements IFeatureModifier {

    private static final String CANNOT_EDIT_MSG = "Selected object cannot be edited";

    private final ECrossReferenceAdapter crossRef;

    private final IEditableChecker editableChecker;

    public ElementFeatureModifier(ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        super();
        this.crossRef = crossRef;
        this.editableChecker = editableChecker;
    }

    @Override
    public Status setValue(EObject featureOwner, String featureName, Object newValue) {
        if (!this.editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }

        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.SET, this.editableChecker,
                this.crossRef).doSwitch(featureOwner);
    }

    @Override
    public Status addValue(EObject featureOwner, String featureName, Object newValue) {
        if (!this.editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }
        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.ADD, this.editableChecker,
                this.crossRef).doSwitch(featureOwner);
    }

    @Override
    public Status removeValue(EObject featureOwner, String featureName, Object newValue) {
        if (!this.editableChecker.canEdit(featureOwner)) {
            return Status.createFailingStatus(CANNOT_EDIT_MSG);
        }
        return new ElementFeatureModifierSwitch(featureName, newValue, ModificationType.REMOVE, this.editableChecker,
                this.crossRef).doSwitch(featureOwner);
    }

    static class ElementFeatureModifierSwitch extends UMLSwitch<Status> {

        private final String featureName;

        private final Object newValue;

        private final ModificationType operationType;

        private IEditableChecker editableChecker;

        private ECrossReferenceAdapter crossRef;

        ElementFeatureModifierSwitch(String featureName, Object newValue, ModificationType operationType,
                IEditableChecker editableChecker, ECrossReferenceAdapter crossRef) {
            super();
            this.featureName = featureName;
            this.newValue = newValue;
            this.operationType = operationType;
            this.editableChecker = editableChecker;
            this.crossRef = crossRef;
        }

        /**
         * When an Action is created in an {@link ActivityPartition}, its Pins should
         * have the inPartition feature set to the specified ActivityPartition.
         *
         * @param activityPartition
         *                          the modified ActivityPartition
         * @return the status of the modification
         */
        @Override
        public Status caseActivityPartition(ActivityPartition activityPartition) {
            if (this.newValue instanceof Action action
                    && UMLPackage.eINSTANCE.getActivityPartition_Node().getName().equals(this.featureName)) {
                List<InputPin> inputs = action.getInputs();
                List<OutputPin> outputs = action.getOutputs();
                if (ModificationType.ADD.equals(this.operationType)
                        || ModificationType.SET.equals(this.operationType)) {
                    for (InputPin inputPin : inputs) {
                        inputPin.getInPartitions().add(activityPartition);
                    }
                    for (OutputPin outputPin : outputs) {
                        outputPin.getInPartitions().add(activityPartition);
                    }
                } else {
                    for (InputPin inputPin : inputs) {
                        inputPin.getInPartitions().remove(activityPartition);
                    }
                    for (OutputPin outputPin : outputs) {
                        outputPin.getInPartitions().remove(activityPartition);
                    }
                }
            }
            return super.caseActivityPartition(activityPartition);
        }

        @Override
        public Status caseComponent(Component object) {
            if (this.newValue instanceof Component
                    && UMLPackage.eINSTANCE.getClass_NestedClassifier().getName().equals(this.featureName)) {
                return Status.createFailingStatus("Cannot create a Component under another Component using the "
                        + this.featureName + " feature.");
            }
            return super.caseComponent(object);
        }

        @Override
        public Status caseProperty(Property object) {

            this.setNoneLiteralToMemberEndsAggregation(object);

            boolean isNullTypeSet = this.deleteAssociationWithNullType(object);

            this.deleteConnectorEndPartWithRole(object);

            if (!isNullTypeSet) {
                // in case of type set to null, setting type was mandatory before several action
                // so setting type is not launch at the end.
                return super.caseProperty(object);
            } else {
                return Status.DONE;
            }
        }

        /**
         * See
         * org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeSetCommand(SetRequest)
         *
         * For a property referenced by an association, we ensure that all properties of
         * this association are not "shared" or "composite". Only one member end can be
         * "shared" or "composite".
         *
         * @param property
         *                 the property to edit
         */
        private void setNoneLiteralToMemberEndsAggregation(Property property) {
            if (UMLPackage.eINSTANCE.getProperty_Aggregation().getName().equals(this.featureName)
                    && !(property instanceof Port) && this.newValue != AggregationKind.NONE_LITERAL) {
                Association association = property.getAssociation();
                if (association != null) {
                    Set<Property> members = new HashSet<>();
                    members.addAll(association.getMemberEnds());
                    members.remove(property);
                    ElementFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
                    for (Property member : members) {
                        if (member.getAggregation() != AggregationKind.NONE_LITERAL) {
                            // Two member ends of an association cannot be set to composite at the same
                            // time. To avoid such a situation, other ends are changed into aggregation
                            // "none".
                            modifier.setValue(member, UMLPackage.eINSTANCE.getProperty_Aggregation().getName(),
                                    AggregationKind.NONE_LITERAL);
                        }
                    }
                }
            }
        }

        /**
         * See
         * org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeSetCommand(SetRequest)
         *
         * Type set to null implies the property should be kept and the association
         * deleted: see https://bugs.eclipse.org/bugs/show_bug.cgi?id=477724
         *
         * @param property
         *                 the property to edit
         */
        private boolean deleteAssociationWithNullType(Property property) {
            boolean isNullTypeSet = false;
            if (UMLPackage.eINSTANCE.getTypedElement_Type().getName().equals(this.featureName)
                    && !(property instanceof Port) && this.newValue == null) {
                // Type set to null implies the property should be kept and the association
                // deleted: see https://bugs.eclipse.org/bugs/show_bug.cgi?id=477724
                List<Association> associationRefs = this.crossRef
                        .getInverseReferences(property, UMLPackage.eINSTANCE.getAssociation_MemberEnd(), true).stream()
                        .map(s -> (Association) s.getEObject())//
                        .collect(toList());

                // Type set before destroy association to avoid to delete this property with
                // association
                property.eSet(UMLPackage.eINSTANCE.getTypedElement_Type(), this.newValue);
                isNullTypeSet = true;
                ElementDestroyer deleter = ElementDestroyer.buildDefault(this.crossRef, this.editableChecker);
                for (Association association : associationRefs) {
                    if (association.getMembers().size() <= 2) {
                        deleter.destroy(association);
                    }
                }
            }
            return isNullTypeSet;
        }

        /**
         * When the type of a property change we need to delete all ConnectorEnds with
         * portWithPart referencing the given property
         *
         * @param property
         *                 a Property
         */
        private void deleteConnectorEndPartWithRole(Property property) {
            if (UMLPackage.eINSTANCE.getTypedElement_Type().getName().equals(this.featureName)
                    && !(property instanceof Port)) {

                List<ConnectorEnd> connectorEnds = this.crossRef
                        .getInverseReferences(property, UMLPackage.eINSTANCE.getConnectorEnd_PartWithPort(), true)
                        .stream().map(s -> (ConnectorEnd) s.getEObject())//
                        .collect(toList());

                ElementDestroyer deleter = ElementDestroyer.buildDefault(this.crossRef, this.editableChecker);

                List<Property> validProperties = new ArrayList<>();
                if (this.newValue instanceof StructuredClassifier) {
                    StructuredClassifier strucClassifier = (StructuredClassifier) this.newValue;
                    validProperties = strucClassifier.allAttributes();
                } else if (this.newValue instanceof DataType) {
                    validProperties = ((DataType) this.newValue).allAttributes();
                } else {
                    validProperties = List.of();
                }
                for (ConnectorEnd connectorEnd : connectorEnds) {
                    if (connectorEnd.getRole() == null || !validProperties.contains(connectorEnd.getRole())) {
                        deleter.destroy(connectorEnd);
                    }
                }
            }
        }

        /**
         * When removing a ConnectableElement from the Collaboration#collaborationRole
         * feature, destroy the related role bindings.
         *
         * @see org.eclipse.papyrus.uml.service.types.helper.advice.CollaborationHelperAdvice.getBeforeSetCommand(SetRequest)
         *
         * @param collaboration
         *                      the modified collaboration
         */
        @Override
        public Status caseCollaboration(Collaboration collaboration) {
            if (UMLPackage.eINSTANCE.getCollaboration_CollaborationRole().getName().equals(this.featureName)
                    && ModificationType.REMOVE.equals(this.operationType)
                    && this.newValue instanceof ConnectableElement) {
                Set<Dependency> relatedRoleBindings = CollaborationHelper.getRelatedRoleBindings(collaboration,
                        (ConnectableElement) this.newValue, this.crossRef);
                ElementDestroyer deleter = ElementDestroyer.buildDefault(this.crossRef, this.editableChecker);
                for (Dependency roleBinding : relatedRoleBindings) {
                    deleter.destroy(roleBinding);
                }
            }
            return super.caseCollaboration(collaboration);
        }

        @Override
        public Status caseCollaborationUse(CollaborationUse collaborationUse) {
            if (UMLPackage.eINSTANCE.getCollaborationUse_Type().getName().equals(this.featureName)
                    && collaborationUse.getType() != this.newValue
                    && (this.newValue instanceof Collaboration || this.newValue == null)) {
                List<Dependency> roleBindings = new ArrayList<>(collaborationUse.getRoleBindings());
                ElementDestroyer deleter = ElementDestroyer.buildDefault(this.crossRef, this.editableChecker);
                for (Dependency roleBinding : roleBindings) {
                    deleter.destroy(roleBinding);
                }
            }
            return super.caseCollaborationUse(collaborationUse);
        }

        /**
         * If the last InteractionOperand of a CombinedFragment is removed, remove the
         * CombinedFragment. See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.advice.InteractionOperandEditHelperAdvice.
         *
         *
         * @param combinedFragment
         *                         the modified combinedFragment
         * @return the status of the modification
         */
        @Override
        public Status caseCombinedFragment(CombinedFragment combinedFragment) {
            if (UMLPackage.eINSTANCE.getCombinedFragment_Operand().getName().equals(this.featureName)
                    && this.newValue instanceof InteractionOperand
                    && ModificationType.REMOVE.equals(this.operationType)) {
                if (combinedFragment.getOperands().size() == 1
                        && combinedFragment.getOperands().get(0) == this.newValue) {
                    ElementDestroyer deleter = ElementDestroyer.buildDefault(this.crossRef, this.editableChecker);
                    deleter.destroy((InteractionOperand) this.newValue);
                }
            }
            return super.caseCombinedFragment(combinedFragment);
        }

        /**
         * If an InteractionOperand is created under another InteractionOperand, it must
         * be set as a sibling, not a child. See Papyrus behavior:
         * org.eclipse.papyrus.uml.service.types.helper.advice.InteractionOperandEditHelperAdvice.
         *
         * @param interactionOperand
         *                           the edited operand
         * @return the modifier status
         */
        @Override
        public Status caseInteractionOperand(InteractionOperand interactionOperand) {
            if (UMLPackage.eINSTANCE.getInteractionOperand_Fragment().getName().equals(this.featureName)
                    && this.newValue instanceof InteractionOperand
                    && interactionOperand.getOwner() instanceof CombinedFragment) {
                CombinedFragment combinedFragment = (CombinedFragment) interactionOperand.getOwner();
                ElementFeatureModifier modifier = new ElementFeatureModifier(this.crossRef, this.editableChecker);
                return modifier.addValue(combinedFragment, UMLPackage.eINSTANCE.getCombinedFragment_Operand().getName(),
                        this.newValue);
            }
            return super.caseInteractionOperand(interactionOperand);
        }

        @Override
        public Status defaultCase(EObject object) {

            EClass eClass = object.eClass();
            EStructuralFeature feature = eClass.getEStructuralFeature(this.featureName);

            final Status result;
            if (feature == null) {
                result = Status.createFailingStatus(
                        MessageFormat.format("Unkown feature {0} on {1}", this.featureName, eClass.getName()));
            } else if (feature.isDerived() || !feature.isChangeable()) {
                result = Status.createFailingStatus(
                        MessageFormat.format("{0} can''t be modified on the selected element.", this.featureName));
            } else if (this.newValue != null && !feature.getEType().isInstance(this.newValue)) {
                return Status.createFailingStatus(
                        "Invalid value type for reference " + this.featureName + " object " + this.newValue);
            } else {
                result = this.genericSet(object, feature, this.newValue);
            }

            return result;
        }

        @SuppressWarnings({ "unchecked", "rawtypes" })
        private Status genericSet(EObject owner, EStructuralFeature feature, Object newValueToSet) {

            Status status = Status.DONE;
            switch (this.operationType) {
            case ADD:
                if (feature.isMany()) {
                    ((List) owner.eGet(feature)).add(newValueToSet);
                } else {
                    if (feature instanceof EReference ref && ref.isContainment() && owner.eGet(feature) != null) {
                        // Prevent replacing a element in a containment reference.
                        // It can cause many issue if the element is not properly destroy
                        // (DanglingHREFException n this element or one of its children)
                        status = Status.createFailingStatus("Unable to add the element in '" + featureName
                                + "' feature. It is already filled with an element and can only contain one.");
                    } else {
                        owner.eSet(feature, newValueToSet);
                    }
                }
                break;
            case REMOVE:
                if (feature.isMany()) {
                    ((List) owner.eGet(feature)).remove(newValueToSet);
                } else {
                    status = Status
                            .createFailingStatus("Unable to remove a value from an unary feature :" + this.featureName);
                }
                break;
            case SET:
                if (feature.isMany()) {
                    List currentValue = (List) owner.eGet(feature);
                    currentValue.clear();
                    currentValue.add(newValueToSet);
                } else {
                    owner.eSet(feature, newValueToSet);
                }
                break;

            default:
                break;
            }

            return status;
        }

    }

}
