/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.DeploymentExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for
 * {@link DeploymentExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class DeploymentExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private DeploymentExternalSourceToRepresentationDropBehaviorProvider deploymentExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.deploymentExternalSourceToRepresentationDropBehaviorProvider = new DeploymentExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping an element into a Package. Since NOTHING is expected, we do not
     * check other kind of container.
     *
     * @param clazz
     *                       the type of the element to check the drop state.
     * @param expectedResult
     *                       true if NOTHING is expected, false if FAILED is
     *                       expected.
     *
     */
    @ParameterizedTest
    @MethodSource("getDragAndDropParameters")
    public void testDragAndDrop(Class<? extends Element> clazz, boolean expectedResult) {
        Element element = this.create(clazz);
        Package pkg = this.create(Package.class);

        DnDStatus status = this.deploymentExternalSourceToRepresentationDropBehaviorProvider.drop(element, pkg,
                this.getCrossRef(), this.getEditableChecker());
        State state = State.FAILED;
        if (expectedResult) {
            assertTrue(List.of(element).containsAll(status.getElementsToDisplay()));
            state = State.NOTHING;
        }
        assertEquals(state, status.getState());
    }

    public static Stream<Arguments> getDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Artifact.class, true),
                Arguments.of(Comment.class, true),
                Arguments.of(CommunicationPath.class, true),
                Arguments.of(Constraint.class, true),
                Arguments.of(Dependency.class, true),
                Arguments.of(Deployment.class, true),
                Arguments.of(DeploymentSpecification.class, true),
                Arguments.of(Device.class, true),
                Arguments.of(ExecutionEnvironment.class, true),
                Arguments.of(Generalization.class, true),
                Arguments.of(Manifestation.class, true),
                Arguments.of(Model.class, true),
                Arguments.of(Node.class, true),
                Arguments.of(Package.class, true),
                Arguments.of(Activity.class, false)
                );
        // @formatter:on
    }

}
