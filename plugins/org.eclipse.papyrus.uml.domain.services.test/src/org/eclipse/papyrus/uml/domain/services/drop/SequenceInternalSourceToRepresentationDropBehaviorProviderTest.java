/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.SequenceInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link SequenceInternalSourceToRepresentationDropBehaviorProvider}.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class SequenceInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private SequenceInternalSourceToRepresentationDropBehaviorProvider sequenceInternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.sequenceInternalSourceToRepresentationDropBehaviorProvider = new SequenceInternalSourceToRepresentationDropBehaviorProvider();
    }

    @Test
    public void testCommentDropFromInteractionToInteractionOperand() {
        Interaction interaction = this.create(Interaction.class);
        Comment commentToDrop = this.create(Comment.class);
        interaction.getOwnedComments().add(commentToDrop);
        InteractionOperand interactionOperand = create(InteractionOperand.class);

        Status status = this.sequenceInternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop, interaction,
                interactionOperand, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(interactionOperand.getOwnedComments().contains(commentToDrop));
        assertFalse(interaction.getOwnedComments().contains(commentToDrop));
    }

    @Test
    public void testCommentDropFromInteractionOperandToInteraction() {
        InteractionOperand interactionOperand = create(InteractionOperand.class);
        Comment commentToDrop = this.create(Comment.class);
        interactionOperand.getOwnedComments().add(commentToDrop);
        Interaction interaction = this.create(Interaction.class);

        Status status = this.sequenceInternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop,
                interactionOperand, interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(interaction.getOwnedComments().contains(commentToDrop));
        assertFalse(interactionOperand.getOwnedComments().contains(commentToDrop));
    }

    @Test
    public void testConstraintDropFromInteractionToInteractionOperand() {
        Interaction interaction = this.create(Interaction.class);
        Constraint constraintToDrop = this.create(Constraint.class);
        interaction.getOwnedRules().add(constraintToDrop);
        InteractionOperand interactionOperand = create(InteractionOperand.class);

        Status status = this.sequenceInternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop,
                interaction, interactionOperand, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(interactionOperand.getOwnedRules().contains(constraintToDrop));
        assertFalse(interaction.getOwnedRules().contains(constraintToDrop));
    }

    @Test
    public void testConstraintDropFromInteractionOperandToInteraction() {
        InteractionOperand interactionOperand = create(InteractionOperand.class);
        Constraint constraintToDrop = this.create(Constraint.class);
        interactionOperand.getOwnedRules().add(constraintToDrop);
        Interaction interaction = this.create(Interaction.class);

        Status status = this.sequenceInternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop,
                interactionOperand, interaction, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(interaction.getOwnedRules().contains(constraintToDrop));
        assertFalse(interactionOperand.getOwnedRules().contains(constraintToDrop));
    }
}
