/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.UseCaseInternalSourceTorepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link UseCaseInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author Jessy Mallet
 *
 */
public class UseCaseInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping an {@link Actor} from a {@link Package} container to another
     * {@link Package}.
     */
    @Test
    public void testActorDropFromPackageToPackage() {
        Package packageOldContainer = this.create(Package.class);
        Actor actor = this.createIn(Actor.class, packageOldContainer);
        Package packageNewContainer = this.create(Package.class);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(actor, packageOldContainer,
                packageNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(packageOldContainer.getPackagedElements().isEmpty());
        assertEquals(List.of(actor), packageNewContainer.getPackagedElements());
    }

    /**
     * Test dropping an {@link Actor} from a {@link Package} container to another
     * {@link Package}.
     */
    @Test
    public void testActorDropFromSamePackage() {
        Package packageContainer = this.create(Package.class);
        Actor actor = this.createIn(Actor.class, packageContainer);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(actor, packageContainer,
                packageContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertEquals(List.of(actor), packageContainer.getPackagedElements());
    }

    /**
     * Test dropping a {@link UseCase} from a {@link Class} container to a
     * {@link Class}.
     */
    @Test
    public void testUseCaseDropFromClassToClass() {
        UseCase useCaseToDrop = this.create(UseCase.class);
        Class clazzOldContainer = this.create(Class.class);
        Class clazzNewContainer = this.create(Class.class);
        clazzOldContainer.getOwnedUseCases().add(useCaseToDrop);
        clazzOldContainer.getUseCases().add(useCaseToDrop);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(useCaseToDrop,
                clazzOldContainer, clazzNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());

        // check containers
        assertTrue(clazzOldContainer.getOwnedUseCases().isEmpty());
        assertFalse(clazzOldContainer.getUseCases().contains(useCaseToDrop));
        assertTrue(clazzNewContainer.getOwnedUseCases().contains(useCaseToDrop));
        assertTrue(clazzNewContainer.getUseCases().contains(useCaseToDrop));

        // check useCase
        assertEquals(clazzNewContainer, useCaseToDrop.eContainer());
        assertFalse(useCaseToDrop.getSubjects().contains(clazzOldContainer));
        assertTrue(useCaseToDrop.getSubjects().contains(clazzNewContainer));
    }

    /**
     * Test dropping a {@link UseCase} from a {@link Class} container to a
     * {@link Model}.
     */
    @Test
    public void testUseCaseDropFromClassToModel() {
        UseCase useCaseToDrop = this.create(UseCase.class);
        Class clazzOldContainer = this.create(Class.class);
        Model modelNewContainer = this.create(Model.class);
        clazzOldContainer.getUseCases().add(useCaseToDrop);
        clazzOldContainer.getOwnedUseCases().add(useCaseToDrop);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(useCaseToDrop,
                clazzOldContainer, modelNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());

        // check containers
        assertTrue(clazzOldContainer.getOwnedUseCases().isEmpty());
        assertFalse(clazzOldContainer.getUseCases().contains(useCaseToDrop));
        assertTrue(modelNewContainer.getPackagedElements().contains(useCaseToDrop));

        // check useCase
        assertEquals(modelNewContainer, useCaseToDrop.eContainer());
        assertTrue(useCaseToDrop.getSubjects().isEmpty());
    }

    /**
     * Test dropping a {@link UseCase} from a {@link Model} container to a
     * {@link Class}.
     */
    @Test
    public void testUseCaseDropFromModelToClass() {
        UseCase useCaseToDrop = this.create(UseCase.class);
        Model modelOldContainer = this.create(Model.class);
        Class clazzNewContainer = this.create(Class.class);
        modelOldContainer.getPackagedElements().add(useCaseToDrop);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(useCaseToDrop,
                modelOldContainer, clazzNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());

        // check containers
        assertTrue(modelOldContainer.getPackagedElements().isEmpty());
        assertTrue(clazzNewContainer.getOwnedUseCases().contains(useCaseToDrop));
        assertTrue(clazzNewContainer.getUseCases().contains(useCaseToDrop));

        // check useCase
        assertEquals(clazzNewContainer, useCaseToDrop.eContainer());
        assertTrue(useCaseToDrop.getSubjects().contains(clazzNewContainer));
    }

    /**
     * Test dropping a {@link UseCase} from a {@link Model} container to a
     * {@link Package}.
     */
    @Test
    public void testUseCaseDropFromModelToPackage() {
        UseCase useCaseToDrop = this.create(UseCase.class);
        Model modelOldContainer = this.create(Model.class);
        Package packageNewContainer = this.create(Package.class);
        modelOldContainer.getPackagedElements().add(useCaseToDrop);

        Status status = new UseCaseInternalSourceTorepresentationDropBehaviorProvider().drop(useCaseToDrop,
                modelOldContainer, packageNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());

        // check containers
        assertTrue(modelOldContainer.getPackagedElements().isEmpty());
        assertTrue(packageNewContainer.getPackagedElements().contains(useCaseToDrop));

        // check useCase
        assertEquals(packageNewContainer, useCaseToDrop.eContainer());
        assertTrue(useCaseToDrop.getSubjects().isEmpty());
    }

}
