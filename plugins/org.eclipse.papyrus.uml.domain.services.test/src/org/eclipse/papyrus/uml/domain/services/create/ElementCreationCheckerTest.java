/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.ValueSpecificationAction;
import org.junit.jupiter.api.Test;

/**
 * This class tests the checking of element creation.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 *
 */
public class ElementCreationCheckerTest extends AbstractUMLTest {

    private static final String NULL_PARENT_MSG = "A parent container should be specified.";
    private static final String NULL_TYPE_MSG = "A type should be specified.";
    private static final String NULL_REFERENCE_MSG = "A reference should be specified.";
    private static final String ACTIVITY_TYPE = "uml::Activity";
    private static final String OWNED_ELEMENT_NAME_REF = "ownedElement";
    private static final String SPACE = " ";

    @Test
    public void testNullInput() {
        Activity activityContainer = create(Activity.class);

        CheckStatus checkStatus = new ElementCreationChecker().canCreate(null, null, null);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_PARENT_MSG + SPACE + NULL_TYPE_MSG + SPACE + NULL_REFERENCE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(null, ACTIVITY_TYPE, OWNED_ELEMENT_NAME_REF);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_PARENT_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(activityContainer, null, OWNED_ELEMENT_NAME_REF);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_TYPE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(activityContainer, ACTIVITY_TYPE, null);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_REFERENCE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(activityContainer, null, null);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_TYPE_MSG + SPACE + NULL_REFERENCE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(null, null, OWNED_ELEMENT_NAME_REF);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_PARENT_MSG + SPACE + NULL_TYPE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(null, ACTIVITY_TYPE, null);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_PARENT_MSG + SPACE + NULL_REFERENCE_MSG, checkStatus.getMessage());
    }

    @Test
    public void testEmptyInput() {
        Activity activityContainer = create(Activity.class);

        CheckStatus checkStatus = new ElementCreationChecker().canCreate(activityContainer, "", "");
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_TYPE_MSG + SPACE + NULL_REFERENCE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(activityContainer, ACTIVITY_TYPE, "");
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_REFERENCE_MSG, checkStatus.getMessage());

        checkStatus = new ElementCreationChecker().canCreate(activityContainer, "", OWNED_ELEMENT_NAME_REF);
        assertFalse(checkStatus.isValid());
        assertEquals(NULL_TYPE_MSG, checkStatus.getMessage());
    }

    @Test
    public void testRegionCreationChecking() {
        State state = create(State.class);
        CheckStatus checkStatus = new ElementCreationChecker().canCreate(state, "uml::Region", "region");
        assertTrue(checkStatus.isValid());

        FinalState finalState = create(FinalState.class);
        checkStatus = new ElementCreationChecker().canCreate(finalState, "uml::Region", "region");
        assertFalse(checkStatus.isValid());
        assertEquals("A Final State can not have a region.", checkStatus.getMessage());
    }

    /**
     * The feature DurationConstraint#firstEvent has a bounded multiplicity [0..2].
     * We should not be able to add more than 2 elements.
     */
    @Test
    public void testAddElementsWithBoundedMultiplicityFeature() {
        DurationConstraint durationConstraint = create(DurationConstraint.class);
        CheckStatus checkStatus = new ElementCreationChecker().canCreate(durationConstraint, "Boolean", "firstEvent");
        assertTrue(checkStatus.isValid());

        durationConstraint.getFirstEvents().add(Boolean.TRUE);
        durationConstraint.getFirstEvents().add(Boolean.FALSE);

        checkStatus = new ElementCreationChecker().canCreate(durationConstraint, "Boolean", "firstEvent");
        assertFalse(checkStatus.isValid());
        assertEquals("firstEvent feature has already 2 elements.", checkStatus.getMessage());
    }

    /**
     * The feature Activity#structuredNode has an unbounded multiplicity [0..*]. We
     * should be able to add as many elements we want.
     */
    @Test
    public void testAddElementsWithUnboundedMultiplicityFeature() {
        Activity activityContainer = create(Activity.class);
        CheckStatus checkStatus = new ElementCreationChecker().canCreate(activityContainer,
                "uml::StructuredActivityNode", "structuredNode");
        assertTrue(checkStatus.isValid());

        activityContainer.getStructuredNodes()
                .addAll(Arrays.asList(create(StructuredActivityNode.class), create(StructuredActivityNode.class),
                        create(StructuredActivityNode.class), create(StructuredActivityNode.class)));
        checkStatus = new ElementCreationChecker().canCreate(activityContainer, "uml::StructuredActivityNode",
                "structuredNode");
        assertTrue(checkStatus.isValid());
    }

    /**
     * The feature ValueSpecificationAction#result has a mandatory multiplicity
     * [1..1]. We should not be able to add element if the feature is already set.
     */
    @Test
    public void testAddElementWithMandatoryMultiplicityFeature() {
        ValueSpecificationAction vsa = create(ValueSpecificationAction.class);
        CheckStatus checkStatus = new ElementCreationChecker().canCreate(vsa, "uml::OutputPin", "result");
        assertTrue(checkStatus.isValid());

        vsa.setResult(create(OutputPin.class));
        checkStatus = new ElementCreationChecker().canCreate(vsa, "uml::OutputPin", "result");
        assertFalse(checkStatus.isValid());
        assertEquals("result feature is already set.", checkStatus.getMessage());
    }

    @Test
    public void testAddElementInNonExistingFeature() {
        Activity activityContainer = create(Activity.class);
        CheckStatus checkStatus = new ElementCreationChecker().canCreate(activityContainer, ACTIVITY_TYPE, "test");
        assertFalse(checkStatus.isValid());
        assertEquals("test feature has not been found for the EClass Activity.", checkStatus.getMessage());
    }
}
