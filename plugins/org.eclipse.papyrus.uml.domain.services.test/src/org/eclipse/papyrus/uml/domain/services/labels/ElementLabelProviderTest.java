/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.EMFUtils.allContainedObjectOfType;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_ANGLE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EMPTY;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EQL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_ANGLE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.TILDE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.ChangeEvent;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ConditionalNode;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Duration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ExpansionKind;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.Expression;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.JoinNode;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LoopNode;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.SequenceNode;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.TimeExpression;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.VisibilityKind;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementLabelProvider}.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementLabelProviderTest extends AbstractUMLTest {

    /**
     * Model containing a UML Model with stereotype content.
     */
    public static final URI UML_MODEL_WITH_PROFILE = URI.createPlatformPluginURI(
            "/org.eclipse.papyrus.uml.domain.services.test/profile/UMLModelWithStandardProfile.uml", false);

    private static final String FLOW = "flow";
    private static final String C1_NAME = "c1";
    private static final String IF1 = "if1";
    private static final String SPACE = " ";
    private static final String CONST = "const";
    private static final String CLASS2 = "Class2";
    private static final String CLASS1 = "Class1";
    private static final String LIFELINE = "MyLifeline";
    private static final String PROPERTY = "MyProperty";
    private static final String EXPRESSION = "MyExpression";

    private static final String LITERAL_STRING = "MyLiteralString";
    private static final String LITERAL_STRING_VALUE = "MyLiteralStringValue";
    private static final String MY_JOIN_NODE = "MyJoinNode";
    private static final String LITERAL = "li1";
    private static final String JOIN_SPEC = "joinSpec";
    private static final String SEQUENCE = "sequence";
    private static final String LOOP_NODE = "loop node";
    private static final String STRUCTURED = "structured";
    private static final String CONDITIONAL = "conditional";
    private static final String EXPECTED_WEIGHT_VALUE = "1";
    private static final String OBJECT_FLOW_NAME = "objectFlowName";
    private static final String THE_GUARD_VALUE = "theGuardValue";
    private static final String WEIGHT = "weight";
    private static final String E_CLASS = "EClass"; //$NON-NLS-1$
    private static final String ACTIVITY = "activity";
    private static final String ACTIVITY1 = "activity1";
    private static final String SINGLE_EXECUTION = "singleExecution";
    private static final String A_PREFIX = "aPrefix";

    private static String guillemets(String value) {
        return ST_LEFT + value + ST_RIGHT;
    }

    /**
     * Basic test case for {@link Usage} label. Prefix \u00ABuse\u00BB should
     * appear.
     */
    @Test
    public void testUsageLabels() {
        Usage usage = this.create(Usage.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("use"), elementLabelProvider.getLabel(usage));
        usage.setName("u1");
        assertEquals(guillemets("use") + EOL + "u1", elementLabelProvider.getLabel(usage));
    }

    private ElementLabelProvider buildLabelProviderNoPrefix() {
        return ElementLabelProvider.builder()//
                .withKeywordLabelProvider(new KeywordLabelProvider())//
                .withNameProvider(new DefaultNamedElementNameProvider())//
                .build();
    }

    private ElementLabelProvider buildLabelProviderWithStereotypePrefix() {
        return ElementLabelProvider.builder()//
                .withKeywordLabelProvider(new KeywordLabelProvider())//
                .withPrefixLabelProvider(new StereotypeLabelPrefixProvider())//
                .withNameProvider(new DefaultNamedElementNameProvider()).build();
    }

    private ElementLabelProvider buildLabelProviderWithGivenPrefix(Function<EObject, String> prefix) {
        return ElementLabelProvider.builder()//
                .withKeywordLabelProvider(new KeywordLabelProvider())//
                .withPrefixLabelProvider(prefix)//
                .withNameProvider(new DefaultNamedElementNameProvider())//
                .build();
    }

    private Lifeline createTypedLifeline() {
        Lifeline result = this.create(Lifeline.class);
        result.setName(LIFELINE);
        Class clazz = this.create(Class.class);
        clazz.setName(CLASS1);
        Property property = this.create(Property.class);
        property.setName(PROPERTY);
        property.setType(clazz);
        result.setRepresents(property);
        return result;
    }

    private Expression createExpression() {
        Expression expression = this.create(Expression.class);
        expression.setName(EXPRESSION);
        LiteralString literalString = this.create(LiteralString.class);
        literalString.setName(LITERAL_STRING);
        literalString.setValue(LITERAL_STRING_VALUE);
        expression.getOperands().add(literalString);
        return expression;
    }

    private void assertLifeline(String name, String selection, String type, Element lifeline) {
        assertLabel(MessageFormat.format("{0}[{1}] : {2}", name, selection, type), lifeline);
    }

    private void assertLifeline(String name, String type, Element lifeline) {
        assertLabel(MessageFormat.format("{0} : {1}", name, type), lifeline);
    }

    private void assertLabel(String label, Element element) {
        ElementLabelProvider elementLabelProvider = buildLabelProviderNoPrefix();
        assertEquals(label, elementLabelProvider.getLabel(element));
    }

    /**
     * Basic test case for {@link Abstraction} label. Prefix \u00ABabstraction\u00BB
     * should appear.
     */
    @Test
    public void testAbstractionLabels() {
        Abstraction abstraction = this.create(Abstraction.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("abstraction"), elementLabelProvider.getLabel(abstraction));
        abstraction.setName("ab1");
        assertEquals(guillemets("abstraction") + EOL + "ab1", elementLabelProvider.getLabel(abstraction));
    }

    @Test
    public void testOnNull() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals("", elementLabelProvider.getLabel(null)); //$NON-NLS-1$
    }

    @Test
    public void testOnClass() {
        var clazz = this.create(Class.class);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals("", elementLabelProvider.getLabel(clazz)); //$NON-NLS-1$

        String className = "Class éé"; //$NON-NLS-1$
        clazz.setName(className);
        assertEquals(className, elementLabelProvider.getLabel(clazz));
    }

    @Test
    public void testOnProperty() {

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Property property = this.create(Property.class);
        property.setName("prop1"); //$NON-NLS-1$

        assertEquals("+ prop1: <Undefined> [1]", elementLabelProvider.getLabel(property)); //$NON-NLS-1$

        var type = this.create(Class.class);
        type.setName(C1_NAME);
        property.setType(type);

        assertEquals("+ prop1: c1 [1]", elementLabelProvider.getLabel(property)); //$NON-NLS-1$

        // More use case tested in the labels packages
    }

    @Test
    public void testOnPort() {

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Port port = this.create(Port.class);
        port.setName("port1"); //$NON-NLS-1$
        port.setIsConjugated(true);

        assertEquals("+ port1: " + TILDE + "<Undefined> [1]", elementLabelProvider.getLabel(port)); //$NON-NLS-1$

        // More use case tested in testOnProperty
    }

    @Test
    public void testComment() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Comment comment = this.create(Comment.class);
        comment.setBody("some comment"); //$NON-NLS-1$

        assertEquals(elementLabelProvider.getLabel(comment), "some comment");
    }

    /**
     * Basic test case for {@link CommunicationPath} label.
     */
    @Test
    public void testCommunicationPathLabel() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        assertEquals("", elementLabelProvider.getLabel(communicationPath));
        communicationPath.setName("CommunicationPathName");
        assertEquals("CommunicationPathName", elementLabelProvider.getLabel(communicationPath));
    }

    /**
     * Test for derived {@link CommunicationPath} label. The label should be
     * prefixed with "/".
     */
    @Test
    public void testCommunicationPathIsDerivedLabel() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        communicationPath.setIsDerived(true);
        assertEquals(UMLCharacters.SLASH, elementLabelProvider.getLabel(communicationPath));
        communicationPath.setName("CommunicationPath");
        assertEquals(UMLCharacters.SLASH + "CommunicationPath", elementLabelProvider.getLabel(communicationPath));
    }

    /**
     * Basic test case for a basic {@link Dependency} label such as
     * {@link Realization} label. None Prefix should appear.
     */
    @Test
    public void testRealizationLabels() {
        Realization realization = this.create(Realization.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals("", elementLabelProvider.getLabel(realization));
        realization.setName("r1");
        assertEquals("r1", elementLabelProvider.getLabel(realization));
    }

    /**
     * Basic test case for {@link Substitution} label. Prefix \u00ABsubstitute\u00BB
     * should appear.
     */
    @Test
    public void testSubstitutionLabels() {
        Substitution substitution = this.create(Substitution.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("substitute"), elementLabelProvider.getLabel(substitution));
        substitution.setName("s1");
        assertEquals(guillemets("substitute") + EOL + "s1", elementLabelProvider.getLabel(substitution));
    }

    /**
     * Basic test case for {@link Manifestation} label. Prefix \u00ABmanifest\u00BB
     * should appear.
     */
    @Test
    public void testManifestationLabels() {
        Manifestation manifestation = this.create(Manifestation.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("manifest"), elementLabelProvider.getLabel(manifestation));
        manifestation.setName("m1");
        assertEquals(guillemets("manifest") + EOL + "m1", elementLabelProvider.getLabel(manifestation));
    }

    /**
     * Basic test case for {@link InformationItem} label. Prefix
     * \u00ABInformation\u00BB should appear.
     */
    @Test
    public void testInformationItemLabels() {
        InformationItem informationItem = this.create(InformationItem.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(ST_LEFT + "information" + UMLCharacters.ST_RIGHT, elementLabelProvider.getLabel(informationItem));
        informationItem.setName("ii1");
        assertEquals(ST_LEFT + "information" + UMLCharacters.ST_RIGHT + EOL + "ii1",
                elementLabelProvider.getLabel(informationItem));
    }

    @Test
    public void testStereotypeElementLabel() {
        ResourceSet rs = new ResourceSetImpl();
        Resource umlResource = rs.getResource(UML_MODEL_WITH_PROFILE, true);

        String classOneStereotypeName = "ClassOneStereotype";
        Class classOneStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classOneStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        ElementLabelProvider labelProvider = ElementLabelProvider.buildDefault();
        String label0 = labelProvider.getLabel(classOneStereotype);
        assertEquals(guillemets("Utility") + EOL + classOneStereotypeName, label0);

        String classTwoStereotypeName = "ClassTwoStereotypes";
        Class classTwoStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classTwoStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        String label2 = labelProvider.getLabel(classTwoStereotype);
        assertEquals(guillemets("Auxiliary, Focus") + EOL + classTwoStereotypeName, label2);
    }

    /**
     * Basic test case for {@link PackageImport} label. Prefix import should appear.
     */
    @Test
    public void testPackageImport() {
        PackageImport element = this.create(PackageImport.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("import"), elementLabelProvider.getLabel(element));

        // Label is not related to source or target.
        element.setImportedPackage(this.createNamedElement(Package.class, "Any"));
        assertEquals(guillemets("import"), elementLabelProvider.getLabel(element));

        element.setVisibility(VisibilityKind.PRIVATE_LITERAL);
        String accessKeyword = "access";
        assertEquals(guillemets(accessKeyword), elementLabelProvider.getLabel(element));
        element.setVisibility(VisibilityKind.PACKAGE_LITERAL);
        assertEquals(guillemets(accessKeyword), elementLabelProvider.getLabel(element));
        element.setVisibility(VisibilityKind.PROTECTED_LITERAL);
        assertEquals(guillemets(accessKeyword), elementLabelProvider.getLabel(element));
    }

    /**
     * Basic test case for {@link PackageMerge} label. Prefix merge should appear.
     */
    @Test
    public void testPackageMerge() {
        PackageMerge element = this.create(PackageMerge.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("merge"), elementLabelProvider.getLabel(element));

        // Label is not related to source or target.
        element.setMergedPackage(this.createNamedElement(org.eclipse.uml2.uml.Package.class, "M1"));
        element.setReceivingPackage(this.createNamedElement(org.eclipse.uml2.uml.Package.class, "M2"));
        assertEquals(guillemets("merge"), elementLabelProvider.getLabel(element));
    }

    @Test
    public void testConstraintLabelsWithNoSpecification() {
        Constraint constraint = this.create(Constraint.class);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + "<NULL Constraint>" + UMLCharacters.CLOSE_BRACKET);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    @Test
    public void testConstraintLabelsWithEmptyOpaqueExpressionSpec() {
        Constraint constraint = this.create(Constraint.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        OpaqueExpression opaqueExp = this.create(OpaqueExpression.class);
        constraint.setSpecification(opaqueExp);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + UMLCharacters.OPEN_BRACKET + "NATURAL"
                + UMLCharacters.CLOSE_BRACKET + SPACE + UMLCharacters.CLOSE_BRACKET);

        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    @Test
    public void testConstraintLabelsWithOpaqueExpressionSpec() {
        Constraint constraint = this.create(Constraint.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        OpaqueExpression opaqueExp = this.create(OpaqueExpression.class);
        opaqueExp.getLanguages().add("OCL");
        opaqueExp.getBodies().add("true");
        constraint.setSpecification(opaqueExp);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + UMLCharacters.OPEN_BRACKET + opaqueExp.getLanguages().get(0)
                + UMLCharacters.CLOSE_BRACKET + SPACE + opaqueExp.getBodies().get(0) + UMLCharacters.CLOSE_BRACKET);

        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    /**
     * Basic test case for {@link Collaboration} label. Prefix
     * \u00ABCollaboration\u00BB should appear.
     */
    @Test
    public void testCollaborationLabels() {
        Collaboration collaboration = this.create(Collaboration.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("collaboration"), elementLabelProvider.getLabel(collaboration));
        collaboration.setName(C1_NAME);
        assertEquals(guillemets("collaboration") + EOL + C1_NAME,
                elementLabelProvider.getLabel(collaboration));
    }

    /**
     * Basic test case for {@link Activity} label. Prefix \u00ABActivity\u00BB
     * should appear.
     */
    @Test
    public void testActivityLabels() {
        Activity activity = this.create(Activity.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(ACTIVITY), elementLabelProvider.getLabel(activity));
        activity.setName("a1");
        assertEquals(guillemets(ACTIVITY) + EOL + "a1", elementLabelProvider.getLabel(activity));
    }

    /**
     * Check that the {@link Activity} label with a stereotype and two keyword
     * (activity and singleExecution) is properly displayed.
     */
    @Test
    public void testActivityLabelsWithStereotypeAndSingleExecution() {
        Profile eCoreProfile = this.getECoreProfile();
        Model model = this.create(Model.class);
        Activity activity = this.createIn(Activity.class, model);
        activity.setName(ACTIVITY1);
        model.applyProfile(eCoreProfile);
        Stereotype stereotype = eCoreProfile.getOwnedStereotype(E_CLASS);
        activity.applyStereotype(stereotype);
        activity.setIsSingleExecution(true);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderWithStereotypePrefix();
        String expected = "\u00AB%s, %s\u00BB" + EOL //
                + "\u00AB%s\u00BB" + EOL //
                + "%s";
        expected = expected.formatted(ACTIVITY, SINGLE_EXECUTION, E_CLASS, ACTIVITY1);
        assertEquals(expected, elementLabelProvider.getLabel(activity));
    }

    @Test
    public void testFunctionBehaviorLabels() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("functionBehavior"), elementLabelProvider.getLabel(functionBehavior));
        functionBehavior.setName("fb1");
        assertEquals(guillemets("functionBehavior") + EOL + "fb1",
                elementLabelProvider.getLabel(functionBehavior));
    }

    @Test
    public void testOpaqueBehaviorLabels() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("opaqueBehavior"), elementLabelProvider.getLabel(opaqueBehavior));
        opaqueBehavior.setName("ob1");
        assertEquals(guillemets("opaqueBehavior") + EOL + "ob1",
                elementLabelProvider.getLabel(opaqueBehavior));
    }

    @Test
    public void testInteractionLabels() {
        Interaction interaction = this.create(Interaction.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("interaction"), elementLabelProvider.getLabel(interaction));
        interaction.setName("i1");
        assertEquals(guillemets("interaction") + EOL + "i1", elementLabelProvider.getLabel(interaction));
    }

    @Test
    public void testStateMachineLabels() {
        StateMachine stateMachine = this.create(StateMachine.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("stateMachine"), elementLabelProvider.getLabel(stateMachine));
        stateMachine.setName("sm1");
        assertEquals(guillemets("stateMachine") + EOL + "sm1", elementLabelProvider.getLabel(stateMachine));
    }

    @Test
    public void testProtocolStateMachineLabels() {
        ProtocolStateMachine protocolStateMachine = this.create(ProtocolStateMachine.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("protocol"), elementLabelProvider.getLabel(protocolStateMachine));
        protocolStateMachine.setName("psm1");
        assertEquals(guillemets("protocol") + EOL + "psm1",
                elementLabelProvider.getLabel(protocolStateMachine));
    }

    @Test
    public void testInformationFlowLabels() {
        InformationFlow informationFlow = this.create(InformationFlow.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(FLOW), elementLabelProvider.getLabel(informationFlow));
        informationFlow.setName(IF1);
        assertEquals(guillemets(FLOW) + EOL + IF1, elementLabelProvider.getLabel(informationFlow));

        // Test that conveyeds Elements name are displayed
        Class class1 = this.create(Class.class);
        class1.setName(CLASS1);
        Class class2 = this.create(Class.class);
        class2.setName(CLASS2);
        informationFlow.getConveyeds().addAll(List.of(class1, class2));
        assertEquals(guillemets(FLOW) + EOL + CLASS1 + ", " + CLASS2 + EOL + IF1,
                elementLabelProvider.getLabel(informationFlow));

        // Test that conveyeds Elements name are displayed, even if the InformationFlow
        // has no name.

        InformationFlow informationFlow2 = this.create(InformationFlow.class);
        informationFlow2.getConveyeds().addAll(List.of(class1, class2));
        assertEquals(guillemets(FLOW) + UMLCharacters.EOL + "Class1, Class2",
                elementLabelProvider.getLabel(informationFlow2));
    }

    @Test
    public void testComponentLabels() {
        Component element = this.create(Component.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("component"), elementLabelProvider.getLabel(element));
        element.setName("comp");
        assertEquals(guillemets("component") + EOL + "comp", elementLabelProvider.getLabel(element));
    }

    /**
     * Basic test case for {@link Region} label that should be empty.
     */
    @Test
    public void testRegionLabels() {
        Region region = this.create(Region.class);
        region.setName("Dummy");

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals("", elementLabelProvider.getLabel(region));
    }

    @Test
    public void testIncludeLabels() {
        Include element = this.create(Include.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("include"), elementLabelProvider.getLabel(element));
        element.setName("incl");
        assertEquals(guillemets("include") + EOL + "incl", elementLabelProvider.getLabel(element));
    }

    @Test
    public void testInterfaceLabels() {
        Interface element = this.create(Interface.class);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets("interface"), elementLabelProvider.getLabel(element));
        element.setName("inter");
        assertEquals(guillemets("interface") + EOL + "inter", elementLabelProvider.getLabel(element));
    }

    @Test
    public void testOperationLabels() {
        Operation operation = this.create(Operation.class);
        Parameter p1 = this.create(Parameter.class);
        Parameter p2 = this.create(Parameter.class);
        Parameter p3 = this.create(Parameter.class);
        Parameter p4 = this.create(Parameter.class);
        Class class1 = this.create(Class.class);
        class1.setName(CLASS1);

        operation.setName("op");
        p1.setName("param1");
        p2.setName("param2");
        p2.setType(class1);
        p2.setDirection(ParameterDirectionKind.OUT_LITERAL);
        p3.setName("returnParam");
        p3.setDirection(ParameterDirectionKind.RETURN_LITERAL);
        p4.setName("returnParam2");
        p4.setDirection(ParameterDirectionKind.RETURN_LITERAL);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals("+ op()", elementLabelProvider.getLabel(operation));
        operation.getOwnedParameters().add(p1);
        operation.getOwnedParameters().add(p2);
        assertEquals("+ op(in param1: <Undefined>, out param2: Class1)", elementLabelProvider.getLabel(operation));
        operation.setName("Custom Operation");
        p2.setName("p2");
        assertEquals("+ Custom Operation(in param1: <Undefined>, out p2: Class1)",
                elementLabelProvider.getLabel(operation));
        operation.getOwnedParameters().add(p3);
        operation.getOwnedParameters().add(p4);
        assertEquals("+ Custom Operation(in param1: <Undefined>, out p2: Class1): <Undefined>",
                elementLabelProvider.getLabel(operation));
        p3.setType(class1);
        assertEquals("+ Custom Operation(in param1: <Undefined>, out p2: Class1): Class1",
                elementLabelProvider.getLabel(operation));
    }

    @Test
    public void testOnParameter() {

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Parameter parameter = this.create(Parameter.class);
        parameter.setName("param1"); //$NON-NLS-1$

        assertEquals("in param1: <Undefined>", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$

        var type = this.create(Class.class);
        type.setName(CLASS1); // $NON-NLS-1$
        parameter.setType(type);

        assertEquals("in param1: Class1", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$

        LiteralInteger literalInteger = this.create(LiteralInteger.class);
        literalInteger.setName(LITERAL);
        literalInteger.setValue(2);
        parameter.setDefaultValue(literalInteger);

        assertEquals("in param1: Class1 = 2", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$
    }

    @Test
    public void testOnCollaborationUse() {

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        collaborationUse.setName("cu1"); //$NON-NLS-1$

        assertEquals(" + cu1: <Undefined>", elementLabelProvider.getLabel(collaborationUse)); //$NON-NLS-1$

        Collaboration collaboration = this.create(Collaboration.class);
        collaboration.setName(C1_NAME); // $NON-NLS-1$
        collaborationUse.setType(collaboration);

        assertEquals(" + cu1: c1", elementLabelProvider.getLabel(collaborationUse)); //$NON-NLS-1$
    }

    /**
     * Test the customization of the separator between prefix and keywords.
     */
    @Test
    public void testCustomSeparator() {
        Class clazz = this.create(Class.class);

        ElementLabelProvider labelProvider = ElementLabelProvider.builder()//
                .withKeywordLabelProvider(e -> "Keyword")//
                .withNameProvider(e -> "Name")//
                .withPrefixLabelProvider(e -> "Stereotype")//
                .withKeywordSeparator("--")//
                .withPrefixSeparator("||")//
                .build();

        String label = labelProvider.getLabel(clazz);
        assertEquals("Keyword--Stereotype||Name", label);
    }

    @Test
    public void testTimeExpressionLabels() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        TimeExpression timeExpression = this.create(TimeExpression.class);
        timeExpression.setName("timeExpr");

        assertEquals("timeExpr", elementLabelProvider.getLabel(timeExpression));

        LiteralString literalString = this.create(LiteralString.class);
        literalString.setValue("two");
        timeExpression.setExpr(literalString);
        assertEquals("timeExpr=two", elementLabelProvider.getLabel(timeExpression));
    }

    @Test
    public void testDurationLabels() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Duration duration = this.create(Duration.class);
        duration.setName("durationExpr");

        assertEquals("durationExpr", elementLabelProvider.getLabel(duration));

        LiteralString literalString = this.create(LiteralString.class);
        literalString.setValue("two");
        duration.setExpr(literalString);
        assertEquals("durationExpr=two", elementLabelProvider.getLabel(duration));
    }

    @Test
    public void testActivityPartitionLabels() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        ActivityPartition activityPartition = this.create(ActivityPartition.class);
        activityPartition.setName("partition");

        assertEquals("partition", elementLabelProvider.getLabel(activityPartition));

        Class clazz = this.create(Class.class);
        clazz.setName(C1_NAME);
        activityPartition.setRepresents(clazz);

        assertEquals(C1_NAME, elementLabelProvider.getLabel(activityPartition));
    }

    @Test
    public void testTransitionLabels() {
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        Transition transition = this.create(Transition.class);
        transition.setName("transition1");

        // check with a Guard
        Constraint constraint = this.create(Constraint.class);
        constraint.setName("constraint1");
        transition.getOwnedRules().add(constraint);
        transition.setGuard(constraint);

        OpaqueExpression opaqueExpression = this.create(OpaqueExpression.class);
        opaqueExpression.getBodies().add("body1");
        constraint.setSpecification(opaqueExpression);
        assertEquals("[body1]", elementLabelProvider.getLabel(transition));

        // check with an Effect
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        opaqueBehavior.setName("opaqueBehavior1");
        transition.setEffect(opaqueBehavior);
        assertEquals("[body1]/\nOpaqueBehavior: opaqueBehavior1", elementLabelProvider.getLabel(transition));

        // check with a Trigger
        Trigger trigger = transition.createTrigger("trigger1");
        Model model = this.create(Model.class);
        ChangeEvent changeEvent = this.create(ChangeEvent.class);
        Duration duration = this.create(Duration.class);
        LiteralReal literalReal = this.create(LiteralReal.class);
        duration.setExpr(literalReal);
        changeEvent.setChangeExpression(duration);
        model.getPackagedElements().add(changeEvent);
        trigger.setEvent(changeEvent);
        assertEquals("0.0[body1]/\nOpaqueBehavior: opaqueBehavior1", elementLabelProvider.getLabel(transition));
    }

    @Test
    public void testExpansionRegionLabel() {
        ExpansionRegion expansionRegion = this.create(ExpansionRegion.class);
        ExpansionKind mode = expansionRegion.getMode();
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(mode.getName()), elementLabelProvider.getLabel(expansionRegion));
        expansionRegion.setMode(ExpansionKind.PARALLEL_LITERAL);
        assertEquals(guillemets(ExpansionKind.PARALLEL_LITERAL.getName()),
                elementLabelProvider.getLabel(expansionRegion));
    }

    @Test
    public void testJoinNodeLabel() {
        JoinNode joinNode = this.create(JoinNode.class);
        joinNode.setName(MY_JOIN_NODE);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(MY_JOIN_NODE, elementLabelProvider.getLabel(joinNode));
        LiteralInteger literalInteger = this.create(LiteralInteger.class);
        literalInteger.setName(LITERAL);
        literalInteger.setValue(2);
        joinNode.setJoinSpec(literalInteger);
        StringBuilder expectedLabel = new StringBuilder();
        expectedLabel.append(UMLCharacters.OPEN_BRACKET);
        expectedLabel.append(JOIN_SPEC);
        expectedLabel.append(SPACE + EQL + SPACE);
        expectedLabel.append(2);
        expectedLabel.append(UMLCharacters.CLOSE_BRACKET);
        expectedLabel.append(UMLCharacters.EOL);
        expectedLabel.append(MY_JOIN_NODE);
        assertEquals(expectedLabel.toString(), elementLabelProvider.getLabel(joinNode));

    }

    @Test
    public void testStructuredActivityNodeLabel() {
        StructuredActivityNode structuredActivityNode = this.create(StructuredActivityNode.class);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(STRUCTURED), elementLabelProvider.getLabel(structuredActivityNode));
    }

    @Test
    public void testLoopNodeLabel() {
        LoopNode loopNode = this.create(LoopNode.class);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(LOOP_NODE), elementLabelProvider.getLabel(loopNode));
    }

    @Test
    public void testSequenceNodeLabel() {
        SequenceNode sequenceNode = this.create(SequenceNode.class);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(SEQUENCE), elementLabelProvider.getLabel(sequenceNode));
    }

    @Test
    public void testConditionalNodeLabel() {
        ConditionalNode conditionalNode = this.create(ConditionalNode.class);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(guillemets(CONDITIONAL), elementLabelProvider.getLabel(conditionalNode));
    }

    @Test
    public void testLifelineWithNameNoRepresents() {
        Lifeline lifeline = this.create(Lifeline.class);
        lifeline.setName(LIFELINE);
        assertLabel(LIFELINE, lifeline);
    }

    @Test
    public void testLifelineWithNoNameNoRepresents() {
        Lifeline lifeline = this.create(Lifeline.class);
        assertLabel(EMPTY, lifeline);
    }

    @Test
    public void testLifelineWithNameAndRepresentsNoSelector() {
        assertLifeline(PROPERTY, CLASS1, createTypedLifeline());
    }

    @Test
    public void testLifelineWithNameAndRepresentsAndLiteralStringSelector() {
        Lifeline lifeline = createTypedLifeline();
        LiteralString literalString = this.create(LiteralString.class);
        literalString.setName(LITERAL_STRING);
        literalString.setValue(LITERAL_STRING_VALUE);
        lifeline.setSelector(literalString);

        assertLifeline(PROPERTY, LITERAL_STRING_VALUE, CLASS1, lifeline);
    }


    @Test
    public void testLifelineWithNameAndRepresentsAndExpressionSelector() {
        Lifeline lifeline = createTypedLifeline();
        lifeline.setSelector(createExpression());

        assertLifeline(PROPERTY, "(" + LITERAL_STRING_VALUE + ")", CLASS1, lifeline);
    }

    @Test
    public void testLifelineWithNameNoRepresentsAndExpressionSelector() {
        Lifeline lifeline = this.create(Lifeline.class);
        lifeline.setName(LIFELINE);
        lifeline.setSelector(createExpression());

        assertLabel(LIFELINE, lifeline);
    }

    @Test
    public void testLifelineWithNameAndRepresentsAndOpaqueExpressionSelector() {
        Lifeline lifeline = createTypedLifeline();

        OpaqueExpression expression = this.create(OpaqueExpression.class);
        expression.setName(EXPRESSION);
        expression.getLanguages().add("JAVA");
        expression.getBodies().add("1 + 1");
        lifeline.setSelector(expression);

        assertLifeline(PROPERTY, "1 + 1", CLASS1, lifeline);
    }

    @Test
    public void testLifelineWithNameAndRepresentsAndTimingExpression() {
        Lifeline lifeline = createTypedLifeline();

        TimeExpression expression = this.create(TimeExpression.class);
        expression.setName(EXPRESSION);
        TimeObservation timeObservation = this.create(TimeObservation.class);
        timeObservation.setName("MyTimeObservation");
        expression.getObservations().add(timeObservation);
        lifeline.setSelector(expression);

        assertLifeline(PROPERTY, "MyTimeObservation", CLASS1, lifeline);
    }

    private Interval createInterval() {
        Interval interval = this.create(Interval.class);
        interval.setName(EXPRESSION);
        LiteralInteger intervalMin = this.create(LiteralInteger.class);
        intervalMin.setValue(0);
        interval.setMin(intervalMin);
        LiteralInteger intervalMax = this.create(LiteralInteger.class);
        intervalMax.setValue(10);
        interval.setMax(intervalMax);
        return interval;
    }

    @Test
    public void testLifelineWithNameAndRepresentsAndInterval() {
        Lifeline lifeline = createTypedLifeline();
        lifeline.setSelector(createInterval());

        assertLifeline(PROPERTY, "0..10", CLASS1, lifeline);
    }

    @Test
    public void testLifelineWithNameNoRepresentsAndInterval() {
        Lifeline lifeline = this.create(Lifeline.class);
        lifeline.setName(LIFELINE);
        lifeline.setSelector(createInterval());

        // No property to select from
        assertLabel(LIFELINE, lifeline);
    }

    @Test
    public void testLifelineWithNameAndRepresentsNoType() {
        Lifeline lifeline = this.create(Lifeline.class);
        lifeline.setName(LIFELINE);
        Property property = this.create(Property.class);
        property.setName(PROPERTY);
        lifeline.setRepresents(property);

        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(PROPERTY, elementLabelProvider.getLabel(lifeline));
    }

    @Test
    public void testObjectFlowLabel() {
        this.checkActivityEdgeLabel(this.create(ObjectFlow.class));
    }

    @Test
    public void testControlFlowLabel() {
        this.checkActivityEdgeLabel(this.create(ControlFlow.class));
    }

    private void checkActivityEdgeLabel(ActivityEdge activityEdge) {
        String expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, null, null, null);
        ElementLabelProvider elementLabelProvider = this.buildLabelProviderNoPrefix();
        assertEquals(expectedEmptyActivityEdgeLabel, elementLabelProvider.getLabel(activityEdge));
        LiteralString literalString = UMLFactory.eINSTANCE.createLiteralString();
        literalString.setValue(THE_GUARD_VALUE);
        activityEdge.setGuard(literalString);
        expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, null, THE_GUARD_VALUE,
                null);
        assertEquals(expectedEmptyActivityEdgeLabel, elementLabelProvider.getLabel(activityEdge));
        LiteralInteger literalInteger = UMLFactory.eINSTANCE.createLiteralInteger();
        literalInteger.setValue(1);
        activityEdge.setWeight(literalInteger);
        activityEdge.setGuard(null);
        expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, EXPECTED_WEIGHT_VALUE, null,
                null);
        assertEquals(expectedEmptyActivityEdgeLabel, elementLabelProvider.getLabel(activityEdge));
        activityEdge.setGuard(literalString);
        expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, EXPECTED_WEIGHT_VALUE,
                THE_GUARD_VALUE, null);
        assertEquals(expectedEmptyActivityEdgeLabel, elementLabelProvider.getLabel(activityEdge));
        activityEdge.setName(OBJECT_FLOW_NAME);
        expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, EXPECTED_WEIGHT_VALUE,
                THE_GUARD_VALUE, null);
        assertEquals(expectedEmptyActivityEdgeLabel, elementLabelProvider.getLabel(activityEdge));

        // We now check with a prefix
        activityEdge.unsetName();
        ElementLabelProvider labelProviderWithGivenPrefix = this.buildLabelProviderWithGivenPrefix(element -> A_PREFIX);
        expectedEmptyActivityEdgeLabel = this.createExpectedActivityEdgeLabel(activityEdge, EXPECTED_WEIGHT_VALUE,
                THE_GUARD_VALUE, A_PREFIX);
        assertEquals(expectedEmptyActivityEdgeLabel, labelProviderWithGivenPrefix.getLabel(activityEdge));
    }

    private String createExpectedActivityEdgeLabel(ActivityEdge activityEdge, String expectedWeightValue,
            String expectedGuardValue, String expectedPrefixValue) {
        ValueSpecification guard = activityEdge.getGuard();
        ValueSpecification weight = activityEdge.getWeight();
        StringBuilder stringBuilder = new StringBuilder();
        if (expectedPrefixValue != null) {
            stringBuilder.append(expectedPrefixValue);
        }
        String name = activityEdge.getName();
        if (name == null) {
            name = EMPTY;
        }
        stringBuilder.append(name);
        if (weight != null) {
            if (!stringBuilder.isEmpty()) {
                stringBuilder.append(EOL);
            }
            stringBuilder.append(OPEN_BRACKET);
            stringBuilder.append(WEIGHT);
            stringBuilder.append(EQL);
            stringBuilder.append(expectedWeightValue);
            stringBuilder.append(CLOSE_BRACKET);
        }
        if (guard != null) {
            if (!stringBuilder.isEmpty()) {
                stringBuilder.append(EOL);
            }
            stringBuilder.append(OPEN_ANGLE_BRACKET);
            stringBuilder.append(expectedGuardValue);
            stringBuilder.append(CLOSE_ANGLE_BRACKET);
        }
        return stringBuilder.toString();
    }

    private Profile getECoreProfile() {
        ResourceSet resourceSet = new ResourceSetImpl();
        Resource profileResource = resourceSet
                .createResource(URI.createURI("pathmap://UML_PROFILES/Ecore.profile.uml")); //$NON-NLS-1$
        try {
            profileResource.load(Map.of());
            return (Profile) profileResource.getContents().get(0);

        } catch (IOException e) {
            fail(e.getMessage());
        }
        return null;
    }

}
