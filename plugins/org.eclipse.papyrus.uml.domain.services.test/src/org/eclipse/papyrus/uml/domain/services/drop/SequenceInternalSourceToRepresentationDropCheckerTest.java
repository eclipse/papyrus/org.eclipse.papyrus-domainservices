/*****************************************************************************
 * Copyright (c) 2024 CEA LIST.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.SequenceInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.InteractionUse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link SequenceInternalSourceToRepresentationDropChecker}.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class SequenceInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private SequenceInternalSourceToRepresentationDropChecker sequenceInternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.sequenceInternalSourceToRepresentationDropChecker = new SequenceInternalSourceToRepresentationDropChecker();
    }

    @Test
    public void testCommentDropOnInteraction() {
        Comment commentToDrop = this.create(Comment.class);
        Interaction targetInteraction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetInteraction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnInteractionOperand() {
        Comment commentToDrop = this.create(Comment.class);
        InteractionOperand targetInteractionOperand = this.create(InteractionOperand.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetInteractionOperand);
        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnCombinedFragment() {
        Comment commentToDrop = this.create(Comment.class);
        CombinedFragment targetCombinedFragment = this.create(CombinedFragment.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetCombinedFragment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnInteractionUse() {
        Comment commentToDrop = this.create(Comment.class);
        InteractionUse targetInteractionUse = this.create(InteractionUse.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetInteractionUse);
        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnInteraction() {
        Constraint constraintToDrop = this.create(Constraint.class);
        Interaction targetInteraction = this.create(Interaction.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetInteraction);
        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnInteractionOperand() {
        Constraint constraintToDrop = this.create(Constraint.class);
        InteractionOperand targetInteractionOperand = this.create(InteractionOperand.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetInteractionOperand);
        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnCombinedFragment() {
        Constraint constraintToDrop = this.create(Constraint.class);
        CombinedFragment targetCombinedFragment = this.create(CombinedFragment.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetCombinedFragment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnInteractionUse() {
        Constraint constraintToDrop = this.create(Constraint.class);
        InteractionUse targetInteractionUse = this.create(InteractionUse.class);

        CheckStatus canDragAndDropStatus = this.sequenceInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetInteractionUse);
        assertFalse(canDragAndDropStatus.isValid());
    }
}
