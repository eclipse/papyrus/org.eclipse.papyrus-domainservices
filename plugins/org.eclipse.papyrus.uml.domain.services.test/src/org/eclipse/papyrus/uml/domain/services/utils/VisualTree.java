/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class VisualTree {

    private final List<VisualNode> rootNodes = new ArrayList<>();

    public VisualNode addChildren(EObject semanticElement) {
        VisualNode node = new VisualNode(semanticElement);
        rootNodes.add(node);
        return node;
    }

}
