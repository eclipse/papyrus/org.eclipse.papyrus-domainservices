/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.StateMachine;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CompositeStructureInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Test dropping a {@link Behavior} from a {@link Class} container to a
     * {@link org.eclipse.uml2.uml.Package} container.
     */
    @ParameterizedTest
    @ValueSource(classes = { FunctionBehavior.class, OpaqueBehavior.class, Interaction.class, StateMachine.class,
            ProtocolStateMachine.class })
    public <T extends Behavior> void testBehaviorDropFromClassToPackage(java.lang.Class<T> type) {
        T elementToDrop = this.create(type);
        Class clazzOldContainer = this.create(Class.class);
        org.eclipse.uml2.uml.Package packageNewContainer = this.create(org.eclipse.uml2.uml.Package.class);
        clazzOldContainer.getOwnedBehaviors().add(elementToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(elementToDrop,
                clazzOldContainer, packageNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(packageNewContainer.getPackagedElements().contains(elementToDrop));
    }

    /**
     * Test dropping a {@link Behavior} from a {@link Class} container to itself.
     */
    @ParameterizedTest
    @ValueSource(classes = { FunctionBehavior.class, OpaqueBehavior.class, Interaction.class, StateMachine.class,
            ProtocolStateMachine.class })
    public <T extends Behavior> void testBehaviorDropFromClassToSameContainer(java.lang.Class<T> type) {
        T elementToDrop = this.create(type);
        Class clazzContainer = this.create(Class.class);
        clazzContainer.getOwnedBehaviors().add(elementToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(elementToDrop,
                clazzContainer, clazzContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzContainer.getOwnedBehaviors().contains(elementToDrop));
    }

    /**
     * Test dropping a {@link Behavior} from a {@link org.eclipse.uml2.uml.Package}
     * container to a {@link Collaboration} container.
     */
    @ParameterizedTest
    @ValueSource(classes = { FunctionBehavior.class, OpaqueBehavior.class, Interaction.class, StateMachine.class,
            ProtocolStateMachine.class })
    public <T extends Behavior> void testBehaviorDropFromPackageToACollaboration(java.lang.Class<T> type) {
        T elementToDrop = this.create(type);
        org.eclipse.uml2.uml.Package packageOldContainer = this.create(org.eclipse.uml2.uml.Package.class);
        Collaboration newContainer = this.create(Collaboration.class);
        packageOldContainer.getPackagedElements().add(elementToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(elementToDrop,
                packageOldContainer, newContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertFalse(packageOldContainer.getPackagedElements().isEmpty());
    }

    /**
     * Test dropping a {@link Behavior} from a {@link org.eclipse.uml2.uml.Package}
     * container to a {@link Class} container.
     */
    @ParameterizedTest
    @ValueSource(classes = { FunctionBehavior.class, OpaqueBehavior.class, Interaction.class, StateMachine.class,
            ProtocolStateMachine.class })
    public <T extends Behavior> void testBehavioredClassifierDropFromPackageToClass(java.lang.Class<T> type) {
        T elementToDrop = this.create(type);
        org.eclipse.uml2.uml.Package packageOldContainer = this.create(org.eclipse.uml2.uml.Package.class);
        Class clazzNewContainer = this.create(Class.class);
        packageOldContainer.getPackagedElements().add(elementToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(elementToDrop,
                packageOldContainer, clazzNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(packageOldContainer.getPackagedElements().isEmpty());
        assertTrue(clazzNewContainer.getOwnedBehaviors().contains(elementToDrop));
    }

    /**
     * Test dropping a {@link Class} from a {@link Class} container to an other one.
     */
    @Test
    public void testClassDropFromClassToClass() {
        Class clazzToDrop = this.create(Class.class);
        Class clazzOldContainer = this.create(Class.class);
        Class clazzNewContainer = this.create(Class.class);
        clazzOldContainer.getNestedClassifiers().add(clazzToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(clazzToDrop,
                clazzOldContainer, clazzNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(clazzNewContainer.getNestedClassifiers().contains(clazzToDrop));
    }

    /**
     * Test dropping a {@link Class} from a {@link Class} container to a
     * {@link Model}.
     */
    @Test
    public void testClassDropFromClassToModel() {
        Class clazzToDrop = this.create(Class.class);
        Class clazzOldContainer = this.create(Class.class);
        Model modelNewContainer = this.create(Model.class);
        clazzOldContainer.getNestedClassifiers().add(clazzToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(clazzToDrop,
                clazzOldContainer, modelNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(modelNewContainer.getPackagedElements().contains(clazzToDrop));
    }

    /**
     * Test dropping a {@link Class} from a {@link Class} container to an other one.
     */
    @Test
    public void testClassDropFromClassToSameContainer() {
        Class clazzToDrop = this.create(Class.class);
        Class clazzContainer = this.create(Class.class);
        clazzContainer.getNestedClassifiers().add(clazzToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(clazzToDrop,
                clazzContainer, clazzContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzContainer.getNestedClassifiers().contains(clazzToDrop));
    }

    /**
     * Test dropping a {@link Class} from a {@link Class} container to a
     * {@link org.eclipse.uml2.uml.Package}.
     */
    @Test
    public void testClassDropOnProperty() {
        Class clazzToDrop = this.create(Class.class);
        Class clazzOldContainer = this.create(Class.class);
        org.eclipse.uml2.uml.Package packageNewContainer = this.create(org.eclipse.uml2.uml.Package.class);
        clazzOldContainer.getNestedClassifiers().add(clazzToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(clazzToDrop,
                clazzOldContainer, packageNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(packageNewContainer.getPackagedElements().contains(clazzToDrop));
    }

    /**
     * Test dropping a {@link Collaboration} from a {@link Class} container to a
     * {@link org.eclipse.uml2.uml.Package} container.
     */
    @Test
    public void testCollaborationDropFromClassToPackage() {
        Collaboration collaborationToDrop = this.create(Collaboration.class);
        Class clazzOldContainer = this.create(Class.class);
        org.eclipse.uml2.uml.Package packageNewContainer = this.create(org.eclipse.uml2.uml.Package.class);
        clazzOldContainer.getNestedClassifiers().add(collaborationToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                collaborationToDrop, clazzOldContainer, packageNewContainer, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(packageNewContainer.getPackagedElements().contains(collaborationToDrop));
    }

    /**
     * Test dropping a {@link Collaboration} from a {@link Class} container to
     * itself.
     */
    @Test
    public void testCollaborationDropFromClassToSameContainer() {
        Collaboration collaborationToDrop = this.create(Collaboration.class);
        Class clazzContainer = this.create(Class.class);
        clazzContainer.getNestedClassifiers().add(collaborationToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                collaborationToDrop, clazzContainer, clazzContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzContainer.getNestedClassifiers().contains(collaborationToDrop));
    }

    /**
     * Test dropping a {@link Collaboration} from a
     * {@link org.eclipse.uml2.uml.Package} container to a {@link Class} container.
     */
    @Test
    public void testCollaborationDropFromPackageToClass() {
        Collaboration collaborationToDrop = this.create(Collaboration.class);
        org.eclipse.uml2.uml.Package packageOldContainer = this.create(org.eclipse.uml2.uml.Package.class);
        Class clazzNewContainer = this.create(Class.class);
        packageOldContainer.getPackagedElements().add(collaborationToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                collaborationToDrop, packageOldContainer, clazzNewContainer, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(packageOldContainer.getPackagedElements().isEmpty());
        assertTrue(clazzNewContainer.getNestedClassifiers().contains(collaborationToDrop));
    }

    /**
     * Test dropping a {@link Comment} from a {@link Class} container to a typed
     * {@link org.eclipse.uml2.uml.Package}.
     */
    @Test
    public void testCommentDropFromClassToPackage() {
        Comment commentToDrop = this.create(Comment.class);
        Class clazzOldContainer = this.create(Class.class);
        org.eclipse.uml2.uml.Package packageNewContainer = this.create(org.eclipse.uml2.uml.Package.class);
        clazzOldContainer.getOwnedComments().add(commentToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(commentToDrop,
                clazzOldContainer, packageNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getOwnedComments().isEmpty());
        assertTrue(packageNewContainer.getOwnedComments().contains(commentToDrop));
    }

    /**
     * Test dropping a {@link Comment} from a {@link Class} container to itself.
     */
    @Test
    public void testCommentDropFromClassToSameContainer() {
        Comment commentToDrop = this.create(Comment.class);
        Class clazzOldContainer = this.create(Class.class);
        clazzOldContainer.getOwnedComments().add(commentToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(commentToDrop,
                clazzOldContainer, clazzOldContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzOldContainer.getOwnedComments().contains(commentToDrop));
    }

    /**
     * Test dropping a {@link InformationItem} from a {@link Class} container to a
     * {@link org.eclipse.uml2.uml.Package} container.
     */
    @Test
    public void testInformationItemDropFromClassToPackage() {
        InformationItem informationItemToDrop = this.create(InformationItem.class);
        Class clazzOldContainer = this.create(Class.class);
        org.eclipse.uml2.uml.Package packageNewContainer = this.create(org.eclipse.uml2.uml.Package.class);
        clazzOldContainer.getNestedClassifiers().add(informationItemToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                informationItemToDrop, clazzOldContainer, packageNewContainer, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getNestedClassifiers().isEmpty());
        assertTrue(packageNewContainer.getPackagedElements().contains(informationItemToDrop));
    }

    /**
     * Test dropping a {@link InformationItem} from a {@link Class} container to
     * itself.
     */
    @Test
    public void testInformationItemDropFromClassToSameContainer() {
        InformationItem informationItemToDrop = this.create(InformationItem.class);
        Class clazzContainer = this.create(Class.class);
        clazzContainer.getNestedClassifiers().add(informationItemToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                informationItemToDrop, clazzContainer, clazzContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzContainer.getNestedClassifiers().contains(informationItemToDrop));
    }

    /**
     * Test dropping a {@link InformationItem} from a
     * {@link org.eclipse.uml2.uml.Package} container to a {@link Class} container.
     */
    @Test
    public void testInformationItemDropFromPackageToClass() {
        InformationItem informationItemToDrop = this.create(InformationItem.class);
        org.eclipse.uml2.uml.Package packageOldContainer = this.create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class clazzNewContainer = this.create(org.eclipse.uml2.uml.Class.class);
        packageOldContainer.getPackagedElements().add(informationItemToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(
                informationItemToDrop, packageOldContainer, clazzNewContainer, this.getCrossRef(),
                this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(packageOldContainer.getPackagedElements().isEmpty());
        assertTrue(clazzNewContainer.getNestedClassifiers().contains(informationItemToDrop));
    }

    /**
     * Test dropping a {@link Parameter} from an {@link Activity} container to an
     * {@link Interaction} container.
     */
    @Test
    public void testParameterDropFromBehaviorToAnotherBehavior() {
        Parameter parameterToDrop = this.create(Parameter.class);
        Activity oldContainer = this.create(Activity.class);
        oldContainer.getOwnedParameters().add(parameterToDrop);

        Interaction newContainer = this.create(Interaction.class);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(parameterToDrop,
                oldContainer, newContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(newContainer.getOwnedParameters().contains(parameterToDrop));
    }

    /**
     * Test dropping a {@link Parameter} from an {@link Activity} container to a
     * {@link Class} container.
     */
    @Test
    public void testParameterDropFromBehaviorToClass() {
        Parameter parameterToDrop = this.create(Parameter.class);
        Activity oldContainer = this.create(Activity.class);
        oldContainer.getOwnedParameters().add(parameterToDrop);

        Class newContainer = this.create(Class.class);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(parameterToDrop,
                oldContainer, newContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
    }

    /**
     * Test dropping a {@link Parameter} from an {@link Activity} container to
     * itself.
     */
    @Test
    public void testParameterDropFromBehaviorToSameContainer() {
        Parameter parameterToDrop = this.create(Parameter.class);
        Activity oldContainer = this.create(Activity.class);
        oldContainer.getOwnedParameters().add(parameterToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(parameterToDrop,
                oldContainer, oldContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(oldContainer.getOwnedParameters().contains(parameterToDrop));
    }

    /**
     * Test dropping a {@link Property} from a {@link Class} container to a
     * {@link Class} container.
     */
    @Test
    public void testPropertyDropFromClassToClass() {
        Property propertyToDrop = this.create(Property.class);
        Class clazzOldContainer = this.create(Class.class);
        Class clazzNewContainer = this.create(Class.class);
        clazzOldContainer.getOwnedAttributes().add(propertyToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(propertyToDrop,
                clazzOldContainer, clazzNewContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getOwnedAttributes().isEmpty());
        assertTrue(clazzNewContainer.getOwnedAttributes().contains(propertyToDrop));
    }

    /**
     * Test dropping a {@link Property} from a Class container to a non typed
     * {@link Property} container.
     */
    @Test
    public void testPropertyDropFromClassToNotTypedProperty() {
        Property propertyToDrop = this.create(Property.class);
        Class clazzOldContainer = this.create(Class.class);
        clazzOldContainer.getOwnedAttributes().add(propertyToDrop);

        Property newPropertyContainer = this.create(Property.class);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(propertyToDrop,
                clazzOldContainer, newPropertyContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.FAILED, status.getState());
        assertEquals("Container should be a Structured Classifier or a Typed Property.", status.getMessage());
    }

    /**
     * Test dropping a {@link Property} from a {@link Class} container to itself.
     */
    @Test
    public void testPropertyDropFromClassToSameContainer() {
        Property propertyToDrop = this.create(Property.class);
        Class clazzContainer = this.create(Class.class);
        clazzContainer.getOwnedAttributes().add(propertyToDrop);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(propertyToDrop,
                clazzContainer, clazzContainer, this.getCrossRef(), this.getEditableChecker());

        assertNull(status);
        assertTrue(clazzContainer.getOwnedAttributes().contains(propertyToDrop));
    }

    /**
     * Test dropping a {@link Property} from a Class container to a Typed
     * {@link Property} container.
     */
    @Test
    public void testPropertyDropFromClassToTypedProperty() {
        Property propertyToDrop = this.create(Property.class);
        Class clazzOldContainer = this.create(Class.class);
        clazzOldContainer.getOwnedAttributes().add(propertyToDrop);

        Property newPropertyContainer = this.create(Property.class);
        Class typedClass = this.create(Class.class);
        newPropertyContainer.setType(typedClass);

        Status status = new CompositeStructureInternalSourceToRepresentationDropBehaviorProvider().drop(propertyToDrop,
                clazzOldContainer, newPropertyContainer, this.getCrossRef(), this.getEditableChecker());

        assertEquals(State.DONE, status.getState());
        assertTrue(clazzOldContainer.getOwnedAttributes().isEmpty());
        assertTrue(typedClass.getOwnedAttributes().contains(propertyToDrop));
    }
}
