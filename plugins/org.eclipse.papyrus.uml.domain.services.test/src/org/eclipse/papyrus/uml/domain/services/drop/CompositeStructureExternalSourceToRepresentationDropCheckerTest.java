/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompositeStructureExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private CompositeStructureExternalSourceToRepresentationDropChecker compositeStructureExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.compositeStructureExternalSourceToRepresentationDropChecker = new CompositeStructureExternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Activity} on {@link Class} => authorized.
     */
    @Test
    public void testActivityDropOnClass() {
        Class classContainer = this.create(Class.class);
        Activity activity = this.create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Comment} => not authorized.
     */
    @Test
    public void testActivityDropOnComment() {
        Comment comment = this.create(Comment.class);
        Activity activity = this.create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Package} => authorized.
     */
    @Test
    public void testActivityDropOnPackage() {
        Activity activity = this.create(Activity.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Property} => authorized.
     */
    @Test
    public void testActivityDropOnProperty() {
        Property property = this.create(Property.class);
        Activity activity = this.create(org.eclipse.uml2.uml.Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Class} => authorized.
     */
    @Test
    public void testClassDropOnClass() {
        Class classContainer = this.create(Class.class);
        Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Comment} => not authorized.
     */
    @Test
    public void testClassDropOnComment() {
        Comment comment = this.create(Comment.class);
        Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Model} => authorized.
     */
    @Test
    public void testClassDropOnModel() {
        Model model = this.create(Model.class);
        Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, model);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Property} => authorized.
     */
    @Test
    public void testClassDropOnProperty() {
        Property property = this.create(Property.class);
        Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Class} => authorized.
     */
    @Test
    public void testCollaborationDropOnClass() {
        Class classContainer = this.create(Class.class);
        Collaboration collaboration = this.create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link CollaborationUse} =>
     * authorized.
     */
    @Test
    public void testCollaborationDropOnCollaborationUse() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Collaboration collaboration = this.create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, collaborationUse);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Comment} => not authorized.
     */
    @Test
    public void testCollaborationDropOnComment() {
        Comment comment = this.create(Comment.class);
        Collaboration collaboration = this.create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Package} => authorized.
     */
    @Test
    public void testCollaborationDropOnPackage() {
        Collaboration collaboration = this.create(Collaboration.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Property} => authorized.
     */
    @Test
    public void testCollaborationDropOnProperty() {
        Property property = this.create(Property.class);
        Collaboration collaboration = this.create(org.eclipse.uml2.uml.Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaboration, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link CollaborationUse} on {@link Comment} => not
     * authorized.
     */
    @Test
    public void testCollaborationUseDropOnComment() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaborationUse, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link CollaborationUse} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testCollaborationUseDropOnStructuredClassifier() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Activity activity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaborationUse, activity);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Comment} => not authorized.
     */
    @Test
    public void testCommentDropOnComment() {
        Comment comment = this.create(Comment.class);
        Comment commentTarget = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, commentTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Package} => authorized.
     */
    @Test
    public void testCommentDropOnPackage() {
        Comment comment = this.create(Comment.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Property} => authorized.
     */
    @Test
    public void testCommentDropOnProperty() {
        Comment comment = this.create(Comment.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testCommentDropOnStructuredClassifier() {
        Comment comment = this.create(Comment.class);
        Activity activity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, activity);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Comment} => not authorized.
     */
    @Test
    public void testConstraintDropOnComment() {
        Constraint constraint = this.create(Constraint.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Package} => authorized.
     */
    @Test
    public void testConstraintDropOnPackage() {
        Constraint constraint = this.create(Constraint.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testConstraintDropOnStructuredClassifier() {
        Constraint constraint = this.create(Constraint.class);
        Activity activity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, activity);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping an element that cannot be dropped on the CompositeStructure
     * diagram.
     */
    @Test
    public void testDeploymentDrop() {
        Deployment deployment = this.create(Deployment.class);
        Package pak = this.create(Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(deployment, pak);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Class} => authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnClass() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Comment} => not
     * authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnComment() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Package} => authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnPackage() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Property} => authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnProperty() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Class} => authorized.
     */
    @Test
    public void testInformationItemDropOnClass() {
        InformationItem informationItem = this.create(InformationItem.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Comment} => not authorized.
     */
    @Test
    public void testInformationItemDropOnComment() {
        InformationItem informationItem = this.create(InformationItem.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Package} => authorized.
     */
    @Test
    public void testInformationItemDropOnPackage() {
        InformationItem informationItem = this.create(InformationItem.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Property} => authorized.
     */
    @Test
    public void testInformationItemDropOnProperty() {
        InformationItem informationItem = this.create(InformationItem.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Class} => authorized.
     */
    @Test
    public void testInteractionDropOnClass() {
        Interaction interaction = this.create(Interaction.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Comment} => not authorized.
     */
    @Test
    public void testInteractionDropOnComment() {
        Interaction interaction = this.create(Interaction.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Package} => authorized.
     */
    @Test
    public void testInteractionDropOnPackage() {
        Interaction interaction = this.create(Interaction.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Property} => authorized.
     */
    @Test
    public void testInteractionDropOnProperty() {
        Interaction interaction = this.create(Interaction.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Class} => authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnClass() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Comment} => not authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnComment() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Package} => authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnPackage() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Property} => authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnProperty() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Class} => not authorized.
     */
    @Test
    public void testParameterDropOnClass() {
        Parameter parameter = this.create(Parameter.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, clazz);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Collaboration} => not authorized.
     */
    @Test
    public void testParameterDropOnCollaboration() {
        Parameter parameter = this.create(Parameter.class);
        Collaboration collaboration = this.create(Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, collaboration);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Comment} => not authorized.
     */
    @Test
    public void testParameterDropOnComment() {
        Parameter parameter = this.create(Parameter.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testParameterDropOnStructuredClassifier() {
        Parameter parameter = this.create(Parameter.class);
        Activity activity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, activity);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on its {@link Classifier} container =>
     * authorized.
     */
    @Test
    public void testPortDropOnContainerClassifier() {
        Port port = this.create(Port.class);
        Class classContainer = this.create(Class.class);
        classContainer.getOwnedAttributes().add(port);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Classifier} which is not its container
     * => not authorized.
     */
    @Test
    public void testPortDropOnNotContainerClassifier() {
        Port port = this.create(Port.class);
        Class classContainer = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, classContainer);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Property} not typed => not authorized.
     */
    @Test
    public void testPortDropOnPropertyNotTyped() {
        Port portToDnD = this.create(Port.class);
        Property propertyTarget = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(portToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link port} on {@link Property} typed with its
     * {@link Classifier} container => authorized.
     */
    @Test
    public void testPortDropOnPropertyTypedWithContainerClassifier() {
        Port port = this.create(Port.class);
        Property propertyTarget = this.create(Property.class);
        Class classContainer = this.create(Class.class);
        classContainer.getOwnedAttributes().add(port);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, propertyTarget);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link Property} not typed with its
     * {@link Classifier} container => not authorized.
     */
    @Test
    public void testPortDropOnPropertyTypedWithoutContainerClassifier() {
        Port portToDnD = this.create(Port.class);
        Property propertyTarget = this.create(Property.class);
        Class classContainer = this.create(Class.class);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(portToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on its {@link Classifier} container =>
     * authorized.
     */
    @Test
    public void testPropertyDropOnContainerClassifier() {
        Property property = this.create(Property.class);
        Class classContainer = this.create(Class.class);
        classContainer.getOwnedAttributes().add(property);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, classContainer);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Classifier} which is not its
     * container => not authorized.
     */
    @Test
    public void testPropertyDropOnNotContainerClassifier() {
        Property property = this.create(Property.class);
        Class classContainer = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(property, classContainer);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} not typed => not
     * authorized.
     */
    @Test
    public void testPropertyDropOnPropertyNotTyped() {
        Property propertyToDnD = this.create(Property.class);
        Property propertyTarget = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} typed with its
     * {@link Classifier} container => authorized.
     */
    @Test
    public void testPropertyDropOnPropertyTypedWithContainerClassifier() {
        Property propertyToDnD = this.create(Property.class);
        Property propertyTarget = this.create(Property.class);
        Class classContainer = this.create(Class.class);
        classContainer.getOwnedAttributes().add(propertyToDnD);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} not typed with its
     * {@link Classifier} container => not authorized.
     */
    @Test
    public void testPropertyDropOnPropertyTypedWithoutContainerClassifier() {
        Property propertyToDnD = this.create(Property.class);
        Property propertyTarget = this.create(Property.class);
        Class classContainer = this.create(Class.class);
        propertyTarget.setType(classContainer);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(propertyToDnD, propertyTarget);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Class} => authorized.
     */
    @Test
    public void testStateMachineDropOnClass() {
        StateMachine stateMachine = this.create(StateMachine.class);
        org.eclipse.uml2.uml.Class clazz = this.create(org.eclipse.uml2.uml.Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Comment} => not authorized.
     */
    @Test
    public void testStateMachineDropOnComment() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Package} => authorized.
     */
    @Test
    public void testStateMachineDropOnPackage() {
        StateMachine stateMachine = this.create(StateMachine.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Property} => authorized.
     */
    @Test
    public void testStateMachineDropOnProperty() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Type} on {@link Package} => not authorized.
     */
    @Test
    public void testTypeDropOnPackage() {
        Artifact artifact = this.create(Artifact.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(artifact, pack);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Type} on {@link Property} => authorized.
     */
    @Test
    public void testTypeDropOnProperty() {
        Artifact artifact = this.create(Artifact.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureExternalSourceToRepresentationDropChecker
                .canDragAndDrop(artifact, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

}
