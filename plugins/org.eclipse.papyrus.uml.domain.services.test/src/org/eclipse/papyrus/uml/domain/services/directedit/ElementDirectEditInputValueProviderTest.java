/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.directedit;

import static org.eclipse.papyrus.uml.domain.services.EMFUtils.allContainedObjectOfType;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.uml.domain.services.labels.ElementLabelProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.junit.jupiter.api.Test;

public class ElementDirectEditInputValueProviderTest extends AbstractUMLTest {

    /**
     * Model containing a UML Model with stereotype content.
     */
    public static final URI UML_MODEL_WITH_PROFILE = URI.createPlatformPluginURI(
            "/org.eclipse.papyrus.uml.domain.services.test/profile/UMLModelWithStandardProfile.uml", false);

    /**
     * Check that the stereotypes are not given in the input value of the direct
     * edit.
     */
    @Test
    public void checkElementWithStereotype() {

        ResourceSet rs = new ResourceSetImpl();
        Resource umlResource = rs.getResource(UML_MODEL_WITH_PROFILE, true);

        String classOneStereotypeName = "ClassOneStereotype";
        Class classOneStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classOneStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        ElementLabelProvider labelProvider = ElementLabelProvider.buildDefault();
        String label0 = labelProvider.getLabel(classOneStereotype);
        assertEquals(ST_LEFT + "Utility" + ST_RIGHT + EOL + classOneStereotypeName, label0);
        String inputValue = new ElementDirectEditInputValueProvider().getDirectEditInputValue(classOneStereotype);
        assertEquals(classOneStereotypeName, inputValue);

    }

    /**
     * Check that the input value for a comment is the content of the body feature.
     */
    @Test
    public void checkComment() {
        Comment comment = create(Comment.class);
        comment.setBody("A body");
        String inputValue = new ElementDirectEditInputValueProvider().getDirectEditInputValue(comment);
        assertEquals("A body", inputValue);

    }

    /**
     * Check that the input value for a NamedElement (here a comment) is the content
     * of the name feature.
     */
    @Test
    public void checkNamedElement() {
        Component component = create(Component.class);
        component.setName("A component");
        String inputValue = new ElementDirectEditInputValueProvider().getDirectEditInputValue(component);
        assertEquals("A component", inputValue);

    }

}
