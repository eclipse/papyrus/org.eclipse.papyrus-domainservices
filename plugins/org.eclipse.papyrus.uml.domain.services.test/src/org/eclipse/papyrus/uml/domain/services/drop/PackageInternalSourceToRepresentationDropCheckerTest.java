/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.PackageInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for {@link PackageInternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PackageInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private PackageInternalSourceToRepresentationDropChecker packageInternalSourceToRepresentationDropChecker;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.packageInternalSourceToRepresentationDropChecker = new PackageInternalSourceToRepresentationDropChecker();
    }

    @ParameterizedTest
    @MethodSource("getCanDragAndDropParameters")
    public void testCanDragAndDrop(Class<? extends Element> elementToDropClazz,
            Class<? extends Element> semanticContainerClazz, boolean expectedCheckStatus) {
        Element elementToDrop = this.create(elementToDropClazz);
        Element semanticContainer = this.create(semanticContainerClazz);

        CheckStatus canDragAndDropStatus = this.packageInternalSourceToRepresentationDropChecker
                .canDragAndDrop(elementToDrop, semanticContainer);
        assertEquals(expectedCheckStatus, canDragAndDropStatus.isValid());
    }

    public static Stream<Arguments> getCanDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Constraint.class, Constraint.class, false),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Constraint.class, Model.class, true),
                Arguments.of(Comment.class, Constraint.class, false),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Comment.class, Model.class, true),
                Arguments.of(Model.class, Constraint.class, false),
                Arguments.of(Model.class, Comment.class, false),
                Arguments.of(Model.class, Package.class, true),
                Arguments.of(Model.class, Model.class, true),
                Arguments.of(Package.class, Constraint.class, false),
                Arguments.of(Package.class, Comment.class, false),
                Arguments.of(Package.class, Package.class, true),
                Arguments.of(Package.class, Model.class, true)
                );
        // @formatter:on
    }

}
