/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.ActivityInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityFinalNode;
import org.eclipse.uml2.uml.ActivityParameterNode;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ExpansionNode;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.InterruptibleActivityRegion;
import org.eclipse.uml2.uml.StructuredActivityNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ActivityInternalSourceToRepresentationDropChecker}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class ActivityInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private ActivityInternalSourceToRepresentationDropChecker activityInternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.activityInternalSourceToRepresentationDropChecker = new ActivityInternalSourceToRepresentationDropChecker();
    }

    @Test
    public void testActivitNodeDropOnActivity() {
        ActivityFinalNode activityNodeToDrop = this.create(ActivityFinalNode.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityNodeToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivitNodeDropOnActivityGroup() {
        ActivityFinalNode activityNodeToDrop = this.create(ActivityFinalNode.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityNodeToDrop, targetActivityPartition);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivitNodeDropOnActivityNode() {
        ActivityFinalNode activityNodeToDrop = this.create(ActivityFinalNode.class);
        ActivityFinalNode targetActivityNode = this.create(ActivityFinalNode.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityNodeToDrop, targetActivityNode);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityDropOnActivity() {
        Activity activityToDrop = this.create(Activity.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityDropOnActivityNode() {
        Activity activityToDrop = this.create(Activity.class);
        ActivityFinalNode targetActivityNode = this.create(ActivityFinalNode.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityToDrop, targetActivityNode);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityParameterNodeDropOnActivity() {
        ActivityParameterNode activityParameterNodeToDrop = this.create(ActivityParameterNode.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityParameterNodeToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityParameterNodeDropOnActivityGroup() {
        ActivityParameterNode activityParameterNodeToDrop = this.create(ActivityParameterNode.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityParameterNodeToDrop, targetActivityPartition);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityPartitionDropOnActivity() {
        ActivityPartition activityPartitionToDrop = this.create(ActivityPartition.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityPartitionToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityPartitionDropOnActivityPartition() {
        ActivityPartition activityPartitionToDrop = this.create(ActivityPartition.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityPartitionToDrop, targetActivityPartition);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testActivityPartitionDropOnInterruptibleActivityRegion() {
        ActivityPartition activityPartitionToDrop = this.create(ActivityPartition.class);
        InterruptibleActivityRegion targetInterruptibleActivityRegion = this.create(InterruptibleActivityRegion.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activityPartitionToDrop, targetInterruptibleActivityRegion);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnActivity() {
        Comment commentToDrop = this.create(Comment.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnActivityGroup() {
        Comment commentToDrop = this.create(Comment.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetActivityPartition);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testCommentDropOnActivityNode() {
        Comment commentToDrop = this.create(Comment.class);
        ActivityFinalNode targetActivityNode = this.create(ActivityFinalNode.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(commentToDrop, targetActivityNode);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnActivity() {
        Constraint constraintToDrop = this.create(Constraint.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnActivityPartition() {
        Constraint constraintToDrop = this.create(Constraint.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetActivityPartition);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testConstraintDropOnStructuredActivityNode() {
        Constraint constraintToDrop = this.create(Constraint.class);
        StructuredActivityNode targetStructuredActivityNode = this.create(StructuredActivityNode.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraintToDrop, targetStructuredActivityNode);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testDeploymentDropOnStructuredActivityNode() {
        Deployment deployment = this.create(Deployment.class);
        StructuredActivityNode targetStructuredActivityNode = this.create(StructuredActivityNode.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(deployment, targetStructuredActivityNode);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testExpansionNodeDropOnActivity() {
        ExpansionNode expansionNodeToDrop = this.create(ExpansionNode.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(expansionNodeToDrop, targetActivity);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testExpansionNodeDropOnExpansionRegion() {
        ExpansionNode expansionNodeToDrop = this.create(ExpansionNode.class);
        ExpansionRegion targetExpansionRegion = this.create(ExpansionRegion.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(expansionNodeToDrop, targetExpansionRegion);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testInterruptibleActivityRegionDropOnActivity() {
        InterruptibleActivityRegion interruptibleActivityRegionToDrop = this.create(InterruptibleActivityRegion.class);
        Activity targetActivity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interruptibleActivityRegionToDrop, targetActivity);

        assertTrue(canDragAndDropStatus.isValid());
    }

    @Test
    public void testInterruptibleActivityRegionDropOnActivityPartition() {
        InterruptibleActivityRegion interruptibleActivityRegionToDrop = this.create(InterruptibleActivityRegion.class);
        ActivityPartition targetActivityPartition = this.create(ActivityPartition.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interruptibleActivityRegionToDrop, targetActivityPartition);

        assertFalse(canDragAndDropStatus.isValid());
    }

    @Test
    public void testInterruptibleActivityRegionDropOnInterruptibleActivityRegion() {
        InterruptibleActivityRegion interruptibleActivityRegionToDrop = this.create(InterruptibleActivityRegion.class);
        InterruptibleActivityRegion targetInterruptibleActivityRegion = this.create(InterruptibleActivityRegion.class);

        CheckStatus canDragAndDropStatus = this.activityInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interruptibleActivityRegionToDrop, targetInterruptibleActivityRegion);

        assertFalse(canDragAndDropStatus.isValid());
    }
}
