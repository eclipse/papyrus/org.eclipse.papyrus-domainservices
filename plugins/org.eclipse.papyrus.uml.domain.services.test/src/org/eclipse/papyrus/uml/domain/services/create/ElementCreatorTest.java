/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementCreatorTest extends AbstractUMLTest {

    private static final String PACKAGED_ELEMENT = UMLPackage.eINSTANCE.getPackage_PackagedElement().getName();

    private static final String COMPONENT_TYPE = UMLPackage.eINSTANCE.getComponent().getName();

    private static final String X = "X"; //$NON-NLS-1$

    private static final String CLASS_TYPE = UMLPackage.eINSTANCE.getClass_().getName();

    private ElementCreator elementCreator;

    private IElementConfigurer settingName = new IElementConfigurer() {

        @Override
        public EObject configure(EObject toInit, EObject parent) {
            if (toInit instanceof NamedElement) {
                NamedElement namedElement = (NamedElement) toInit;
                namedElement.setName(X);
            }
            return toInit;
        }

    };

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementCreator = new ElementCreator(this.settingName,
                new ElementFeatureModifier(getCrossRef(), getEditableChecker()));
    }

    @Test
    public void testCreationOfClasses() {

        var pack = create(Package.class);

        // Create one class
        CreationStatus creationStatus1 = this.elementCreator.create(pack, CLASS_TYPE, PACKAGED_ELEMENT); // $NON-NLS-1$

        assertEquals(State.DONE, creationStatus1.getState());
        EObject newInstance = creationStatus1.getElement();

        assertEquals(1, pack.getPackagedElements().size());
        assertEquals(newInstance, pack.getPackagedElements().get(0));
        assertTrue(newInstance instanceof Class);
        assertEquals(X, ((Class) newInstance).getName());

        // Create a second one
        CreationStatus creationStatus2 = this.elementCreator.create(pack, CLASS_TYPE, PACKAGED_ELEMENT); // $NON-NLS-1$

        assertEquals(State.DONE, creationStatus2.getState());
        EObject newInstance2 = creationStatus2.getElement();
        assertEquals(2, pack.getPackagedElements().size());
        assertEquals(newInstance2, pack.getPackagedElements().get(1));
        assertTrue(newInstance2 instanceof Class);
        assertEquals(X, ((Class) newInstance2).getName());

    }

    /**
     * Checks that a creation is forbidden on a non editable element.
     */
    @Test
    public void testForbiddenCreationOnUneditable() {
        var pack = create(Package.class);

        elementCreator = new ElementCreator(this.settingName, new ElementFeatureModifier(getCrossRef(), e -> false));

        // Create one class
        CreationStatus creationStatus1 = this.elementCreator.create(pack, CLASS_TYPE, PACKAGED_ELEMENT); // $NON-NLS-1$
        assertEquals(State.FAILED, creationStatus1.getState());
    }

    @Test
    public void testInvalidContainmentReference() {
        var pack = create(Package.class);

        // Invalid type
        CreationStatus creationStatus1 = this.elementCreator.create(pack, CLASS_TYPE, "ownedComment"); //$NON-NLS-1$
        assertEquals(State.FAILED, creationStatus1.getState());

        // Invalid ref name
        CreationStatus creationStatus2 = this.elementCreator.create(pack, CLASS_TYPE, "packagedElement2"); //$NON-NLS-1$
        assertEquals(State.FAILED, creationStatus2.getState());

    }

    @Test
    public void testCreateComponentInComponent() {
        Component parentComponent = create(Component.class);

        CreationStatus creationStatus = elementCreator.create(parentComponent, COMPONENT_TYPE, "packagedElement");
        assertEquals(State.DONE, creationStatus.getState());

        CreationStatus creationStatus2 = elementCreator.create(parentComponent, COMPONENT_TYPE, "nestedClassifier");
        assertEquals(State.FAILED, creationStatus2.getState());
    }

}
