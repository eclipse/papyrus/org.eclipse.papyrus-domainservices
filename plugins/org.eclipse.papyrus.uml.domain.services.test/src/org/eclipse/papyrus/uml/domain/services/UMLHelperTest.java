/*******************************************************************************
 * Copyright (c) 2019, 2020 Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.Test;

public class UMLHelperTest {

    private static final String CLASS_TYPE = "Class"; //$NON-NLS-1$

    @Test
    public void testToEclass() {
        assertNull(UMLHelper.toEClass(null));
        assertNull(UMLHelper.toEClass("nonExistingClass")); //$NON-NLS-1$
        assertEquals(UMLPackage.eINSTANCE.getClass_(), UMLHelper.toEClass(CLASS_TYPE));
        assertEquals(UMLPackage.eINSTANCE.getClass_(), UMLHelper.toEClass("uml::Class")); //$NON-NLS-1$
    }

}
